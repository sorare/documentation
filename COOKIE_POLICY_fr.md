# COOKIE POLICY

August 2, 2024

Version 2.0

## 1. Avant-propos

Sorare est une plateforme de divertissement sportif où les fans de sport ("vous" ou "Utilisateur") peuvent s'engager avec leurs athlètes et équipes préférés et créer des liens avec la communauté sportive mondiale.

Le site web de Sorare offre aux Utilisateurs une place de marché pour leur permettre d’acquérir, de collectionner et d’échanger des Cartes à Collectionner (collectivement, les " Services").
Pour permettre aux Utilisateurs de bénéficier des Services proposés par le site internet www.sorare.com (“Site”) tels que sa consultation, l'optimisation de son utilisation ou sa personnalisation en fonction de l'Utilisateur, le Site utilise des cookies.

Vous pouvez désactiver les cookies auxquels vous avez consenti à tout moment, gratuitement, en utilisant les options de désactivation qui vous sont proposées et listées ci-dessous.

## 2. Qu'est-ce qu'un cookie ?

**2.1** Lorsque vous naviguez sur le Site Internet, des informations relatives à la navigation de votre appareil (ordinateur, tablette, smartphone, etc.) sur le Site peuvent être enregistrées dans des fichiers texte appelés "cookies", déposés sur votre navigateur. Les cookies sont utilisés pour reconnaître votre navigateur pendant la durée de validité du cookie afin de renvoyer certaines informations au Site Internet (telles qu'un identifiant de session ou un choix de langue).

**2.2** En théorie, seul l'émetteur d'un cookie peut lire ou modifier les informations les informations qui y sont contenues.

**2.3** Il existe différents types de cookies :

- les cookies de session qui disparaissent dès que vous quittez le site internet;
- les cookies permanents qui restent sur votre appareil jusqu'à l'expiration de leur durée de vie ou jusqu'à ce que vous les supprimiez à l'aide des fonctions de votre navigateur.

**2.4** Vous êtes informé que, lors de vos visites sur le Site, des cookies peuvent être placés sur votre équipement informatique.

## 3. Finalités des cookies placés sur l’appareil

**3.1** Certains cookies sont indispensables à l'utilisation du Site : il s'agit des cookies techniques utilisés par l'hébergeur du Site pour la gestion technique du réseau.

**3.2** D'autres sont utilisés pour analyser la fréquentation et l'utilisation faite du Site Internet, afin de :

- de réaliser des études afin d’améliorer l'expérience de navigation des Utilisateurs, dans l'objectif final de vous proposer un Site Internet toujours plus adapté ;
- de mémoriser les préférences d'affichage de votre navigateur (langue utilisée, paramètres d'affichage, système d'exploitation utilisé, etc.) et d'adapter la présentation du Site au moment de vos visites, en fonction des matériels et logiciels de visualisation ou de lecture que comporte votre équipement terminal et qui sont utilisés pour la navigation sur notre Site ;
- de mettre en œuvre et de maintenir les mesures de sécurité nécessaires à la préservation du Site et de notre communauté et au respect des Conditions Générales de Sorare.

**3.3** Enfin certains cookies sont utilisés à des fins publicitaires et en particulier pour identifier la provenance de nouveaux utilisateurs afin de permettre à Sorare de mesurer la performance de ses campagnes marketing sur des sites tiers et réseaux sociaux.

## 4. Données personnelles traitées

**4.1** Les cookies utilisés sur notre Site sont :

- les cookies de Sorare ;
- des cookies tiers gérés par les prestataires choisis par la société SORARE pour des finalités précises.

**4.2** Dans le cadre de l'utilisation des cookies telle que décrite sur la présente page, la Société Sorare est susceptible de traiter des Données Personnelles vous concernant, telles que des adresses IP ou des identifiants cookies, en sa qualité de responsable de traitement.
**4.3** Les cookies tiers présents sur le Site sont émis par des sous-traitants externes, qui sont susceptibles, si vous acceptez ces cookies, de traiter des Données Personnelles vous concernant.
**4.4** L'émission et l'utilisation de ces cookies par des tiers sont soumises à leurs propres politiques de confidentialité. Pour plus d'informations sur ces processus, vous pouvez vous référer à leurs politiques de confidentialité.
**4.5** Les données collectées sont indispensables pour atteindre les finalités visées par chaque cookie. Elles sont uniquement destinées aux services habilités de Sorare et/ou de la Société émettrice des cookies tiers.
**4.6** Les Données Personnelles vous concernant peuvent être transmises à des prestataires de services situés dans des pays tiers à l'Union européenne.
**4.7** Ces transferts de données sont régis par un accord de flux transfrontalier établi conformément aux clauses contractuelles émises par la Commission européenne et actuellement en vigueur.
**4.8** Les Données Personnelles collectées via les cookies ne sont jamais conservées par Sorare plus longtemps que nécessaire pour atteindre la finalité du cookie et, en tout état de cause, pas plus de 13 mois.
**4.9** Pour plus d'informations, y compris sur la manière d'exercer vos droits, consultez notre [Politique de confidentialité](https://sorare.com/privacy-policy).
**4.10** Le placement et l'utilisation de cookies par ces sociétés sont soumis à leurs propres conditions d'utilisation, que vous pouvez consulter sur leurs sites web respectifs. Vous trouverez, ci-dessous, la liste des sociétés gérant des cookies tiers sur le Site ainsi que leurs finalités et le lien vers les politiques de gestion des données personnelles de chacune des sociétés concernées:

- **4.10.1** Cookies utilisés à des fins de mesures de performance des campagnes publicitaires de Sorare pour la promotion de ses services et l’acquisition de nouveaux utilisateurs:

  - Google: [Google Tag Manager, Google Ads, Google Dynamic Retargeting, DoubleClick G](https://policies.google.com/privacy)
  - Twitter/X: [Twitter Advertising; Twitter Conversion Tracking](https://twitter.com/en/privacy)
  - Facebook: [Facebook Conversions API, Facebook Custom Audience](https://www.facebook.com/privacy/center/?entry_point=facebook_page_footer)
  - TikTok: [Tiktok Conversions API](https://www.tiktok.com/legal/page/eea/privacy-policy/en)
  - Snapchat: [Snapchat Conversions API](https://values.snap.com/en-GB/privacy/privacy-policy)

- **4.10.2** Cookie utilisés à des fins d’analyse et de mesure d’audience pour l’amélioration du Site et des services:

  - [Amplitude](https://amplitude.com/privacy)
  - Google: [Google Analytics 4 Cloud](https://policies.google.com/privacy)
  - Amazon: [AWS S3](https://aws.amazon.com/privacy/)

- **4.10.3** Cookie utilisés à des fins d'emailing marketing:

  - Braze: Braze Web Device Mode (Actions)

- **4.10.3** Cookie utilisés pour déclencher certains SMS et notifications Push:

  - Braze

- **4.10.5** Cookie essentiels au bon fonctionnement du Site:

  - [LaunchDarkly](https://launchdarkly.com/policies/privacy/)
  - [Stripe](https://stripe.com/en-gb-fr/privacy)

## 5. Partage de l'utilisation de votre appareil avec d'autres personnes

**5.1** Si votre équipement informatique est utilisé par plusieurs personnes, et/ou lorsque plusieurs navigateurs sont utilisés sur un même appareil, nous ne pouvons pas être certains que les services et contenus adressés à votre équipement informatique correspondent à votre propre utilisation de cet appareil et non à celle d'un autre Utilisateur de cet appareil.

**5.2** Le partage avec d'autres personnes de l'utilisation de votre équipement informatique et la configuration des paramètres de votre navigateur en matière de cookies relèvent de votre libre choix et de votre responsabilité.

## 6. Consentement

**6.1** Lors de votre première visite sur notre site, il vous sera demandé d'accepter, de configurer ou de refuser l'utilisation des cookies non strictement indispensables au fonctionnement du Site et des Services.

**6.2** Si vous ne souhaitez pas que des cookies soient placés ou lus sur votre équipement informatique, un cookie de refus sera placé sur votre équipement afin que SORARE puisse identifier ce refus à l’avenir. Veuillez noter que si vous supprimez ce cookie de refus, il ne sera plus possible à Sorare de savoir que vous souhaitez vous opposer à l'utilisation de cookies.

**6.3** De même, lorsque vous acceptez le dépôt de cookies, un cookie de consentement est placé.

**6.4** Les cookies de refus ou de consentement doivent rester sur votre équipement informatique. Vous pouvez modifier vos choix à tout moment en cliquant ici.

## 7. Gestion des cookies

**7.1** Vous pouvez à tout moment gérer et modifier l’usage des cookies sur votre terminal via l’une des options listées ci-dessous :

- directement sur notre Site, dans le module prévu à cet effet, en cliquant ici, ou
- depuis les réglages de votre navigateur internet, ou
- à partir des plateformes interprofessionnelles d'opposition.

**7.2** Veuillez noter que votre opposition aux cookies non-indispensables au fonctionnement du Site et des Services sera enregistrée au moyen d’un cookie déposé sur le navigateur en cours d’utilisation de votre terminal. Par conséquent, si vous supprimez les cookies sur votre appareil ou si vous changez d’appareil ou de navigateur, nous ne saurons plus que vous avez choisi cette option et il vous sera nécessaire de vous y opposer à nouveau.

**7.3 Module de gestion des cookies**

**7.4** Un module vous permet de choisir les cookies que vous souhaitez accepter et ceux que vous souhaitez refuser sur notre Site Internet.

**7.5** A tout moment, en cliquant ici, vous pouvez accéder au module et modifier vos préférences.

**7.6 Paramètres du logiciel de navigation**

**7.7** Vous pouvez configurer votre logiciel de navigation de manière à ce que les cookies soient stockés dans votre équipement informatique ou soient rejetés, soit systématiquement, soit en fonction de leur expéditeur.

**7.8** Vous pouvez également régulièrement supprimer les cookies de votre terminal via les réglages de votre navigateur.

**7.9** Veuillez noter qu’il est nécessaire de configurer vos choix sur tous les navigateurs de vos différents appareils (tablettes, smartphones, ordinateurs...) si vous souhaitez qu’ils soient pris en compte sur l’ensemble d’entre eux.

**7.10** Les réglages de chaque navigateur pour la gestion des cookies sont différents. Ceux-ci sont décrits en détail dans le menu d'aide de votre navigateur qui vous permettra de savoir comment modifier vos choix en matière de cookies. Par exemple :

- pour Chrome™: http://support.google.com/chrome/bin/answer.py?hl=fr&hlrm=en&answer=95647 ;
- pour Firefox™: http://support.mozilla.org/fr/kb/Activer%20et%20d%C3%A9sactiver%20les%20cookies ;
- pour Safari™ : https://support.apple.com/fr-fr/guide/safari/sfri11471/mac ;
- pour Edge™: https://support.microsoft.com/fr-fr/microsoft-edge/supprimer-les-cookies-dans-microsoft-edge-63947406-40ac-c3b8-57b9-2a946a29ae09 ;
- pour Opera™: http://help.opera.com/Windows/10.20/fr/cookies.html .

**7.11** Veuillez toutefois noter qu'en paramétrant votre navigateur pour qu'il rejette les cookies, certaines fonctionnalités, pages et zones du site Web qui nécessitent l'utilisation de cookies pourraient ne plus être accessibles ou ne plus fonctionner de manière optimale, ce dont nous ne pourrions être tenus responsables.

**7.12** Pour en savoir plus sur la manière de configurer votre navigateur, nous vous invitons à consulter le site de la Commission Nationale de l'Informatique et des Libertés : https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser.

**7.13 Les plateformes interprofessionnelles d'opposition**

**7.14** Plusieurs plateformes de professionnels de la publicité vous offrent également la possibilité de refuser ou d'accepter les cookies utilisés par les entreprises membres. Ces mécanismes centralisés ne bloquent pas l'affichage des publicités mais empêchent seulement l'installation des cookies qui permettent l'adaptation des publicités à vos intérêts et à votre comportement.

**7.15** Par exemple, vous pouvez vous connecter au site www.youronlinechoices.com afin d'empêcher l'installation de ces cookies sur votre appareil. Ce site est proposé par les professionnels de la publicité numérique réunis au sein de l'association européenne EDAA (European Digital Advertising Alliance) et géré en France par l'Interactive Advertising Bureau France.

**7.16** Vous pourrez prendre connaissance des sociétés inscrites sur cette plateforme et lesquelles vous offrent la possibilité de refuser ou d'accepter les cookies qu'elles utilisent pour adapter, en fonction des données qu'elles collectent, les publicités susceptibles de s'afficher sur votre navigateur : www.youronlinechoices.com/fr/controler-ses-cookies.

**7.17** Cette plateforme européenne est partagée par des centaines de professionnels de la publicité sur Internet et constitue une interface centralisée vous permettant d'exprimer votre refus ou votre acceptation des cookies comme précisé ci-dessus.
