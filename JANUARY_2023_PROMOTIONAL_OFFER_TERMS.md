# Sorare NBA Promotional Offer Terms and Conditions

These Sorare NBA Promotional Offer Terms and Conditions (“**Offer Terms**”), the Terms and
Conditions, accessible at [https://sorare.com/terms_and_conditions](https://sorare.com/terms_and_conditions) (collectively, the “T&amp;Cs”)
and the Privacy Policy accessible at [https://sorare.com/privacy_policy](https://sorare.com/privacy_policy) (the “Privacy Policy”),
apply to the promotional offer available on the Sorare NBA Primary Market (available at
[https://www.sorare.com/nba/market/primary](https://www.sorare.com/nba/market/primary)) as described below (the “**Offer**”). Capitalized terms
not directly defined here shall have the meaning given to them in the T&amp;Cs. In the event of any
inconsistency between these Offer Terms and the T&amp;Cs, these Offer Terms will prevail.

## Offer Description.

A select Eligible Users (as defined below) will be offered the one time
opportunity to claim a 25% (twenty-five percent) credit of up to US$25 (twenty-five United-States dollars) on
their first purchase on the Sorare NBA Primary Market (“**Credit**”).

## Offer Duration.

Offer is available for a limited time starting on February 6, 2023 at 9AM EST
(the “**Start Date**”) and ending on February 20, 2023 at 9AM EST (the “**End Date**”). Except
where prohibited by law, Sorare reserves the right to modify, suspend or terminate the Offer at
any time and for any reason without notice.
Eligible Purchases. Offer applies to the valid purchase on the Sorare NBA Primary Market of
either (i) an individual Collectible, or (ii) Collectibles in a bundle or starter pack by an “Eligible
User,” defined as a User who:

- has never purchased any Collectible on the Sorare platform;
- does not have a Credit that is currently applied to an outstanding bid in an active Auction.

When a Credit is used by an Eligible User, it is immediately deducted from the cost of the
Collectible’s purchase during the purchase process. The Credit will be automatically applied to
the Eligible User’s first purchase of a Collectible during the Eligibility Period, with the option to
save the Credit for a subsequent purchase within the Eligibility Period.  
Collectibles purchased via the Manager Sales Secondary Market are not eligible.

## Eligibility Period.

The Credit shall be used within the first 24 (twenty-four) hours after the Offer
was made available to an Eligible User (the “**Eligibility Period**”). Once expired, the Credit will no
longer be valid and cannot be used.  
For a fixed price purchase, the Credit is used as soon as the payment is processed. In the event
of a failed payment, the Credit can be reused for a subsequent Eligible Purchase during 24
(twenty-four) hours from the payment failure.  
For the purchase of an individual Collectible through Auction, the Credit is used when the bid is
the last, highest one and the payment is processed. If the Eligible User is outbid during the
Auction Period, the Credit can be reused during 24 (twenty-four) hours from the moment the
outbid occurs.

## Restrictions.

Collectible(s) (including those purchased in a bundle or starter pack) purchased
using the Offer by an Eligible User cannot be resold, transferred, or withdrawn from the relevant
Eligible User’s account for a period of seven (7) calendar days from the time of purchase. For
the avoidance of doubt, the Collectible(s) will be available for use in the Sorare NBA Game.
Offer may not be combined with other special offers, discounts, or promotions. Offer is not
transferable. Offer not valid for Sorare Football and Sorare: MLB Primary Market purchases.

## Miscellaneous.

The purchase of any Collectible(s) and the Offer are subject to these Offer
Terms, the T&amp;Cs, the Privacy Policy, and any other applicable additional terms. By participating
in this Offer, Users agree to (i) to be bound by these Offer Terms, the T&amp;Cs, and the Privacy
Policy, and (ii) to the extent permitted by applicable law, to release, discharge and hold
harmless, and waive any and all claims against Sorare, the NBA Entities and the NBPA Entities
(as both defined below), and each of their respective parents, affiliated companies, subsidiaries,
officers, directors, employees, general and limited partners, shareholders, members (including,
with respect to NBPA, all National Basketball Association players (“Players”), agents, licensees,
distributors, dealers, retailers, printers, representatives, advertising and promotion agencies, and
any other company associated with the Offer, and all of their respective officers, directors,
employees, agents, and representatives (collectively, “Released Parties”) for any injury,
damage, liability, or loss of any kind that may occur, directly or indirectly, in whole or in part, from
participation in the Offer. The releases hereunder are intended to apply to all claims not known
or suspected to exist with the intent of waiving the effect of laws requiring the intent to release
future unknown claims. “**NBA Entities**” means NBA Properties, Inc., the National Basketball
Association (the “NBA”), and the NBA member teams. “**NBPA Entities**” means National
Basketball Players Association, its affiliates and the individual members of the NBPA. The Offer
is subject to all applicable laws, rules, and regulations. Void where prohibited or restricted by
law, rule, or regulation.

This Offer is in no way sponsored by any of the NBA Entities or NBPA Entities. NBA trademarks
and copyrights are used with permission of the NBA. NBPA trademarks and copyrights are used
with permissions of the NBPA. All rights reserved.
