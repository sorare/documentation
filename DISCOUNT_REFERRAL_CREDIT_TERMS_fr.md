
# Conditions du programme de parrainage
_Dernière mise à jour : 12 février 2025_

Les présentes Conditions Générales du Programme de Parrainage (les "**Conditions de parrainage**") régissent votre éligibilité au programme de parrainage de Sorare.com et votre utilisation des Crédits de Parrainage (tels que définis ci-dessous) sur le Marché Primaire de Sorare.com (c'est-à-dire les Enchères et l'Achat Immédiat uniquement) pour l'achat de Cartes à Collectionner. Pour participer au Programme de parrainage, vous devez accepter les Conditions générales de Sorare et les présentes Conditions de parrainage. Les termes en majuscules qui ne sont pas définis ici auront la signification qui leur est donnée dans les Conditions générales et/ou les Conditions supplémentaires applicables.

## 1. Programme de parrainage

**1.1**. Vous pouvez recommander à d'autres utilisateurs de s'inscrire à un compte d'utilisateur sur le Site web de Sorare en participant au programme de parrainage régi par les présentes Conditions de parrainage (le "**Programme de parrainage**"). Le Programme de Parrainage offre aux Utilisateurs (le "**Parrain**") la possibilité de gagner des récompenses en invitant d'autres personnes (le "**Parrainé**") à utiliser les Services.

## 2. Éligibilité des parrains

**2.1**. Pour pouvoir participer au Programme de parrainage, vous devez :      

- disposer d'un compte valide sur le site Web qui soit conforme aux Conditions générales ;
- respecter les présentes Conditions de parrainage ;
- générer un lien de parrainage à partir du site web https://sorare.com/invite (le "**Lien de parrainage**").

**2.2**. Pour éviter toute ambiguïté, l'envoi, le partage et/ou la publication d’un Lien de parrainage dans un lieu public, ou la publicité et/ou la distribution massive de ce dernier par le biais de messages sur les médias sociaux, d'e-mails ou d'applications de messagerie (y compris à l'aide de publicités payantes), afin de tirer profit du Programme de parrainage, peut constituer une violation des présentes Conditions de parrainage. Pour de plus amples informations, veuillez vous reporter à la section 4 "Violation des conditions de parrainage".

## 3. Parrainages valides

**3.1**. Pour être considérée comme valide ("**Parrainage valide**"), le parrainage doit remplir les conditions suivantes :

1. le Parrainé doit être une personne physique, âgée d'au moins dix-huit (18) ans et distincte du Parrain, qui n'a jamais créé de compte Utilisateur Sorare et qui n'est pas en train de créer un compte Utilisateur.
2. Le Parrainé doit:           
a) créer un compte en accédant au Site web par le biais du Lien de parrainage du Parrain ;           
b) vérifier son compte Utilisateur;          
c) accepter les Conditions générales de Sorare;           
d) acheter cinq (5) Cartes à collectionner du sport concerné via les Enchères ou les Achats immédiats (ou sept (7) pour Sorare: MLB), dans les 30 jours suivant la création d'un compte Utilisateur. Pour éviter toute ambiguïté, les achats sur le marché secondaire par un Parrainé (c'est-à-dire par le biais de ventes de managers) ne seront pas pris en compte pour qualifier un Parrainage valide.
3. Le parrain et le parrainé doivent valider leur numéro de téléphone dans les paramètres de leur compte Utilisateur. 

**3.2. Récompenses de parrainage**.

**3.2.1**. Dans le cas d'un Parrainage valide, le Parrain et le Parrainé reçoivent tous deux un Crédit de parrainage ("**Crédit(s)**"). Sauf en cas de retard dû à des raisons techniques, les Crédits offerts en récompense sont ajoutés aux comptes d'Utilisateur du Parrain et du Parrainé (et sont accessibles via la section "Mon Portefeuille Sorare" depuis le Site Web ou l'Application) dans un délai de quatorze (14) jours à compter de la date de validité du Parrainage (le “**Délai de réclamation**”).

**3.2.2**. Le Délai de réclamation peut être réduit si le Parrainé participe à deux (2) Game Weeks distinctes en utilisant des Équipes qui comprennent chacune au moins une (1) Carte à collectionner qu'il a acheté pour valider le parrainage.  Dans ce cas, et à l'exception d’éventuels retards dus à des raisons techniques, tous les Crédits gagnés en récompense seront ajoutés aux comptes Utilisateurs du Parrain et du Parrainé à la clôture de la seconde Game Week jouée. 

**3.3. Utilisation des Crédits**.

**Crédits applicables**. Les Crédits sont une réduction de cinquante pour cent (50%) sur le marché primaire pour l’achat de Cartes à collectionner de Sorare.com (ventes aux enchères ou achats immédiats), jusqu'à un certain montant en devise, comme indiqué. Par exemple, un crédit de 50€ permettrait à un Utilisateur d'acheter jusqu'à 100€ de Cartes à collectionner avec une remise totale de 50€, ce qui donnerait un prix final de 50€. La devise applicable aux Crédits dépend de la Devise Préférée de l'Utilisateur.

**Dépenses minimales**. Sorare, à sa seule discrétion, peut désigner un montant minimum d'achat pour lequel un Crédit peut être appliqué par compte d'Utilisateur et/ou par transaction. Dans le cas où un montant minimum d'achat est applicable, il vous sera indiqué.

**Utilisation maximale**. Sorare, à sa seule discrétion, peut désigner une limite maximale sur le montant d'un Crédit pouvant être appliqué par compte d'Utilisateur et/ou par transaction. Sauf indication contraire, les Crédits peuvent être utilisés pour des achats multiples avant la Date d'Expiration (telle que définie ci-dessous) jusqu'à un certain montant en Crédits (par exemple 50€). Par exemple, si un utilisateur achète une Carte à collectionner coûtant 10€ avec un Crédit de 50€, le Crédit ne s'applique qu'à 5€ et l'Utilisateur pourra réutiliser le Crédit pour des achats futurs jusqu'à 45€ avant la Date d'Expiration.

**Expiration**. Les crédits expirent trente (30) jours après avoir été mis à la disposition d'un utilisateur (la "**Date d'Expiration**"). Nonobstant ce qui précède, chaque fois que des Crédits supplémentaires sont mis à la disposition d'un Utilisateur, la Date d'Expiration est réinitialisée de sorte que l'Utilisateur dispose de trente (30) jours à compter de la date la plus récente à laquelle un Crédit a été gagné pour utiliser le nombre de Crédits accumulés. Par exemple, si un Utilisateur gagne un crédit de 50€ qui expire dans cinq jours, et qu'il gagne ensuite un crédit de 5€ avant la Date d'Expiration, cet Utilisateur aura maintenant un Crédit de 55€, qui expire dans trente (30) jours. En n'utilisant pas votre/vos Crédit(s) avant la Date d'Expiration, vous comprenez et acceptez que vous avez renoncé au droit d'utiliser ce(s) Crédit(s) et que vous n'avez pas droit à, et ne recevrez pas, de Crédit(s) supplémentaire(s) ou alternatif(s), ni aucune autre compensation ou rémunération.

**Restrictions**. Les Crédits n'ont pas de valeur monétaire, ils ne sont pas échangeables contre de l'argent, de l'ETH, ou d'autres articles sur Sorare.com, y compris les articles du Club Shop ou d'autres prix disponibles. Les Crédits sont personnels, non remboursables et non transférables et ne peuvent être échangés que comme indiqué dans ces Conditions de parrainage, et ne peuvent pas être échangés sur le Marché de Sorare.com. Sauf indication contraire, les Crédits ne peuvent pas être combinés avec d'autres offres ou réductions disponibles sur Sorare.com ou proposées par Sorare.com. Les Crédits ne peuvent pas être achetés contre de l'argent et ne peuvent pas être vendus. Les objets de collection achetés avec un Crédit ne peuvent pas être vendus ou échangés sur le Marché Sorare.com pendant une certaine période de temps (par exemple 14 jours), comme indiqué sur le Site Web ou l'Application ("**Délai de Changement de Propriété**"). Sorare se réserve le droit de modifier le Délai de Changement de Propriété à tout moment, et pour toute raison, à sa seule discrétion.

## 4. Violation des conditions de parrainage

**4.1**. En cas de fraude et/ou d'abus réels, suspectés et/ou tentés, de violation des présentes Conditions de parrainage et/ou des Conditions générales de Sorare, Sorare se réserve le droit de limiter l'accès de l'Utilisateur concerné aux Services et/ou de participer au Programme de parrainage et/ou de recevoir les Récompenses de parrainage, et d'entreprendre d'autres actions en relation avec le(s) compte(s) de l'Utilisateur concerné conformément aux Conditions générales de Sorare, sans préjudice de toute action en justice qui pourrait être entreprise par Sorare à l'encontre de l'Utilisateur concerné.

**4.2**. Les Crédits gagnés en violation des présentes Conditions de parrainage et/ou des Conditions générales de Sorare seront invalidés et considérés comme nuls, quel que soit le temps restant disponible avant la Date d'Expiration, et le Parrain concerné sera considéré comme inéligible pour toute Récompense de parrainage future. 

## 5. Divers

**5.1**. Sorare se réserve le droit de demander aux Utilisateurs du programme de parrainage de passer au programme d'affiliation si leurs performances dépassent le cadre et l'objectif du Programme de parrainage.

**5.2**. Les conditions et avantages énoncés dans les présentes Conditions de parrainage peuvent, à tout moment et sans préavis, être mis à jour unilatéralement. La poursuite de la participation au Programme de parrainage après une mise à jour vaut acceptation des conditions mises à jour.

**5.3**. Sorare se réserve le droit, à sa seule discrétion, de modifier, de limiter ou d'interrompre le Programme de parrainage à tout moment, sans pénalité ni obligation supplémentaire à votre égard.

