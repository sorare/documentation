# PRIVACY POLICY

September 2022

Version 1.0

## 1. Avant-propos

Sorare est une plateforme de divertissement sportif où les fans de sport ("vous" ou "Utilisateur") peuvent s'engager avec leurs athlètes et équipes préférés et créer des liens avec la communauté sportive mondiale.

Nous respectons votre vie privée et nous nous engageons à mettre à disposition de nos Utilisateurs un environnement sécurisé. Cette politique de confidentialité ("**Politique**") est destinée à vous fournir des informations importantes sur la manière et les raisons pour lesquelles vos données peuvent être collectées et traitées par Sorare ("**Nous**" ou "**Sorare**"). Vos données seront traitées conformément à la loi applicable, y compris le Règlement Général sur la Protection des Données de l'Union Européenne (“RGPD”), la loi fédérale et la loi des États des États-Unis régissant la protection des données personnelles, et toutes autres lois applicables en matière de protection des données à caractère personnel (“Données Personnelles”). La présente politique fait partie intégrante des [Conditions Générales](https://sorare.com/terms_and_conditions) de Sorare, tel qu’énoncé directement dans ces dernières. Les termes en majuscules qui ne sont pas définis dans la présente Politique ont la signification qui leur est donnée dans les Conditions Générales.

## 2. Données collectées et finalités de traitements

Nous collectons et traitons une quantité limitée de Données Personnelles à des fins spécifiques et déterminées, telles que pour vous permettre d'accéder aux Services, de rendre les Services faciles et agréables à utiliser, d’améliorer votre expérience et d’identifier et de prévenir la fraude. Chaque fois que nous collectons vos Données personnelles, et quelles que soient la ou les finalités poursuivies, nous nous engageons à toujours collecter le minimum de Données nécessaires à la poursuite de la finalité concernée.

Cette section a pour but de vous fournir plus d'informations concernant les Données Personnelles que nous collectons, les finalités pour lesquelles nous traitons ces Données Personnelles et la base légale sur laquelle repose chaque traitement spécifique.

- **Accès et utilisation des Services**. Sorare collecte et traite vos Données Personnelles afin de vous permettre d'accéder et d'utiliser ses différents Services, y compris pour faciliter l'achat d'une Carte à Collectionner, jouer au Jeu, ou participer à nos Programmes de Parrainage ou de Partenariat.

  - **Pour acheter une Carte à Collectionner** par le biais de ventes aux enchères proposées par Sorare et/ou pour acheter, transférer, retirer et/ou échanger une Carte à Collectionner via le Marché, Sorare recueille votre nom d'utilisateur, votre numéro de téléphone, votre clé publique de portefeuille ou votre identifiant de paiement et a accès à une partie de votre numéro de carte de crédit (c'est-à-dire les quatre derniers chiffres de la carte de crédit auxquels nous avons accès par l'intermédiaire de nos fournisseurs de services de paiement mais que nous ne recueillons pas et ne stockons pas directement).
  - **Pour faciliter la participation à un Tournoi**, remettre des récompenses aux gagnants, publier des classements, et plus encore, Sorare traite votre nom d'utilisateur, votre numéro de téléphone, la clé publique de votre portefeuille et le(s) prix que vous avez gagné(s) (le cas échéant). Sorare peut publier votre nom d'utilisateur et le(s) prix que vous avez gagné(s) sur son site Web, dans le cadre du classement présentant les résultats des tournois.
  - **Pour permettre la participation à notre programme d'affiliation ou de parrainage**, Sorare collecte et traite votre adresse électronique, votre numéro de téléphone, votre nom d'utilisateur et votre clé publique de portefeuille.
  - La base légale de ce traitement est contractuelle. Nous avons besoin de ces données afin d'exécuter les Conditions générales, et toute autre condition supplémentaire applicable, que vous avez acceptées lors de la signature de votre compte utilisateur
  - **Pour jouer directement avec vos amis Utilisateurs du Jeu sur l’Application**, Sorare pourra accéder à votre carnet de contacts téléphonique avec votre consentement. A cette fin, Sorare pourra partager votre nom d'utilisateur aux Utilisateurs du Jeu qui disposent d’ores et déjà de votre numéro téléphonique parmi leurs contacts. Cette possibilité peut être changée à tout moment dans vos réglages utilisateurs.
  - La base légale de ce traitement repose sur votre consentement.

- **Respect des conditions** ; sécurité du site web et du compte. Sorare recueille certaines informations sur votre appareil et votre navigateur (“user agent” ou agent utilisateur en français), vos données de navigation sur son site, y compris votre adresse IP (à partir de laquelle des informations sur votre pays/état de localisation peuvent être déduites), vos informations de compte (y compris votre adresse e-mail et votre numéro de téléphone) et les détails de vos transactions, afin d'assurer la sécurité des Services et de votre compte Utilisateur, et de détecter, prévenir, investiguer et/ou identifier toute tentative de violation de nos Conditions Générales ou de toute autre condition applicable, comme par un piratage malveillant. Votre numéro de téléphone pourra être utilisé par Sorare à des fins d’authentification de compte par SMS effectuée par nos partenaires. Votre numéro de téléphone ne sera aucunement partagé ou vendu à des tiers à des fins marketing. Au cours d'une investigation, Sorare peut prendre des mesures de sécurité supplémentaires, y compris, mais sans s'y limiter, en demandant des pièces justificatives et/ou des informations supplémentaires, ou en suspendant ou en annulant la transaction concernée dans le cas où la transaction ne peut pas être vérifiée. Lorsqu’une fraude au paiement est avérée après investigation, les Données Personnelles pertinentes et d’autres informations relatives aux faits concernés seront stockées dans le système de surveillance de Sorare et pourront être utilisées pour des vérifications futures.

  - La base légale de ce traitement est l'intérêt légitime de Sorare. Sorare a un intérêt légitime à détecter, prévenir et investiguer sur les fraudes et les tentatives de fraude pouvant affecter ses Services afin de garantir un jeu équitable pour tous les Utilisateurs et de protéger ses Services et ses Utilisateurs contre d'éventuelles fraudes à la carte de crédit ou autres.

- **Demandes et communications des utilisateurs**. Sorare collecte et traite des Données Personnelles telles que votre nom d'utilisateur, vos coordonnées de contact et le contenu de votre message (dans la mesure où celui-ci pourrait contenir des Donnée Personnelle vous concernant vous ou un tiers) lors de la gestion et du suivi des questions et/ou demandes que vous pouvez soumettre via notre Help Center ou toute autre méthode.

  - La base légale de ce traitement est contractuelle. L'assistance et le support Utilisateur fait partie intégrante des Services que nous fournissons, de sorte que nous avons besoin de ces données pour exécuter les Conditions générales, et toutes autres conditions supplémentaires applicables, que vous avez acceptées ou pourriez accepter lors de la création de votre compte Utilisateur et/ou de votre utilisation des Services.

## 3. La technologie blockchain et vos données

- **NFT.** La technologie Blockchain (ou chaîne de blocs) est au cœur des Services que Sorare fournit. Nos Cartes à Collectionner reposent sur cette technologie sous forme de jetons non fongibles (“NFT” ou “JNF” en français) émis par Sorare et qui permettent d’assurer l'authenticité, la rareté et l'interopérabilité de vos Cartes à Collectionner.

- **Layer 2 et ZKP.** Pour vous offrir la meilleure expérience possible, Sorare s'est associé à Starkware afin de bâtir une infrastructure de deuxième niveau (“Layer 2”) reposant sur une technologie de preuve à divulgation nulle de connaissance (Zero Knowledge Proof ou " ZKP " en anglais) en complément de la blockchain publique Ethereum.
  Cette infrastructure permet à Sorare de bénéficier d’un second niveau complémentaire de la blockchain publique Ethereum de niveau 1. Le fonctionnement de cette technologie repose sur une méthode de traitement des données ZKP qui traite les données de transaction sur une blockchain privée de niveau 2, met à jour l’état du second niveau puis inscrit l’état compressé du second niveau sur la blockchain Ethereum publique de niveau 1 pour référence.
  Du point de vue de la protection des données, ce mécanisme génère des reçus cryptographiques qui confirment l'authenticité des données de transaction sans publier ni stocker les données personnelles des participants à la transaction sur la blockchain publique Ethereum de niveau 1.

- **Vos données.** Votre clé publique de portefeuille et les informations relatives à vos achats, transferts, retraits et échanges de Cartes à Collectionner sont stockées sur nos serveurs AWS, par Starkware qui fournit le second niveau décrit ci-dessus et par notre comité de disponibilité des données dont les membres assurent la sauvegarde de ces informations et sont soumis à des obligations de confidentialité strictes. Aucune donnée personnelle de nos Utilisateurs n'est stockée par Sorare sur la blockchain publique Ethereum. Toutefois, veuillez noter que si vous retirez vos fonds ou une Carte à Collectionner de la plateforme Sorare (par exemple pour les transférer vers un autre service ou vers un autre portefeuille externe), ce transfert impliquera le stockage des informations liées à ce transfert sur une blockchain publique. Tous les transferts vers des portefeuilles externes sont soumis à la politique de confidentialité et/ou aux conditions de service du service tiers concerné, veillez à consulter les politiques et pratiques de confidentialité des tiers concernés avant de procéder à ce transfert. Comme indiqué dans nos conditions générales, Sorare ne sera pas tenu responsable du contenu ou du service d'un tiers et décline toute responsabilité pour tous les services fournis à un utilisateur par un fournisseur de services tiers.
  - La base légale de ce traitement est contractuelle. La blockchain est au cœur de nos services. Ces technologies permettent à Sorare d’assurer l’authenticité, la rareté et l’interopérabilité de vos Cartes à Collectionner.

## 4. Conservation des données

- **Informations du compte Utilisateur**. Sorare conservera vos Données Personnelles jusqu'à la clôture de votre compte. Toutefois, lorsque la loi applicable l'exige ou l'autorise, si Sorare doit conserver certaines Données Personnelles au-delà de la date de clôture de votre compte, alors la période de conservation maximale applicable s'appliquera conformément aux délais de prescription légale applicables. Cette durée n'excédera pas la durée pendant laquelle la Société doit légalement conserver les Données.

- **Utilisation du site Internet**. Les Données Personnelles traitées par ou pour le compte de Sorare à des fins d'analyse ou d'autres études ou analyses ne seront pas conservées par Sorare après que ces études aient été achevées et que les rapports, résultats, statistiques et/ou aperçus pertinents aient été produits.

- **Accès aux Services**. Les Données personnelles traitées pour l'utilisation des Services ne seront conservées que pendant quinze (15) mois à compter de la date de l'activité concernée.

- **Technologie Blockchain**. Les Données personnelles traitées et enregistrées sur une blockchain dans le cadre de vos transactions sur Sorare seront stockées de manière immuable. Veuillez consulter la section " Technologie blockchain " ci-dessus pour mieux comprendre quelles catégories d'informations sont enregistrées sur la blockchain publique Ethereum et quelles mesures ont été mises en œuvre par Sorare pour protéger la confidentialité de vos Données Personnelles.

- **Respect des conditions ; sécurité du site web et du compte**. Les Données à caractère personnel traitées pour la prévention de la fraude seront conservées pendant trois (3) ans après que les données concernées aient été incluses dans une liste d'alerte. Les Données personnelles traitées pour des questions de sécurité seront conservées pendant cinq (5) ans à compter de la dernière connexion au compte ou de la dernière activité suspecte.

- **Demandes et communications des utilisateurs**. Pour la gestion de vos questions et/ou demandes, Sorare conservera vos Données Personnelles pendant un maximum de trois (3) ans à compter du dernier contact dont vous êtes à l’initiative.

Nonobstant ce qui précède, Sorare peut conserver certaines Données Personnelles pour une durée plus longue en vertu de ses intérêts légitimes et des obligations légales applicables.
Lorsque la période de conservation pertinente expire, Sorare supprimera vos Données Personnelles ou appliquera des mesures adaptées aux données pertinentes afin de s'assurer que vous ne puissiez plus être identifié.

Si Sorare conclut qu'il n'est pas nécessaire de conserver vos Données Personnelles dans sa base de données active, elle archivera les Données concernées. Pour toutes les Données actives et archivées, Sorare veillera à ce que l'accès aux Données Personnelles pertinentes soit limité à un nombre restreint de personnes ayant un besoin réel d'y accéder.

## 5. Sous-traitants et transfert de données

Sorare peut utiliser des fournisseurs de services tiers ("Sous-Traitants") pour faciliter, maintenir, améliorer, analyser et sécuriser le site Web, l'App et les Services. Ces Sous-traitants peuvent avoir accès à certaines Données Personnelles dans le seul et unique but de réaliser le traitement spécifique assigné à un Sous-traitant par Sorare. Sorare s'assure que les prestataires présentent des garanties suffisantes pour l'exécution de l'activité de traitement concernée et leur impose de respecter les lois et règlements applicables en matière de protection des données personnelles.

Vos Données Personnelles peuvent être traitées en dehors de l'Espace Economique Européen ("EEE"). Lorsque les Données sont transférées en dehors de l'EEE, Sorare prend toutes les précautions nécessaires, et alternativement ou cumulativement : (i) s'assure que la Commission européenne a pris une décision d'adéquation concernant le pays de destination, (ii) que des clauses contractuelles types adoptées par la Commission européenne ou l'autorité de contrôle ont été signées avec le destinataire, (iii) que le destinataire a adhéré à un code de conduite ou un mécanisme de certification approuvé.

## 6. Partage des données

La Société partage vos données personnelles avec des prestataires de services tiers et des fournisseurs qui aident la Société à remplir les objectifs spécifiés dans la présente politique.

**L'API de Sorare**

Chez Sorare, nous nous engageons à fournir une plateforme ouverte pour notre communauté d'utilisateurs et de développeurs afin de construire de nouveaux services, jeux ou applications. À cette fin, nous fournissons un accès à une interface de programmation d'applications (API) permettant aux Utilisateurs authentifiés et autorisés d'accéder plus facilement à certaines informations publiques des Utilisateurs publiées sur le site Web de Sorare :

- Les informations de base de l'Utilisateur, notamment son nom d'Utilisateur, son avatar et l'adresse publique de son portefeuille,
- Les cartes, les succès et les favoris de l'Utilisateur,
- Les enchères, les offres, les achats, les ventes et les notifications des Utilisateurs.

Veuillez noter que les informations suivantes ne sont jamais partagées via notre API :

- Votre adresse électronique,
- Les futures compositions d’équipes et,
- Les récompenses obtenues.

**Services tiers**

- Certains contenus ou fonctionnalités du Site Web et/ou de l'application mobile sont fournis par des prestataires de services tiers. Afin de faciliter votre accès à ces contenus et/ou fonctionnalités, Sorare peut partager certaines de vos Données Personnelles avec ces prestataires tiers. Ces services peuvent exiger que vous fournissiez des données supplémentaires (notamment pour les services de paiement accessibles sur le Site Web). Qu'ils soient ou non intégrés directement au Site Internet et/ou à l'Application, ces services tiers collectent et traitent vos Données Personnelles de manière indépendante conformément à leur politique de confidentialité pertinente et sous la seule responsabilité du prestataire de services tiers concerné.
- Veuillez noter que les accords contractuels entre Sorare et ces prestataires tiers (tels que les prestataires de services de paiement ou les partenaires marketing) précisent que Sorare et le prestataire tiers agiront tous deux en tant que "responsable de traitements indépendants" au sens du RGPD et devront se conformer à toutes les lois et réglementations applicables en matière de protection des données. Veuillez consulter la politique de confidentialité pertinente du Service tiers concerné pour plus d'informations.

**Ligues, équipes et clubs sportifs partenaires**

- Les ligues, équipes et clubs sportifs partenaires de Sorare ("**Partenaire(s)**") rendent nos Services uniques. Sorare peut partager votre adresse e-mail et votre nom d'utilisateur avec ces Partenaires de confiance à des fins de marketing direct et de promotion propres au Partenaire concerné. Lors de la création de votre compte Utilisateur, vous pouvez choisir si vous consentez ou non à ce que nous partagions ces données avec nos Partenaires. Cela dépend entièrement de vous - si vous ne consentez pas, nous ne partagerons pas vos Données.
- Si vous consentez à ce partage, veuillez noter que les dispositions contractuelles entre Sorare et ses Partenaires précisent que tant Sorare que chaque Partenaire agira en tant que "responsable de traitement indépendant" tel que défini par le RGPD et devra se conformer à toutes les lois et réglementations applicables en matière de protection des données. Veuillez consulter la politique de confidentialité pertinente du Partenaire concerné pour plus d'informations.

**Autorités judiciaires et gouvernementales**

- Lorsque la loi ou une réquisition judiciaire valide l'exige et dans la mesure où cela est légalement autorisé, Sorare peut partager vos Données personnelles avec les tribunaux compétents et/ou toute autre autorité gouvernementale et/ou publique demandant l'accès à vos Données personnelles. Soyez assuré que Sorare vérifie toujours la légitimité de telles demandes.

Nous prenons le partage de vos Données Personnelles au sérieux. Chaque fois que nous partageons vos Données personnelles avec l'un des destinataires mentionnés au présent article, nous nous assurons préalablement que cela est absolument nécessaire à la poursuite de la finalité du traitement concerné.

## 7. Utilisation par les enfants

Le Site Web, l'Application et les Services sont destinés à être utilisés par des personnes âgées de dix-huit (18) ans ou plus. Conformément aux Conditions Générales, les Utilisateurs de moins de 18 ans ne peuvent pas accéder au Site Web, à l'Application et/ou aux Services.

## 8. Vos droits

La plupart des lois applicables en matière de protection des données (y compris le RGPD, et les lois de certains États américains tel que le California Consumer Privacy Act notamment) accordent aux individus des droits en rapport avec leurs Données Personnelles et la manière dont ces Données sont traitées. La section ci-dessous est destinée à vous informer des droits dont vous bénéficiez en rapport avec le traitement de vos Données Personnelles. Veuillez noter que toutes les demandes formulées en vertu de la présente section seront traitées conformément aux lois européennes applicables (notamment le RGPD), indépendamment de votre lieu ou pays de résidence. Nos réponses pourront toutefois être adaptées pour se conformer aux exigences des lois locales éventuellement applicables.

Vous avez le droit d'accéder à vos Données Personnelles, de les corriger, de les déplacer, de les supprimer et un droit de restreindre la manière dont vos Données sont traitées.
Vous avez également le droit de nous adresser des instructions définissant la manière dont vos Données Personnelles devraient être gérées après votre décès.

**Demandes**

- Pour exercer vos droits ou nous poser une question concernant les pratiques de Sorare en matière de confidentialité des données, vous pouvez faire une demande accompagnée d'un justificatif d'identité par courrier adressé à Sorare - 5 avenue du Général de Gaulle 94160 SAINT-MANDÉ FRANCE ou par email à [privacy@sorare.com](mailto:privacy@sorare.com).
- Sorare s'efforcera de répondre à votre demande ou requête dans les meilleurs délais, et au plus tard dans un délai d'un (1) mois à compter de la réception de la demande. Pour les demandes complexes, nous nous réservons le droit d'étendre ce délai à trois (3) mois.
- Les personnes physiques situées en France ont également la possibilité de soumettre une réclamation à la Commission Nationale Informatique et Libertés ("CNIL"). Les personnes situées dans un autre Etat Membre de l’Union Européenne peuvent soumettre une demande à l'autorité compétente en matière de protection des données dans leur pays de résidence.

**Suppression du compte d'utilisateur**

- Si vous souhaitez demander la suppression de vos Données Personnelles, vous pouvez utiliser la méthode décrite ci-dessus ou simplement demander la suppression de votre compte Utilisateur via les paramètres de votre compte Utilisateur, tel que décrit dans les Conditions Générales de Sorare.
- La suppression de votre compte entraînera la suppression irréversible des Données Personnelles associées au compte. Afin de garantir les respects de vos droits sans altérer l'historique et la traçabilité de chaque Carte à Collectionner dont vous étiez propriétaire, le nom d'utilisateur associé à la propriété de ces Cartes à Collectionner sera pseudonymisé et remplacé par une série aléatoire de caractères alphanumériques.
- Il est indispensable que vous sauvegardiez toutes les Cartes à Collectionner et crypto-monnaies stockées sur votre Portefeuille dans un portefeuille externe avant toute demande de suppression. Toutes Cartes à Collectionner et/ou crypto-monnaie stockées sur votre Portefeuille qui n'auraient pas été transférées hors de votre compte à la date de suppression sera définitivement irrécupérable. Sorare ne sera pas responsable de la perte définitive de toutes Cartes à Collectionner et/ou crypto-monnaie qui n'a pas été sauvegardé avant une demande de suppression.

La protection de vos Données Personnelles et le respect du cadre légal applicable en matière de protection des données sont deux priorités pour Sorare et nous avons besoin de votre aide pour y parvenir. En acceptant cette politique, vous acceptez de maintenir à jour vos Données Personnelles sur Sorare et de ne fournir que des Données Personnelles exactes et à jour. En outre, si vous nous envoyez des informations qui nous permettraient d'identifier, directement ou indirectement, d'autres personnes physiques, vous déclarez et garantissez que, avant de partager ces informations, la présente Politique a été communiquée à ces autres personnes physiques et, dans la mesure où cela est applicable, que celles-ci ont consenti au traitement de leurs Données Personnelles.

## 9. Cookies

- Lorsque vous naviguez sur notre Site Internet, des cookies sont déposés sur votre terminal, soit directement, soit après avoir recueilli votre consentement lorsque la réglementation sur les cookies l'exige.
- Les cookies sont des éléments d'information déposés sur l'équipement terminal de l'internaute par le serveur du site Internet visité. Ils sont utilisés par un site web pour envoyer des informations au navigateur de l'internaute et permettre à ce navigateur de renvoyer des informations au site web d'origine (par exemple, un identifiant de session ou le choix d'une langue).
- En théorie, seul l'émetteur d'un cookie peut lire ou modifier les informations qu'il contient.
- Il existe différents types de cookies :
  - les cookies de session qui disparaissent dès que vous quittez le site Web ;
  - les cookies permanents qui restent sur votre terminal jusqu'à l'expiration de leur durée de vie ou jusqu'à ce que vous les supprimiez à l'aide des fonctionnalités de votre navigateur.
- Vous êtes informé que, lors de vos visites sur le Site Internet, des cookies peuvent être installés sur votre équipement terminal.
  Pour en savoir plus sur l'utilisation des cookies sur notre Site Internet, veuillez consulter la politique de cookies ici : [Politique de cookies de Sorare](https://sorare.com/cookie_policy).

## 10. Sécurité

Sorare a mis en place et maintient des mesures visant à protéger la confidentialité, la sécurité et l'intégrité de votre compte Utilisateur et de vos Données Personnelles contre tout accès et divulgation non autorisés, toute modification, altération, dommage, perte accidentelle ou destruction accidentelle ou illicite, ainsi que contre toute autre forme de traitement ou de divulgation non autorisée.

Nous vous recommandons de respecter les meilleures pratiques en matière de sécurité, notamment en stockant les informations de votre compte de manière sécurisée et en ne partageant pas les informations de votre compte. Veuillez noter que nos services de sauvegarde n'effectueront pas une sauvegarde de vos données sur votre stockage ou lecteur personnel et local ; nous vous recommandons d'utiliser une source de sauvegarde secondaire et/ou d'autres bonnes pratiques pour la préservation des données. En outre, nous vous recommandons d'activer l'authentification à deux facteurs, de choisir un mot de passe fort et/ou d'utiliser un gestionnaire de mots de passe pour toutes les fonctionnalités protégées par mot de passe et pour votre compte Utilisateur lui-même.

Sorare reconnaît l'importance et la valeur des efforts réalisés par les chercheurs en sécurité pour nous aider à assurer et maintenir la sécurité de notre communauté. Nous encourageons la divulgation responsable des vulnérabilités de sécurité via notre programme de “bug bounty” décrit sur cette [page](https://hackerone.com/sorare).

## 11. Mises à jour de la présente politique de confidentialité

Sorare se réserve le droit de modifier la présente politique si nécessaire, au fur et à mesure que le Service évolue ou si les lois applicables l'exigent. Toute modification importante de la politique vous sera communiquée.
Lorsque vous utilisez les Services, vous êtes réputé avoir accepté les termes de la Politique, tels que publiés et mis à jour sur notre Site Web.

## 12. Juridiction et loi applicable

La présente Politique est régie et interprétée conformément au droit français et européen. Toutefois, si vous êtes un consommateur et un résident d'un État Membre de l'Union Européenne, vous bénéficiez des dispositions obligatoires et des droits qui vous sont reconnus par les lois de votre pays de résidence. A ce titre, rien dans la présente Politique n'affecte vos droits, en tant que consommateur, de vous appuyer sur les dispositions et droits obligatoires qui vous sont accordés par votre législation locale.

Lorsque la loi applicable le permet ou l'exige, vous acceptez que les tribunaux français soient les seuls compétents. Toutefois, si vous êtes un consommateur et un résident de tout autre membre de l'Union européenne, vous et Sorare pouvez également engager des procédures dans ce pays.

---

<a id="sms-terms-and-conditions"></a>

# Conditions générales d'utilisation de SMS

Décembre 2023

Version 1.0

Les présentes conditions d'utilisation de SMS ("Conditions") régissent l'utilisation par Sorare SAS ("Sorare") de services de messages courts (SMS) à des fins de vérification de compte et de détection de la fraude. En fournissant votre numéro de téléphone mobile et en acceptant de recevoir des messages SMS, vous acceptez ces conditions. Si vous n'êtes pas d'accord, veuillez ne pas utiliser le service SMS.

## 1. Consentement à la réception de messages SMS
   En fournissant votre numéro de téléphone mobile, vous acceptez de recevoir des messages SMS de Sorare dans le but de vérifier votre compte et de détecter les fraudes. Les tarifs standards des messages et des données peuvent s'appliquer.

## 2. Fréquence des messages
   Sorare n'enverra des messages SMS que lorsque cela sera nécessaire pour la vérification du compte ou la détection de la fraude. La fréquence des messages sera réduite au minimum afin de respecter votre vie privée.

## 3. Refus
   Vous pouvez à tout moment vous opposer à la réception des messages SMS en répondant "STOP" au message reçu. Il n'est toutefois pas recommandé de le faire dans la mesure où cela empêcherait Sorare de procéder à la vérification de votre compte. Sachez que si vous ne recevez pas de messages SMS à des fins de vérification de votre compte, vous risquez de ne pas pouvoir récupérer l'accès à votre compte en cas de perte de vos identifiants et/ou mot de passe.

En vous désinscrivant, vous ne recevrez plus de messages SMS, à l'exception de ceux qui sont nécessaires à des fins légales ou transactionnelles.


## 4. Confidentialité des données
   Sorare attache une grande importance à la protection de votre vie privée. Votre numéro de téléphone portable et toute autre information collectée pour l’envoi de SMS seront traités conformément à notre politique de confidentialité, disponible à l'adresse suivante : https://sorare.com/privacy-policy.

## 5. Contenu des messages
   Les messages SMS envoyés par Sorare peuvent inclure, mais ne sont pas limités à, des codes de vérification de compte, des alertes sur des activités de compte suspectes, et d'autres messages nécessaires à la sécurité du compte.


## 6. Envoi des messages
   Sorare fera des efforts raisonnables pour assurer l’envoi des messages SMS dans les meilleurs délais. Toutefois, nous ne sommes pas responsables des retards ou des échecs dans la délivrance des messages causés par des facteurs indépendants de notre volonté.


## 7. Sécurité
   Il est de votre responsabilité d'assurer la sécurité de votre téléphone portable et des informations relatives à votre compte. Sorare n'est pas responsable de l'accès ou de l'utilisation non autorisés de votre téléphone portable ou de votre compte.


## 8. Responsabilité des partenaires indépendants
   Les messages SMS sont envoyés par des partenaires indépendants, y compris, mais sans s'y limiter, Twilio et Vonage, au nom et pour le compte de Sorare. Ces partenaires sont considérés comme des "sous-traitants" au sens du Règlement général sur la protection des données de l'UE et comme des "Businesses" au sens de la loi californienne sur la protection de la vie privée des consommateurs (CCPA). Sorare n'est pas responsable des problèmes liés au contenu, à la livraison ou à la sécurité des messages SMS envoyés par ces partenaires indépendants.

## 9. Modifications des conditions
   Sorare se réserve le droit de modifier ou de mettre à jour les présentes conditions à tout moment. Les modifications seront notifiées par le biais du service SMS ou sur notre site web. La poursuite de l'utilisation du service SMS après la date d'entrée en vigueur des modifications constitue une acceptation des conditions modifiées.

## 10. Contact
    Pour toute question concernant les présentes conditions d'utilisation du service SMS, veuillez nous contacter à l'adresse suivante : privacy@sorare.com.


