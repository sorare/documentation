# Marketplace Terms
## Last Update: March 29, 2024

## 1. Scope

The Marketplace is a bulletin board where you, as a non-professional, can advertise that a Collectible is for sale or purchase by other non-professionals through a post. If you post a collectible for sale, you are a “Seller” and if you buy a Collectible, you are a “Buyer.” These Marketplace Terms, together with the Terms and Conditions, govern your offers to sell or purchase a Collectible on Sorare.

## 2. Sale and Exchange

**2.1** **Buying, Selling, and Exchanging.** You may post your Collectibles for sale on the Marketplace as set out in the [Help Center](https://help.sorare.com/hc/en-us/sections/10968029982621-Secondary-marketplace). When you offer a Collectible for sale on the Marketplace, you set the price and post an advertisement for the relevant Collectible on the Marketplace (the “**Offer**”). The Offer will be open on the Marketplace for the period you determine (the "**Offer Period**") and is subject to counter offers from other Users (as defined below). You may accept Currencies, a Collectible, or the combination of the two for the relevant Offer or Counter Offer; this agreement involves exclusively the Seller and Buyer. If you do not sell your Collectible within the Offer Period you have determined, the relevant Offer expires, but you may post the Collectible for sale again at any time. Please note that due to technical constraints, it is currently not technically possible to mix Cryptocurrencies and Local Currencies, nor Collectibles from different sports available in the Services (Football, MLB and NBA) in any Offer.

**2.1.1** **Counter Offers**. As a User interested in another User’s Offer, you may want to make a counter offer to acquire a Collectible, using Currencies, Collectibles, or a combination of both (“**Counter Offer(s)**”). If the amount (in either Cryptocurrencies or Local Currencies) stipulated in your Counter Offer exceeds your current Wallet balance, you will be prompted to make a Currency deposit in your relevant Wallet prior to sending your Counter Offer. Please note that due to technical constraints, it is currently not technically possible to mix Cryptocurrencies and Local Currencies, nor Collectibles from different sports available in the Services (Football, MLB and NBA) in any Counter Offer.

**2.1.2** **Market Fee.** When you buy a Collectible in the Marketplace, Sorare and the Payment Providers that you may use may charge a fee for the transaction (“**Market Fees**”). Market Fees applied by Sorare correspond solely to the Marketplace technical infrastructure provision. All applicable Market Fees and their exact amounts will be clearly indicated to you prior to transaction. Sorare reserves the right to change the applicable Market Fees at our discretion.

**2.2** **Transactions Between Buyers and Sellers.** Sorare does not intervene, arrange or more generally participate in the conclusion of the sale between Buyers and Sellers on the Marketplace in any way, nor does it intervene in any way in the choice and/or price of the Collectibles subject to the Offers on the Marketplace. Sorare is not a party nor an agent to the sale or purchase of a Collectible on the Marketplace, and Sorare does not act as an intermediary for the bilateral or multilateral matching of the interests of Buyers and Sellers. This means that Sorare will not arrange and/or match potential buying interests of the potential Buyers to Collectibles listed for sale by the Buyers, or otherwise direct Offers to Buyers. You are solely responsible for your offers to sell or buy Collectibles; we make no representations regarding any Offer, Seller, price, Buyer, and/or Collectible value.

**2.3** **Marketplace Prices and Offers for Sale.** Sorare does not control the value, relevance and/or fairness of the prices proposed by a Seller. The Seller is solely responsible for determining the selling price of the relevant Collectible and the Buyer is solely responsible for evaluating the price of any Collectible listed on the Marketplace. Any User with a valid Sorare account can post an Offer on the Marketplace, in that User’s sole discretion.

**2.4 Ranking an Offer.**  
**2.4.1.** The Collectibles listed for sale are displayed by default based on the “Popular Player” criterion, which is determined by the recent number of Users who followed on Sorare  the athlete represented on the Collectible.

**2.4.2.** Users can nevertheless choose another criterion that will determine the display of Offers. They have the option to choose from the following criteria:

- “**Newly Listed**”: Collectibles are displayed according to the date upon which they have been listed on the Marketplace;
- “**Most Relevant**”: Collectibles are displayed taking cumulatively into account the following elements:
	- The performance of the athletes represented on the Collectible (average player score of the last 15 matches),
	- The Seller’s proposed price,
	- Any potential bonuses (experience, collection, season) attached to the Collectibles;
- “**Lowest Price**”: Collectibles are displayed based on the price offered by the Seller (in ascending order);
- “**Highest Average Score**”: Collectibles are displayed based on the average score of the athlete represented over the last 15 matches (in descending order).

**2.4.3.** Users can then filter the Collectibles listed on the Marketplace according to the objective criteria they wish. For instance, this allows users to display the Collectibles listed for sale on the Market according to their price, scarcity, league or championship.

**2.4.4.** A Collectible representing the same athlete in the same scarcity may be offered by multiple Sellers. In this case, the Offers are displayed by default on the basis of the “Most Relevant” criterion that takes into account the athlete’s performance, the price offered and the bonuses attached to the Collectible. However, Users can select another criterion: “Newly Listed", “Lowest Price” or “Highest Price”. 
Users can then filter the listed Collectibles based on their chosen objective criteria, including serial number, purchase options, offered price or season. Collectibles are then displayed based on the selected criteria.

**2.5 Disputes on the Marketplace.**  
**2.5.1. Responsibility for disputes between the Buyers and the Sellers.** When buying or selling Collectibles on the Marketplace, you understand and agree that you are solely responsible for the relevant transaction. To the fullest extent permitted by applicable law, Sorare shall not be held liable for damages caused by the sale, purchase, price, or any other act carried out by either Buyers or Sellers on or in connection with the Marketplace.

**2.5.2. Dispute resolution between the Buyers and the Sellers.** Any and all potential claims must be resolved between the relevant Buyer and Seller. The Seller is solely responsible for addressing any claim made against them. The consumer rights arising from European Union law or any other applicable consumer protection law, including the right of withdrawal, do not apply when you buy Collectibles on the Marketplace. Similarly, the legal warranties of conformity for digital content existing under French law or any other applicable law do not apply when buying or selling Collectibles (with the exception of the warranty against hidden defects, where applicable).

**2.5.3. Immutability of transactions on blockchain.** Transactions carried out on the Marketplace are immutably recorded on the blockchain. Once recorded, these transactions cannot be modified, canceled or deleted by Sorare or any other person outside the transaction. You are therefore invited to exercise the utmost diligence before proceeding with any transactions, as these are by nature definitive and irreversible for Sorare.

**2.6 Tax and Social Security Obligations.**   
**2.6.1** **General Obligations.** Note that according to most tax legislation, the sale of a Collectible is likely to constitute taxable income, particularly where the sale price of the Collectible exceeds its purchase price.

(A) You are solely responsible for declaring any income from a sale of your Collectible to the competent tax and/or social security administration.

(B) Nothing in this section shall be construed as financial, tax or legal advice. We encourage you to contact the competent tax and social security authorities to ensure you comply with all laws and regulations applicable to you.

(C) You may not use the Services and/or the Marketplace in an attempt to circumvent and/or avoid any tax, financial or other obligation applicable to you, including but not limited to income or social security obligations, anti-money laundering laws and/or regulations, court-mandated payment obligations and/or to finance or support any illicit or terrorist related activities.

**2.6.2 Residents of France.** Users residing in France for tax purposes can fulfill their tax obligations via the French tax authority’s website at [www.impots.gouv.fr](ww.impots.gouv.fr), and obtain information on any applicable tax regimes and tax declaration procedures at [https://www.impots.gouv.fr/portail/node/10841](https://www.impots.gouv.fr/portail/node/10841). Users residing in France for tax purposes may, where applicable, fulfill their social security obligations via the social security website, [www.urssaf.fr](https://www.urssaf.fr), and obtain information on any applicable social security schemes and declaration procedures at [https://www.urssaf.fr/portail/home/espaces-dedies/activites-relevant-de-leconomie.html](https://www.urssaf.fr/portail/home/espaces-dedies/activites-relevant-de-leconomie.html)

**2.6.3 Transaction Reporting.** Collectibles listed for Offers via the Marketplace and which have been subject to transactions will be subject to a Transaction Reporting. The Transaction Reporting covers the number of transactions and amount made during the previous year and will be made available to you at the beginning of the year.