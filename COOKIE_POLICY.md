# COOKIE POLICY

August 2, 2024

Version 2.0

## 1. Foreword

Sorare enables users (“User”) registered on the Website sorare.com (“Website”) to benefit from an enhanced user interface. Sorare is a game where Users can collect Collectibles featuring professional sport players, access a fantasy game, play with officially licensed digital cards, and earn prizes. The Website provides a marketplace for registered users to acquire, collect, and exchange collectibles and their underlying non-fungible tokens based on blockchain technology (collectively, the “Services”).

To enable Users to benefit from the Services offered by the Website https://sorare.com/ such as its consultation, optimization of its use or personalization according to the User, the Website uses cookies.

You can deactivate cookies to which you have consented at any time, free of charge, using the deactivation options offered to you and listed below.

## 2. What is a cookie?

**2.1** When you browse out the Website, information relating to the navigation of your terminal (computer, tablet, smartphone, etc.) on the Website may be recorded in text files called "cookies", placed on your browser. Cookies will be used to recognize your browser for the duration of the cookie's validity and return certain information to the Website (such as a session identifier or language choice).

**2.2** Only the sender of a cookie can read or modify the information contained in it.

**2.3** There are different types of cookies:

- session cookies that disappear as soon as you leave the Website;
- permanent cookies that remain on your terminal until their lifetime expires or until you delete them using your browser's functions.

**2.4** You are informed that, during your visits to the Website, cookies may be placed on your terminal equipment.

## 3. Purposes of cookies placed on terminal

**3.1** Some cookies are essential to the use of the Website: these are technical cookies used by the Website host for the technical management of the network.

**3.2** Others are used to analyze the frequentation and the use made of the Website, in order:

- to make it evolve in the sense of an improvement of the experience of navigation, in the final objective to propose you a Website always more adapted; to conduct studies;
- to memorize your browser's display preferences (language used, display parameters, operating system used, etc.) and to adapt the presentation of the Website to the time of your visits, according to the hardware and software of visualization or reading that your terminal equipment includes and which are used for navigation on our Website;
- to implement security measures.

**3.3** Finally, certain cookies are used for advertising purposes and in particular to identify the origin of new users, enabling Sorare to measure the performance of its marketing campaigns on third-party websites and social networks.

## 4. Personal Data processed

**4.1** Cookies used on our Website are:

- Sorare’s cookies ;
- third party cookies chosen by SORARE Company in order to achieve certain objectives

**4.2** Within the framework of the use of cookies as described on the present page, the SORARE Company is likely to process Personal Data concerning you, such as IP addresses, in its capacity as data controller.
**4.3** Third-party cookies on the Website are set by external processors, which are likely, if you accept these cookies, to process Personal Data about you.
**4.4** The issuance and use of these cookies by third parties is subject to their own privacy policies. For more information about these processes, you can refer to their privacy policies.
**4.5** The data collected is essential to achieve the objectives pursued by each cookie. It is only intended for the authorized departments of Sorare and/or the Company issuing the third-party cookies.
**4.6** Personal Data about you may be transmitted to service providers located in third countries outside the European Union.
**4.7** These data transfers are governed by a cross-border flow agreement established in accordance with the contractual clauses issued by the European Commission and currently in force.
**4.8** Personal Data collected via cookies is never kept longer than necessary to achieve the purpose of the cookie and, in any case, not more than 13 months.
**4.9** For more information, including how to exercise your rights, see our [Privacy Policy](https://sorare.com/privacy-policy).
**4.10** The placement and use of cookies by these companies is subject to their own terms of use, which you can view on their respective websites. Below, you will find a list of the companies that manage third-party cookies on the Site as well as their purposes and the link to the personal data management policies of each company concerned:

- **4.10.1** Cookies used to measure the performance of Sorare's advertising campaigns for the promotion of its services and the acquisition of new users:

  - Google: [Google Tag Manager, Google Ads, Google Dynamic Retargeting, DoubleClick G](https://policies.google.com/privacy)
  - Twitter/X: [Twitter Advertising; Twitter Conversion Tracking](https://twitter.com/en/privacy)
  - Facebook: [Facebook Conversions API, Facebook Custom Audience](https://www.facebook.com/privacy/center/?entry_point=facebook_page_footer)
  - TikTok: [Tiktok Conversions API](https://www.tiktok.com/legal/page/eea/privacy-policy/en)
  - Snapchat: [Snapchat Conversions API](https://values.snap.com/en-GB/privacy/privacy-policy)

- **4.10.2** Cookie used for analysis and audience measurement to improve the Site and services:

  - [Amplitude](https://amplitude.com/privacy)
  - Google: [Google Analytics 4 Cloud](https://policies.google.com/privacy)
  - Amazon: [AWS S3](https://aws.amazon.com/privacy/)

- **4.10.3** Cookies used to send our users marketing emails:

  - Braze: Braze Web Device Mode (Actions)

- **4.10.3** Cookies to send our users SMS and push notifications:

  - Braze

- **4.10.5** Cookie essential to the proper functioning of the Site:

  - [LaunchDarkly](https://launchdarkly.com/policies/privacy/)
  - [Stripe](https://stripe.com/en-gb-fr/privacy)

## 5. Sharing your terminal use with others

**5.1** If your terminal is used by several persons, and when a single terminal has multiple browsers, we cannot be certain that the services and advertisements directed to your terminal equipment correspond to your own use of that terminal equipment and not to that of another User of that terminal equipment.

**5.2** Sharing with others the use of your terminal equipment and configuring your browser settings with respect to cookies is your free choice and responsibility.

## 6. Consent

**6.1** When you first visit our Website, you will be asked to accept, configure or refuse the use of some cookies.

**6.2** If you do not want cookies to be placed or read on your terminal equipment, a refusal cookie will be placed on your equipment so that SORARE can record the information according to which you have refused the use of cookies. If you delete this refusal cookie, it will no longer be possible to identify you as having refused the use of cookies.

**6.3** Also, when you accept the deposit of cookies, a consent cookie is placed.

**6.4** Refusal or consent cookies must remain on your terminal equipment. You can change your choices at any time by clicking here.

## 7. Cookie management

**7.1** You can manage and modify the use of cookies at any time according to the possibilities listed below:

- directly on our Website, in the module provided for this purpose, by clicking here, or
- from your browser, or
- from interprofessional opposition platforms.

**7.2** Please note your refusal to place a cookie is based on the deposit of a cookie. Therefore, if you deactivate all cookies on your terminal or if you change your terminal, we will no longer know that you have chosen this option.

**7.3 Cookie management module**

**7.4** A module allows you to choose which cookies you wish to accept and which you wish to reject on our Website.

**7.5** At any time, by clicking here you can access the module and change your preferences.

**7.6 Navigation software settings**

**7.7** You can configure your browser so that cookies are stored in your terminal equipment or are rejected, either systematically or according to their sender.

**7.8** You can also regularly delete cookies from your terminal via your browser.

**7.9** However, don't forget to configure all the browsers on your various terminals (tablets, smartphones, computers...).

**7.10** Each browser is configured differently in order to manage cookies and your choices. It is described in the help menu of your browser, which will allow you to know how to modify your choices regarding cookies. For example:

- for Chrome™: https://support.google.com/accounts/answer/32050?hl=en ;
- for Firefox™: https://support.mozilla.org/en-US/kb/clear-cookies-and-site-data-firefox ;
- for Safari™ : https://support.apple.com/en-gb/guide/safari/sfri11471/mac ;
- for Edge™: https://support.microsoft.com/en-us/microsoft-edge/delete-cookies-in-microsoft-edge-639474 ;
- for Opera™: https://help.opera.com/en/latest/web-preferences/#cookies .

**7.11** Please note, however, that by setting your browser to reject cookies, certain features, pages and areas of the Website that require the use of cookies will not be accessible, for which we cannot be responsible.

**7.12** To learn more about how to configure your browser, we invite you to consult the site of the Commission Nationale de l'Informatique et des Libertés: https://www.cnil.fr/fr/cookies-les-outils-pour-les-maitriser.

**7.13 Interprofessional opposition platforms**

**7.14** Several advertising professionals' platforms also offer you the possibility to refuse or accept cookies used by the member companies. These centralized mechanisms do not block the display of advertisements but only prevent the installation of cookies that allow the adaptation of advertisements to your interests and behavior.

**7.15** For example, you can connect to the site www.youronlinechoices.com in order to prevent these cookies from being placed on your terminal. This Website is proposed by the professionals of digital advertising gathered within the European association EDAA (European Digital Advertising Alliance) and managed in France by the Interactive Advertising Bureau France.

**7.16** You will be able to find out which companies are registered with this platform and which offer you the possibility of refusing or accepting the cookies they use to adapt, according to the data they collect, the advertisements that may be displayed on your browser: www.youronlinechoices.com/fr/controler-ses-cookies.

**7.17** This European platform is shared by hundreds of Internet advertising professionals and constitutes a centralized interface allowing you to express your refusal or acceptance of cookies as specified above.
