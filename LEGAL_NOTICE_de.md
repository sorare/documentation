# Impressum

## Informationen zu SORARE SAS

SORARE SAS ist ein französisches Unternehmen (Société par Actions Simplifiée) mit einem Stammkapital von 2.405.149,31 €. Es ist unter der Nummer 844 355 727 in Créteil, Frankreich, registriert, mit Sitz in 5, Avenue du Général de Gaulle, 94160 Saint-Mandé, Frankreich.

UST-ID-Nummer von SORARE SAS: FR75844355727

Kontakt für Support: support@sorare.zendesk.com

Gesetzlicher Vertreter und Herausgeber: Nicolas Julia

## Webhosting

Amazon Web Services EMEA SARL

Anschrift: 38 avenue John F. Kennedy, L-1855 99137 Luxemburg

Kontakt: https://aws.amazon.com/fr/contact-us/

Telefonnummer: +352 26 73 30 00

## Geistiges Eigentum

Alle Inhalte auf der Website, einschließlich Marken, Fotos, Texte, Kommentare, Illustrationen, Bilder (animiert oder nicht), Videosequenzen und Töne sowie die für ihren Betrieb verwendeten Computeranwendungen, unterliegen den geltenden Gesetzen zum geistigen Eigentum.

Diese Elemente sind das ausschließliche Eigentum von Sorare SAS oder seinen Partnern. Jede Vervielfältigung, Darstellung, Nutzung oder Anpassung, in welcher Form auch immer, von Teilen oder der Gesamtheit dieser Elemente, einschließlich der Computeranwendungen, ist ohne die vorherige schriftliche Zustimmung des Verlags strengstens untersagt. Die Tatsache, dass der Verlag nicht unverzüglich rechtliche Schritte einleitet, nachdem er von einer solchen unbefugten Nutzung erfahren hat, stellt in keiner Weise eine Zustimmung zu dieser Nutzung dar oder einen Verzicht auf die Absicht, rechtliche Schritte zu unternehmen.
