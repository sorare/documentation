# Sorare Terms and Conditions
## Effective: July 23, 2024 


**FOR US-BASED USERS ONLY: THESE TERMS CONTAIN AN ARBITRATION AGREEMENT, WHICH WILL, WITH LIMITED EXCEPTION, REQUIRE YOU TO SUBMIT CLAIMS YOU HAVE AGAINST US TO BINDING AND FINAL ARBITRATION. UNDER THE ARBITRATION AGREEMENT, (1) YOU WILL ONLY BE PERMITTED TO PURSUE CLAIMS AGAINST SORARE ON AN INDIVIDUAL BASIS, NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY CLASS OR REPRESENTATIVE ACTION OR PROCEEDING, (2) YOU WILL ONLY BE PERMITTED TO SEEK RELIEF (INCLUDING MONETARY, INJUNCTIVE, AND DECLARATORY RELIEF) ON AN INDIVIDUAL BASIS, AND (3) YOU MAY NOT BE ABLE TO HAVE ANY CLAIMS YOU HAVE AGAINST US RESOLVED BY A JURY OR IN A COURT OF LAW.**

Sorare is a sports entertainment platform where sports fans (“**you**” or “**User**”) can engage with their favorite athletes and teams and build connections with the global sports community. Sorare creates digital trading cards backed by blockchain technology and featuring professional athletes (“**Collectibles**”). You can buy Collectibles on Sorare website or mobile applications, use your sports knowledge to compose your dream team and enter that team in a fantasy sports tournament based on the real-life performance of the athletes you have collected. You can also post your Collectibles for sale or buy other users’ Collectibles on the Marketplace.

These Terms and Conditions (“**T&Cs**” or “**Terms**”) govern your use of the Sorare website located at sorare.com (including any successor URLs) (“**Sorare Website**” or “**Website**”), and/or mobile application(s) available for download (“**Sorare App**” or “**App**”), operated by Sorare SAS and its affiliates (“**Sorare**”, “**we**” or “**us**”). Additionally, Sorare’s privacy policy (“**Privacy Policy**”) governs how we process any of the data that we receive through your use of the Website, App, and Services.
Depending on how you use the Website, additional terms (“**Additional Terms**”) may apply, including:

- The Marketplace Terms, if you post an offer for sale or purchase of a Collectible;
- The Referral Terms, if you participate in our Referral Program;
- The Affiliate Terms, if you participate in our Affiliate Program;
- The Game Rules, if you enter into a tournament;
- The API Terms, if you access Sorare’s API;
- The Brand Guidelines, if you use any of Sorare’s marks or trademarks.

These Terms and Conditions and any Additional Terms (where applicable) form a legally binding agreement between you (or any involved parties such as your authorized successors, assignees, and beneficiaries) and Sorare (the “**Agreement**”)– please read them carefully. The Agreement hereby represents the entirety of the agreement, overriding any prior or concurrent representations, understandings, or agreements between you and Sorare regarding the subject matter herein. Should any provision of this Agreement be deemed invalid or unenforceable by an arbitrator or a court of valid jurisdiction, that specific provision will be enforced to the fullest extent permitted under applicable law, while the remaining provisions of this Agreement will continue to be fully valid and effective.

For the purposes of these Terms, any capitalized terms, including their plural forms, shall have the meaning assigned. Sorare also makes a help center available at  [https://help.sorare.com/](https://help.sorare.com/), where you can find useful explainers, guides, and interesting articles (the “**Help Center**”). The materials provided in the Help Center are purely informative and not legally binding. We invite you to have a look or reach out to our Support Team via the Help Center if you ever have any questions.

## 1. The Sorare Website and App
**1.1**. The Website, App, and Services are owned and provided to Users by Sorare SAS, a French company (société par actions simplifiée) with a capital of 2 405 149,31 euros, registered in the Créteil Trade and Companies Register under number 844 355 727, with a registered address of 5 avenue du Général de Gaulle, 94160, à Saint-Mandé.

**1.2**. The services include all products, offerings, games, events and features, including the Game (as defined in the “**Fantasy Games**” section below) and the Marketplace, offered by Sorare through the Website and App (“**Services**”). Through the Website and App, you can acquire, collect, exchange, and play Collectible digital cards that leverage blockchain technology and feature professional athletes from officially-licensed teams. You can also compose teams with Common Cards and/or Collectibles (each defined below), enter the team into a tournament and compete with other Users. 

**1.3**. The Website and App may contain links or content from services not operated by Sorare that provide additional content or features such as payment services, the purchase and sale of digital assets in fiat currencies, digital asset wallets as well as player data and third-party collectible marketplaces (“**Third-Party Services**”). Whether or not integrated into the Website and/or App, these Third-Party Services are provided under the terms of use and privacy policy of the relevant third-party provider, under that provider’s sole responsibility. Sorare is not responsible or liable for the availability or accuracy of the Third-Party Services, or the services, products, or content available from the Third-Party Services. Links to or use of any Third-Party Services are not an endorsement by Sorare of such Third-Party Services. Please consult the relevant Third-Party Service’s terms and conditions and/or privacy policy for more information.

## 2. Account
**2.1. Account Creation**. 

**2.1.1** Sorare is free to use. To sign up for a User account, you must provide:

- a username;
- an e-mail address.

Alternatively, you can create an account by signing on through your Google, Apple or Facebook account.

When creating a User account, you must agree to these Terms, and by doing so you represent and warrant that you (i) are sixteen (16) years old or older; (ii) agree to these Terms and Conditions; (iii) will use the Website, App, and/or Services in compliance with these Terms and any applicable law, rule or regulation; and (iv) you are not located in a country that the U.S. government, European Union and/or United Nations has placed embargo restriction on and you are not on the U.S. government, European Union or United Nations list of restricted parties or persons (including being a Specially Designated National by the U.S. government or being included in the consolidated list of persons subject to EU financial sanctions).
For the avoidance of doubt, you shall not attempt to use or access or use or access the Website, App, or Services at any time from a country that the U.S. government, European Union and/or United Nations has placed embargo restriction on or if you are added to the U.S. government, European Union or United Nations list of restricted parties or persons (including being a Specially Designated National by the U.S. government).

**2.1.2**. Notwithstanding the foregoing, entering a Team in a Sorare Pro Competition is restricted to Users who are eighteen (18) years of age or older, except in connection with some specific Competitions where applicable. For the avoidance of doubt, the terms “Team” and “Sorare Pro Competition” shall have the meaning assigned to them in the Game Rules. 

**2.2**. Account Use. The Website, App, User accounts and Services are free for individual, non-commercial, personal use. You shall not sign up on behalf of or act in coordination with another individual, group of individuals and/or a company.

**2.3. Username**.

**2.3.1**. While creating a User account, you will be required to pick a username that will be associated with any purchases and Game participation of your User account.

**2.3.2**. You shall not change your username more than once during a three-month period.

**2.3.3**. You shall not attempt to mislead, confuse and/or defraud other users via elements in your username. You may not include any abusive, harassing, offensive or otherwise malicious language or suggestions in your username.

**2.4. Account Security**.

**2.4.1**. Please do not share your account information with third parties and observe other security best practices, such as choosing a strong password, using a password manager, using two-factor authentication and storing your information securely. We are not liable for the loss or theft of your password or other account information. It is your responsibility to take all measures to secure access to your login and account information, including your password. You are responsible for the actions of any person using your User account, including without your prior consent.

**2.4.2**. You agree to report any potential theft of your information, login information, or password, unauthorized access to your account and/or any other fraudulent or unauthorized third-party use of your account, to us as quickly as possible, by contacting our Support Team via the Help Center at [https://help.sorare.com/](https://help.sorare.com/).

**2.5. Prohibited Account Uses**.

**2.5.1. Multiple Accounts**. You may not create more than one User account and/or manage the User account(s) of other Users (even if given explicit permission by such Users). Only a natural person can create an account, and partnerships, joint ventures, and/or corporate accounts are prohibited.

**2.5.2. Associating Accounts**. You may not collude with or otherwise act in association or conjunction with other User accounts, whether formally or informally, in order to attempt to circumvent or circumvent these Terms; attempt to create or create an unfair competitive advantage and/or other unfair or artificial advantage in the Game; or attempt to unfairly or otherwise artificially influence Collectible value, including but not limited to through directing other Users regarding actions on such Users’ respective User Account, lending, trading, buying, repeatedly selling and/or selling Collectibles below market value.

**2.5.3. Account Re-creation**. You may not create a new User Account if your User account has been temporarily suspended or if we have permanently deleted a User Account associated with you in the past.

## 3. Wallets

**3.1. Cash Wallet**. Each User account has a corresponding unique and personal e-wallet made available through and operated by Third Party Service Mangopay to store fiat currencies as detailed on the Website (“**Local Currency**”) and for use in connection with the Services (“**Wallet**” or “**Cash Wallet**”). Please note that all services provided in connection with this Cash Wallet are provided by Mangopay in a completely independent manner and that Sorare is not involved in any financial flows or fiat currencies custody. 

**3.1.1**. To activate your Wallet and all its corresponding features (including withdrawals, deposits and rewards claims in Local Currencies), you will be required to (i) agree to [Mangopay’s terms and conditions for payment services](https://mangopay.com/terms-and-conditions/payment-services), (ii) provide any requested or required pieces of information for account verification purposes and, (iii) choose your preferred Local Currency amongst those supported (including U.S Dollars, Euros and British Pounds) (“**Preferred Currency**”). Please note that once you have selected your Preferred Currency and verified your account, you will not be able to change your currency setting. Your Preferred Currency deposits may be limited to certain amounts, which will either be specified on the Website or communicated to you through Mangopay. The Wallet may not be accessible in your location. In such a case, you may still be able to use a Blockchain Wallet (as defined below).

**3.1.2**. You will be required to create a password to access your Wallet. Please choose a strong password, use a password manager, store your information securely, do not share your account information with third parties and observe other security best practices. We are not liable for the loss or theft of your password and/or any Local Currency that you store in your Wallet. It is your responsibility to take all measures to secure access to your login and account information, including your password.

**3.1.3**. You may use your Wallet in connection with the purchase or sale of Collectibles through the Services. You may also add or remove your Local Currency to or from your Wallet by linking an external bank account. Information on how to deposit and withdraw your Local Currencies to and from the Wallet or an external bank account is provided on the Help Center at [https://help.sorare.com/](https://help.sorare.com/).

**3.1.4**. You hereby acknowledge and agree that Sorare may allow Mangopay to debit any sums due by You in connection with transactions that are subject to chargebacks, fraud or other potential risks, in accordance with [Mangopay’s terms and conditions for payment services](https://mangopay.com/terms-and-conditions/payment-services). 

**3.2. Blockchain Wallet**. In addition to your Cash Wallet, a digital assets wallet (“**Blockchain Wallet**”) will be automatically created for you upon User account creation to store your Collectibles and the other digital assets and use them in connection with the Services (“**Cryptocurrencies**”).

**3.2.1**. You will be required to create a password to access your Blockchain Wallet. Please choose a strong password, use a password manager, store your information securely, do not share your account information with third parties and observe other security best practices. We are not liable for the loss or theft of your password and/or any Cryptocurrency or Collectibles that you store in your Blockchain Wallet. Only you will have access to your password and to the private key securing your Blockchain Wallet. Sorare has no access to your password, private key, or other access mechanism. It is your responsibility to take all measures to secure custody and access to your login and account information, including your password and private key (which is accessible through your Blockchain Wallet settings).

**3.2.2**. You may use your Blockchain Wallet in connection with the purchase or sale of Collectibles through the Services. You may also add or remove your own Cryptocurrency and Collectibles to or from your Blockchain Wallet by linking an external wallet. Information on how to deposit and withdraw Collectibles or Cryptocurrencies to and from the Blockchain Wallet or an external wallet is provided on the Help Center at [https://help.sorare.com/](https://help.sorare.com/).

**3.3**. Your Wallet and Blockchain Wallet are collectively referred to as your “**Wallets**”. Local Currencies and Cryptocurrencies may be collectively referred to as “**Currencies**”.

**3.4**. If you lose any of your Wallets’ password or suspect that a third-party has gained unauthorized access to any of your Wallets, please contact us immediately via the Help Center at [https://help.sorare.com/](https://help.sorare.com/).

## 4. Services

**4.1. License Grant**. Subject to your compliance with these Terms and any applicable Additional Terms, we hereby grant you a limited, non-exclusive, non-transferable, non-sublicensable, revocable personal license to use the Services.

## 5. Common Cards

**5.1. License Grant**. After signing up for an account, you may have access to free digital cards that are not NFTs (“**Common Cards**”). Subject to your compliance with these Terms, we hereby grant you a limited, non-exclusive license to collect and play the Common Cards within Sorare. They are located in the “My Club” section of your User account.

**5.2. Card Elements and Third-Party Rights**. The player image, team branding (including trademarks and jerseys) and other elements figuring in the Common Cards are officially licensed from the relevant third-party rights holder (“**Third-Party Card Elements**”). This may include third-party patent rights, image rights, copyrights, trade secrets, trademarks, know-how or any other intellectual property rights recognized in any country or jurisdiction in the world (“**Third-Party Rights**”). For the avoidance of doubt, you are only authorized to use and display Third-Party Card Elements as part of the use and display (as applicable) of the Common Card embedding such Third-Party Card Elements, within the Sorare Website only. Any other use or display of the Third-Party Card Elements is strictly prohibited.

**5.2.1. License Restrictions**. You may not, under any circumstances and without Sorare’s prior written consent, attempt to carry out or carry out any of the following to the Common Cards, whether or not held by you: (i) use the Common Cards and any of the Third-Party Card Elements for commercial purposes, advertising or promotion of a third party product or service; (ii) market merchandise, physical or digital, that represents the Common Card; (iii) alter the image associated with the Common Card, and/or alter, edit or modify the Third-Party Card Elements in any other way; (iv) attempt to claim any additional intellectual property rights relating to the Common Card or the Third-Party Card Elements; (v) violate any applicable Third-Party Right; and/or (vii) use the Third-Party Card Elements and/or the Common Card in connection with images, videos, or other forms of media that depict hatred, intolerance, violence, cruelty, or anything else that could reasonably be found to constitute hate speech, defamation or otherwise infringe upon the rights of others, including the image right(s) of the featured player.

## 6. Collectibles

**6.1. Collectibles**. Collectibles are unique digital trading cards featuring a professional athlete backed by a non-fungible token (“**NFT**”) using blockchain technology and issued by Sorare. The NFT ensures the scarcity and authenticity of the Collectible and the transparency of the Services. NFTs on the Sorare platform are minted using a secondary protocol built on top of the Ethereum Blockchain (“**Layer 2**”) by Starkware. This layer 2 solution allows for a higher processing capacity for transactions and reduces gas fees.

**6.2. Scarcity**. Collectibles are classified based on a level of scarcity specified for each issuance. For instance, a Collectible may be Limited, Rare, Super Rare, or Unique, regarding the total number of items offered. The scarcity of a Collectible is guaranteed by the underlying NFT.

**6.3. Value**. Collectibles are the digital equivalent of paper sports trading cards. They are sold without an investment purpose and/or for the purpose of gaining additional value, even if the Collectibles can be resold via the Marketplace or through a Third-Party Service or marketplace.

**6.4. Ownership**. When you purchase a Collectible, you become the definitive owner of the underlying NFT, in accordance with these Terms. Except where otherwise explicitly stated to the contrary, you have the right to freely dispose of your Collectible (via sale, loan, donation, transfer, etc. including in the Marketplace or through a Third-Party Service or marketplace).

**6.5. Collectible Elements and Third-Party Rights**. The player image, team branding (including trademarks and jerseys) and other elements figuring in the Collectibles are officially licensed from the relevant third-party rights holder (“**Third-Party Collectible Elements**”). This may include Third-Party Rights as defined herein.

**6.5.1. License to Third-Party Rights in Collectible Elements**. Subject to compliance with these Terms, you are hereby granted, on a non-exclusive basis, a worldwide limited license to use, display and transfer the Third-Party Collectible Elements associated with the Collectibles for the period that you own the relevant NFT(s), for personal and non-commercial purposes only. For the avoidance of doubt, you are only authorized to use, display, and transfer Third-Party Collectible Elements as part of the use, display, and transfer (as applicable) of the Collectible embedding such Third-Party Collectible Elements. Any other use, display or transfer of the Third-Party Collectible Elements is strictly prohibited. If, at any time, you decide to sell, swap, donate, transfer, give away or otherwise dispose of your Collectible for any reason, the license hereby granted to you will immediately terminate in connection with that Collectible without the requirement of notice. Such license shall automatically transfer to the recipient of the Collectible and you shall use your best efforts to communicate the terms of this license to such recipient upon transfer of the relevant Collectible. The limitations specified below in 6.5.2. will persist beyond the termination of these Terms.

**6.5.2. License Restrictions**. You may not, under any circumstances and without Sorare’s prior written consent, attempt to carry out or carry out any of the following to the Collectibles, whether or not owned by you: (i) use the Collectible and any of the Third-Party Collectible Elements for commercial purposes, advertising or promotion of a third-party product or service; (ii) market merchandise, physical or digital, that represents the purchased Collectible; (iii) alter the image associated with the Collectible, and/or alter, edit or modify the Third-Party Collectible Elements in any other way; (iv) attempt to claim any additional intellectual property rights relating to the Collectible or the Third-Party Collectible Elements; (v) violate any applicable Third-Party Right; and/or (vi) use the Third-Party Collectible Elements and/or the Collectible in connection with images, videos, or other forms of media that depict hatred, intolerance, violence, cruelty, or anything else that could reasonably be found to constitute hate speech, defamation or otherwise infringe upon the rights of others, including the image right(s) of the featured player; (vii) trade, distribute for profit (which includes, but is not limited to, transfer without consideration with the intention of potential future profit), or otherwise engage in any commercial activities involving merchandise that incorporates, contains, or is composed of the Third-Party Collectible Elements related to your Collectible.

**6.6 Collectible Issuance**.

**6.6.1**. You can buy a Collectible from Sorare through auctions or Instant Buy (as defined below), via offers from other Users (in the Marketplace), or through a third-party marketplace.

**6.6.2. Auctions**. When Collectibles are first issued, we may offer them for sale at auction (“**Auction**”). The Auction for the relevant Collectible will be open for a specified period of time (the “**Auction Period**”). You may set the maximum amount you would like to bid for a Collectible at an Auction (“**Maximum Bid**”) and Sorare will automatically bid in increments on your behalf up to your Maximum Bid as detailed in our Help Center ("**Automatic Bidding**"). If you get outbid by another User during the Auction Period, you will have the option to increase your Maximum Bid. You can stop Automatic Bidding at any time during the Auction Period. The last, highest bidder acquires the Collectible. This may include single Collectibles and Collectible bundles. If you buy a bundle, the included individual Collectibles will be specified.

**6.6.3**. Any bid registered on the Website through a User account is deemed authentic and valid. Once you have placed a bid, it cannot be withdrawn or modified. If the Auction Period concludes and your bid is the highest, your payment method will be charged.

**6.6.4**. In the event of a service interruption or technical problem that is unforeseeable and/or outside of our reasonable control that causes an Auction to be inaccessible for any period of time within four (4) hours of its scheduled end, Sorare reserves the right to extend the Auction Period or to cancel the Auction and relaunch it at a later date.

**6.6.5. Instant Buy**. Sorare reserves its right to offer for sale certain Collectibles, individually or in packs/bundles at a set price which will be based on the price of the last sales observed for equivalent Collectibles. Instant Buy allows you to directly purchase some Collectibles offered for sale by Sorare on the App and/or on the Website while supplies last.

**6.6.6**. If you are located in a country  where a legal withdrawal right is offered, you may generally cancel your purchase for any reason within fourteen (14) calendar days. Notwithstanding the foregoing, given the digital nature of the Collectibles, when you purchase a Collectible from Sorare, you will be required to accept the immediate performance of the contract (and hence the immediate access to the Collectibles upon purchase) and to waive your right of withdrawal from the purchase of a Collectible. We will confirm your agreement to the provision of the digital content and your waiver of your right of withdrawal by email.

**6.7. Transfer and Exchange**.

**6.7.1**. **Through the Marketplace** (as defined in the Sorare Marketplace Terms). You may sell your Collectibles to other Users or buy other Users’ Collectibles by placing an ad for the relevant Collectible on the Marketplace. Sorare may, at its sole and absolute discretion, limit your ability to use the Marketplace (including, without limitation, the selling and trading of Collectibles) from time to time. Use of the Marketplace is governed by the Sorare Marketplace Terms.

**6.7.2**. **Through a Third-Party Service**. You may withdraw your Collectibles from your Blockchain Wallet and transfer to and/or sell the Collectibles through a Third-Party Service or marketplace. In order to transfer your Collectibles to an external wallet or Third-Party Service or marketplace, you will need to pay the gas fees associated with transferring the Collectibles from our Layer 2 solution to the Ethereum Blockchain. Any sales or exchanges on Third-Party Services and/or marketplaces will be subject to the terms and conditions provided by such Third-Party Service or marketplace.

**6.7.3. Disclaimer**. To the fullest extent permitted by the applicable law, Sorare, its affiliates, officers, agents, consultants, vendors, equity holders, employees, successors, assigns, directors, partners, subsidiaries, contractors and licensors are under no circumstances responsible or liable for any loss or damage that may occur during the transfer, withdrawal, sale, or exchange of Collectibles or other digital assets through the Marketplace or outside the Website.

## 7. Payment

**7.1**. When you make a payment in connection with the Services, your payment will be processed by one of our trusted, Third-Party Service payment providers (“**Payment Provider**”). For more information regarding your different payment options, please consult the Help Center.

**7.2**. In order to process your payment, the relevant Payment Provider may ask you for additional information in order to verify your identity or validate the transaction, in compliance with that Payment Provider’s terms of service and privacy policy.

**7.3**. You acknowledge and agree that due to the nature of blockchain technology, once a transaction has been processed, the associated transaction data will be irreversibly associated with the relevant Collectible and documented on the blockchain.

**7.4**. Your transaction may be declined, frozen, or held for various reasons, including but not limited to suspected fraud, adherence to economic or trade sanctions, pursuant to Sorare and its Payment Providers internal risks management policies.

## 8. Fantasy Games

**8.1**. You may test your sports skills and knowledge by composing teams of your Common Cards and/or Collectibles and then entering your team in a virtual tournament. The virtual tournament tracks the real performance of the players represented on the Common Cards or Collectibles you’ve added to your dream team (the “**Game**”).

**8.2**. Participation in the Game is governed by the rules available [here](https://sorare.com/game_rules) (the “**Game Rules**”).

## 9. Club Shop and Sorare Factory

**9.1. Club Shop**. You may acquire virtual in-game items, officially licensed goods and other items made available by Sorare from time to time through a dedicated shop available on the Website and App (the “**Club Shop**”), using Sorare’s in-game items that can be obtained by playing the Game (“**Coins**”).

**9.2. Sorare Factory**. You may unlock (or “craft”) certain Collectibles through a segment of the Website or App (the “**Sorare Factory**”), using dedicated in-game items that can be obtained by playing the Game (the “**Essence**”). While collecting Essences may affect your chances of crafting a Collectible associated with a particular league or club (as indicated on the Website or App), crafting a Collectible in the Sorare Factory does not guarantee or allow you to choose to receive a particular Collectible (i.e. associated with a particular player). The number of Essences required to craft a Collectible is at the sole discretion of Sorare and Sorare reserves the right to change this number at any time without prior notice. For more information on how the Essences work, please read this [article](https://sorare.com/en-gb/help/s/19736400470813/the-sorare-factory).

**9.3**. Coins and Essences do not have any value in either fiat or digital currency and cannot be exchanged or converted against any other currency within or outside the Services. Coins and Essences do not earn interest, they cannot be traded nor transferred between Users and cannot be used or transferred outside of the Services.  

**9.4**. You are granted a limited license to use the Coins in the Club Shop and Essences in the Sorare Factory. Sorare retains all rights to the Coins and Essences and reserves all rights to change or suspend temporarily or indefinitely the possibility to acquire Coins and Essences in any way. Sorare does not provide any warranties or representation in relation with these Coins and Essences notably the possibility to exchange these Coins or Essences for anything else of value.

**9.5**. Coins and Essences are specific to each sport available in the Services (MLB, NBA and Football where available) and cannot be transferred from one sport to another. Please note, that in the case where your User account is suspended as provided in Section 16 of the present Terms, your Coins and/or Essences will be forfeited.

## 10. Referral Program

You may refer other users to the Sorare website by participating in the referral program (the “**Referral Program**”), governed by the Referral Program Terms.

## 11. Affiliate Program

You may participate in the affiliate program, where you can earn rewards for purchases by other Users you have referred to Sorare (the “**Affiliate Program**”). Participation in the Affiliate Program is governed by the Affiliate Program Terms.

## 12. Your Information, Content, and Feedback

**12.1. Your Information**. Please keep your User account information up to date. Note that pursuant to legal and/or regulatory requirements, we might request more information from time-to-time. We will contact you via your User account or the email associated with your User account if this is the case.

**12.2. Your Content**. You hereby grant and/or agree to grant to Sorare the transferable, sub-licensable, free of charge, global right to exclusively use, copy, distribute, adapt, creative derivative works, reproduce, distribute, modify, translate, and otherwise exploit, any content you produce when using the Services (including but not limited to username, comments, team composition, team name, etc.) (“**Content**”), by any present or future process and by any means of distribution on the Website, social networks, or media (such as TikTok, Facebook, X, Instagram, Reddit, etc.) or platforms (such as YouTube), e-banners and promotional emails or newsletter and on Sorare intranet or in its internal documentation, whether in whole or in part, and whether as provided or as modified, for commercial or promotional purposes.

**12.3. Your Feedback**. We invite you to send us any feedback, comments, suggestions or ideas you have regarding the Website, the App and/or the Services (“**Feedback**”) via Discord, X, Reddit or other social media platform, and/or by email to our team. When you send us Feedback, you agree to grant Sorare a transferable, sub-licensable, free of charge, worldwide and for the legal duration of the potential property rights in your Feedback, right to use, copy, distribute, adapt, create derivative works, reproduce, distribute, modify, translate, make publicly available or publicly display your Feedback, by any present or future process and by any means of distribution on the Website, social networks or media (such as TikTok, Facebook, X, Instagram, Reddit, etc.) or platforms (such as YouTube), e-banners and promotional emails or newsletter and on Sorare intranet or in its internal documentation, whether in whole or in part, and whether as provided or as modified, for commercial or promotional purposes.

## 13. Sorare’s Content

**13.1. Proprietary Content**. The Website and the Services, including but not limited to its proprietary content, information, design (including the “look and feel” of the Website or Services) logos, text, graphics, images, icons, data, software, algorithms and scripts are and will remain the proprietary property of Sorare and our affiliates (and licensors, where applicable) (“**Proprietary Content**”). Any unauthorized extraction or reproduction of the Sorare database is prohibited.

**13.2**. Nothing in these Terms should be construed as granting you any property, patent, or other right in the Proprietary Content. You may not copy, imitate, use, distribute, or otherwise commercialize, in whole or in part, any of the Proprietary Content without prior written permission from Sorare. For the avoidance of doubt, you may not use the Proprietary Content to promote third-party products and services without prior written permission from Sorare. You also agree to not apply for, register, or otherwise utilize Sorare’s trademarks or logos, or any elements that may cause confusion, in any jurisdiction worldwide without acquiring Sorare’s prior written consent for each specific use. Sorare reserves the right to withhold consent at its sole and absolute discretion. 

**13.3**. For the avoidance of doubt, purchase of a Collectible does not confer any ownership or other rights in the Proprietary Content, Third-Party Collectible Elements, content, code, data or other materials or over Sorare’s rights and/or Third-Party Rights that may be associated with the Services.

## 14. Modification, Suspension or Discontinuation of the Services

**14.1**. We reserve the right to modify, suspend, or discontinue, in each case temporarily or permanently, part or all of the Services, products, offerings, promotions and/or features of the Website, App, or Services, or we may introduce new features and/or Services. Modifications, can occur for any reason, including (without limitation): improvement of the Website or the Services (such as offering new, modified or updated services, offering different content or services), necessary modifications for technical reasons, an increased or decreased number of users, a change in the Website, App, or Services, and/or other operational reasons, intellectual property obligations or claims, changes in licenses we hold from third parties or other third-party compliance requirements, significant changes due to specific and verifiable open market costs, necessary enhancements of the safety of users or third-parties or other material legal, regulatory, technical, marketing, product, or security reasons.

**14.2**. Users agree, to the fullest extent permitted by applicable law, that any modification, suspension or termination of access to the Services may be without notice, and that Sorare shall not be liable for the consequences of any such modification, suspension or termination.

**14.3**. If you do not agree to such modifications, you will have the right to terminate these Terms and delete your User account. If you do not delete your User account, your continued use of the Website, App, and/or the Services shall be considered as an implicit consent to the relevant modifications. 

**14.4. Inactive Accounts**. We reserve our right, upon reasonable prior written notice, to delete a User account that has been inactive for more than six (6) months, provided that the corresponding (i) Blockchain wallet is empty from any Collectible and Cryptocurrency, and (ii) Cash Wallet is empty from any Local Currency. 

**14.5. Beta Tests**. In the event a beta feature, product, service, or offering (each a “**Beta Tes**t”) is made available, we reserve the right to modify, suspend, or discontinue the Beta Test, in each case temporarily or permanently, at Sorare’s sole and absolute discretion and without any liability to you.

## 15. Prohibited Website, App and Services Use

**15.1**. Sorare wants the Website, App, and Services to be inclusive places where fans of all sports and teams across the globe can engage with their favorite sports and each other. In connection with the Website, App, and Services, including your User account, the Game and/or the Marketplace, you shall not:   
**15.1.1**. post or otherwise create material that is misleading, suspicious and/or that intends to defraud or confuse other Users;   
**15.1.2**. post or otherwise create material that is abusive, obscene, pornographic, defamatory, harassing, offensive, vulgar, threatening, or malicious or infringing on privacy or publicity rights, inciting violence, racial or ethnic hatred or which may be described as gross indecency or incitement to commit certain crimes or offenses;   
**15.1.3**. interfere with, disrupt, or attempt to gain unauthorized access to any other Users’ accounts, other Users’ Wallets and/or computer networks;   
**15.1.4**. attempt to use or access or use or access another User’s account;  
**15.1.5**. use a Sorare username to represent that you work for Sorare;    
**15.1.6**. impersonate any person or entity or misrepresent your affiliation with another person or entity;  
**15.1.7**. use your or other Users’ Account(s) and/or Wallet(s) to buy, sell or transfer fraudulently obtained or stolen Collectibles or Cryptocurrency;  
**15.1.8**. conduct transactions that are suspicious, fraudulent, or otherwise disruptive of the normal functioning of the Services;  
**15.1.9**. use the Services and/or effectuate transactions from your or other Users’ Account(s) in an effort to engage in price manipulation, fraud or other deceptive activity;  
**15.1.10**. withdraw Local Currencies from your Wallet to an external bank account where the name of the bank account owners’ does not match the information provided during the identity checks carried out by the Payment Provider;  
**15.1.11**. use any non-public information in connection with the Services and/or any transactions you effectuate from your or other Users’ Account(s).  
**15.2.** Additionally, and in order to maintain the integrity and proper functioning of the Website, App and the Services, there are certain restrictions regarding what you can do when visiting the Website, App, or while using your User account. In connection with the Website, App, the Services, your user account, the Game and/or the Marketplace, you shall not:  
**15.2.1**. attempt to access or access any non-public areas of the Website or App;  
**15.2.2**. interfere with any access or use restrictions, attempt to prevent or prevent another User from accessing or using the Website, App, or Services, or disrupt, interfere and/or overburden the Website, App, or the Services;  
**15.2.3**. use any robot, spider, or other automated means to access, scrape or scan the Website, App, and/or the Services and/or frame or mirror any part of the Website, App, and/or the Services in violation of these T&Cs;  
**15.2.4**. use any data mining or data gathering or extraction methods, or otherwise collect information about Sorare, and/or the Services in violation of these T&Cs and/or its Users;  
**15.2.5**. send viruses, worms, malware, ransomware, junk email, spam, chain letters, phishing emails, unsolicited messages, promotions or advertisements of any kind and for any purpose;  
**15.2.6**. attempt to probe, scan, compromise or test the vulnerability of the Website, App, the Services, system or network or breach any security or authentication;  
**15.2.7**. modify, adapt, translate, reverse engineer, disassemble or decompile any part of the Website, App and/or the Services (and/or any of their source code, algorithms, methods, or techniques) or create derivative works;  
**15.2.8**. resell, sublicence, rent, lease, offer or otherwise commercialize any Sorare offering, product and/or feature or infringe Sorare’s intellectual property rights on the Website, App and its components;  
**15.2.9**. use data collected from the Services for any commercial activity;  
**15.2.10**. violate any applicable laws, regulations or rules (including these Terms and Conditions and/or any applicable Additional Terms);  
**15.2.11**. remove or alter any copyright, trademark, confidentiality or other proprietary notices, designations, or marks from the Website, App, and/or Services;  
**15.2.12**. use the Website, App, and/or the Services for any illegal or unauthorized purpose, including but not limited to for money laundering or other illicit financial activities.  

## 16. Suspicious Activity and Violations

**16.1. Suspicious Activity**.

**16.1.1 Reporting Suspicious Activity**. We welcome you to report any fraudulent or illegal activities of other Users. Sorare in its sole and absolute discretion reserves the right to enquire into account activity considered suspicious and limit their access to the Services or Game and/or temporarily suspend their User account during such investigation. If you would like to report suspicious activity, you may contact the Support Team via the Help Center at [https://help.sorare.com/](https://help.sorare.com/). Any reporting will be kept strictly confidential and not disclosed to a third party, unless (i) to the extent required by applicable law or regulation, or (ii) pursuant to a subpoena or order of a court or regulatory, self-regulatory or legislative body of competent jurisdiction, or (iii) in connection with any regulatory report, audit, or inquiry, or (iv) where requested by a regulator.

**16.1.2. Investigations**. Sorare reserves the right to enquire into account activity considered suspicious and limit your access to the Services or Game and/or temporarily suspend your User account during such investigation, including where such activity has been flagged by other Users. Where Sorare has contacted you concerning suspicious activity you will provide any requested information.

**16.2**. Violation of these Terms and/or any applicable Additional Terms. You shall not use the Website, App, Services, and/or your User account in any way that violates these Terms and/or any applicable Additional Terms. Sorare reserves the right to limit a User account from accessing parts of the Services, temporarily suspend a User account and/or permanently suspend a User account where such User is suspected of violating or has actually violated these Terms and/or any other applicable Additional Terms. A temporary or permanent suspension of a User account will result in a cancellation of all potential Offers listed by this User on the Marketplace. 

**16.2.1. Service Limits**.

**16.2.1.1. Game Ineligibility**. We reserve the right to temporarily exclude you from the Game and/or remove you from any tournament(s) you are participating in if you are suspected of violating or have violated these Terms and Conditions, and/or any other applicable Additional Terms.

**16.2.1.2. Reward Ineligibility**. We reserve the right to withhold any prize(s) you have won in the event you are suspected of violating or have violated the Terms and Conditions and/or any other applicable Additional Terms.

**16.2.2. Account Suspension**. We reserve our right to suspend a User account from accessing all or part of the Services, temporarily or permanently. Your User account will be suspended including but not limited to where you violate or are suspected to have violated any part of these Terms and/or any applicable Additional Terms. 

**16.2.3. Permanent Suspension**. In case of permanent suspension, we will inform you by sending an email to the email address associated with your User account. You will then have a period of thirty (30) days to transfer any Local Currency, Collectibles and/or Cryptocurrency from your Wallets to an external wallet and/or bank account. You agree and acknowledge that if you do not make such transfer within the thirty (30) day period, any and all Local Currency, Collectibles and/or Cryptocurrency in your Wallets will be permanently irretrievable. Sorare shall not be responsible for any related loss.
Please note that in certain cases of breaches of these Terms and/or Applicable Laws, Sorare and/or the independent Payment Services Providers involved may decline, freeze, or hold this transfer for various reasons, including suspected fraud, AML compliance, adherence to economic or trade sanctions.

**16.3. Appeal Process**. If we take any action pursuant to this Section, you may have a right of appeal. For further information you may contact the Support Team at [https://help.sorare.com/](https://help.sorare.com/) by providing us with the following information: your name, username, email address, the full details of the issue, and where applicable Sorare’s previous response to your complaint if any and why you disagree with the outcome. We will ensure to investigate your complaint and get back to you as soon as reasonably possible.

**16.4**. You agree and acknowledge that Sorare has no responsibility towards you. To the fullest extent permitted by applicable law, you will not file a lawsuit or seek compensation from Sorare, its affiliates, officers, agents, consultants, vendors, equity holders, employees, successors, assigns, directors, partners, subsidiaries, contractors and licensors resulting from or in connection with Sorare’s choice to revoke or refuse your access to any Services, suspend or end your access to the Services, or undertake any other action during an inquiry into a suspected breach or upon determination by Sorare that a violation of these Terms and/or any Additional Terms has occurred.

## 17. Indemnification

**17.1**. To the fullest extent permitted by applicable law, you will indemnify and hold harmless Sorare, its affiliates, officers, agents, consultants, vendors, equity holders, employees, successors, assigns, directors, partners, subsidiaries, contractors and licensors against any claims, liabilities, damages, awards, judgments, losses, liabilities, obligations, penalties, fees, expenses (including, without limitation, attorneys’ fees) and costs in connection with:

- Your violation of these Terms and/or any applicable Additional Terms;
- Your violation of the Game Rules;
- Your use or misuse of the Services;
- Your violation of any third-party rights or your obligations to any third party, including the Third-Party Rights of any relevant Third-Party Collectible Elements and/or other Users;
- Your violation of any applicable law, regulation, or rule; and/or 
- Your negligence, gross negligence and/or willful misconduct.

**17.2**. To the fullest extent permitted by applicable law, you agree and acknowledge that Sorare will not indemnify you for any claims, damages, liabilities, awards, judgments and/or losses related to or in connection with your use of the Website, App and/or Services.

## 18. Liability

**18.1. General**. To the fullest extent permitted by applicable law Sorare, its affiliates, officers, agents, consultants, vendors, equity holders, employees, successors, assigns, directors, partners, subsidiaries, contractors and licensors disclaim all liability arising out of or in connection with the Website, the App and/or Services. We shall not be held liable for:

- your reliance on any statement, information and/or data made available on the Website, the App, the Services, and/or any social media platform or blog operated by Sorare;
- loss of Users’ Content, data, Common Cards, Collectibles, Local Currency or Cryptocurrencies;
- any event that may occur during Auctions and that could impact notably but not limited to the price, the Collectible, its final design, etc.;
temporary or permanent account suspensions;
- direct or indirect damage resulting from the use by a User or any third party of the Website, the App and/or Services;
- interruptions in the Website, App or Services, and/or any losses caused by errors, bugs, breaches and/or malfunctions;
- technologies provided by Third Party Services (as defined below);
- a malfunction or cyberattack;
- any case of force majeure;
- any malware or other harmful elements contained in the Website, App and/or Services. 

**18.2**. You understand and accept the risks inherent in the provision of information, online internet exchanges and experimental technologies such as the blockchain and non-fungible tokens, and agree that Sorare, its affiliates, officers, agents, consultants, vendors, equity holders, employees, successors, assigns, directors, partners, subsidiaries, contractors and licensors shall not be liable for any failure of the foregoing technologies and/or breach of the security of the Website and/or Services. Except in relation to any liability that cannot be excluded or limited by law, Sorare’s maximum, aggregate liability to an individual, relevant User, under and in connection with this these Terms and/or any Additional Terms, is limited to direct damages in an amount not to exceed the sum(s) actual paid by the individual, relevant User to Sorare in the twelve (12) months prior to the claim giving rise to such liability.

## 19. Assumption of Risk and Disclaimers 

**TO THE FULLEST EXTENT PERMITTED BY APPLICABLE LAW, YOUR USE OF THE WEBSITE, APP AND/OR SERVICES IS AT YOUR SOLE RISK. THE WEBSITE, APP AND/OR SERVICES ARE PROVIDED ON AN “AS IS” AND “AS AVAILABLE” BASIS. TO THE FULLEST PERMITTED BY APPLICABLE LAW, SORARE EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS, IMPLIED, OR STATUTORY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT. SORARE MAKES NO WARRANTY THAT (A) THE SERVICES WILL MEET YOUR REQUIREMENTS; (B) THE SERVICES WILL BE UNINTERRUPTED, TIMELY, SECURE, FREE OF ANY ERRORS, HARMFUL ELEMENTS AND/OR MALWARES; (C) THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SERVICES (INCLUDING ANY DATA PRESENTED THEREIN OR IN CONNECTION WITH, OR ON ANY THIRD-PARTY WEBSITE) WILL BE ACCURATE OR RELIABLE; (D) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE SERVICES WILL MEET YOUR EXPECTATIONS. TO THE FULLEST PERMITTED BY APPLICABLE LAW, SORARE DISCLAIMS ALL RESPONSIBILITY FOR ANY LOSSES THAT MAY BE INCURRED BY YOU AS A RESULT OF USING THE WEBSITE, THE APP, THE SERVICES, THE BLOCKCHAIN TECHNOLOGY AND/OR DIGITAL ASSET WALLETS.**

**In addition**, you acknowledge and agree that:
 
**19.1**. the applicable price of a digital asset may be highly volatile, and fluctuations in other digital asset prices could significantly (and negatively impact) any resale value a Collectible may have. These factors may result in the potential for permanent and total loss of the value of a Collectible, if demand for the Collectible disappears. There is no representation or warranty that a Collectible will maintain a specific value or price if offered for resale. There is no representation or warranty that purchasers of Collectibles will not incur losses if they choose to resell a Collectible.
 
**19.2**. The Website and/or App do not store, send, or receive Collectibles. Collectibles exist solely through ownership records maintained on the Website’s and App's supporting blockchain within the Ethereum network. Any Collectible transfers occur within the Ethereum network supporting blockchain, which Sorare does not control, not within the Website or App. 
 
**19.3**. Sorare relies on internet-based services, and as such, you acknowledge that interruptions, delays, or downtime in internet connectivity may occur. These disruptions may impact the accessibility and functionality of the Services: 

- ***Third-Party Services***: Sorare may utilize or provide links or other access to Third Party Services for various functions. Your access and use of the Third-Party Services may also be subject to additional terms and conditions, privacy policies, and other agreements with such third parties, and you may be required to authenticate to or create separate accounts to use Third-Party Services on the websites or via the technology platforms of their respective providers. Some Third-Party Services will provide Sorare with access to certain information that you have provided to third parties, including through such Third-Party Services, and we will use, store and disclose such information in accordance with our Privacy Policy. The reliability and security of these services are outside Sorare's direct control, and any issues with Third-Party Services may affect your experience on the platform. Sorare is not responsible for such Third-Party Services, including for the accuracy, availability, reliability, or completeness of information shared by or available Third-Party Services, or on the privacy practices of Third-Party Services. Sorare enables these Third-Party Services merely as a convenience and the integration or inclusion of such Third-Party Services does not imply an endorsement or recommendation. Any dealings you have with third parties while using the Services are between you and the third party. Sorare will not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on Third-Party Services.

- ***Data Security***: Despite employing industry-standard security measures, internet-based services are susceptible to data breaches, hacking, or unauthorized access. You acknowledge the inherent risks associated with the transmission of information over the internet. You are responsible for maintaining the confidentiality of your password and account details, if any, and are fully responsible for any and all activities that occur under your password or account. You agree to (a) immediately notify Sorare of any unauthorized use of your password or account or any other breach of security, and (b) ensure that you exit from your account at the end of each session when accessing the Services. Sorare will not be liable for any loss or damage arising from your failure to comply with this paragraph.

To the fullest extent permitted by applicable law, you acknowledge that Sorare is not responsible for any communication failures, disruptions, errors, distortions, or delays experienced while using internet-based services in relation with the Services, Website and/or App, regardless of the cause. 
 
**19.4**. Using blockchain based services may involve the following risks:

- The decentralized and distributed nature of blockchain technology may expose you to technical glitches, cyber-attacks, or other unforeseen issues that could result in the loss of digital assets or disruption of services.

- Network Risks: Delays, interruptions, congestion, or other unpredictable failures on the blockchain network may affect the speed and efficiency of transactions. 

Because of these risks, a transaction for a Collectible, including the sale or other transfer of ownership of the Collectible, may be (i) irreversible, and losses caused by fraudulent or accidental transactions may occur; or (ii) recorded on the ledger of the Ethereum network at a time later than when you or Sorare initiated the transaction involving the Collectible. You acknowledge that Sorare is not responsible for any communication failures, disruptions, errors, distortions, delays experienced and/or any other harmful effects, losses or damages incurred while using blockchain networks, regardless of the cause.

**19.5**. Blockchain Wallets require careful management of Users’ private keys. You are the only one able to access the private key of your Blockchain Wallet. If you lose access to your private key or fail to secure it properly, you may lose access to the Collectibles and digital assets linked to your Blockchain Wallet. Please note that Blockchain Wallets may be susceptible to hacking, phishing, or other security breaches. It is your responsibility to take appropriate security measures to protect your wallet.
 
**19.6**. The Collectible does not represent an investment, security, share, equity interest, debt, loan, or derivative instrument related to any of these. The Collectible is not intended to be a “security” under any statute, regulation, rule, order, treaty, or other laws governing securities, including, but not limited to, the Securities Act of 1933 (United States), the Securities Exchange Act of 1934 (United States), or the Investment Company Act of 1940 (United States), as amended. Neither the provided Terms nor any information conveyed by Sorare constitutes a prospectus or offering document, nor do they present an offer to sell or solicit the purchase of Collectible or any other asset.
 
**19.7**. You bear sole responsibility for determining applicable taxes on transactions related to your Collectible. Sorare does not assume responsibility for determining taxes related to your transactions on the App, Website, or blockchain-based smart contracts.
 
**19.8**. The regulatory frameworks governing blockchain technologies, cryptocurrencies and tokens remain uncertain, and new regulations or policies throughout the world could significantly impact the development of the Sorare ecosystem and the potential utility or value of Collectibles.
 
**19.9**. Collectibles cannot be received, used, held by, transferred, or sold to individuals or entities to a country that the U.S. government, European Union and/or United Nations has placed embargo restriction on or if you are added to the U.S. government, European Union or United Nations list of restricted parties or persons (including being a specially designated national by the U.S. government).
 
## 20. Account Deletion and Termination

**20.1. Account Deletion**.

**20.1.1**. You may terminate these T&Cs at any time by requesting the deletion of your User account and discontinuing your use of the Website, App, and Services.

**20.1.2**. The deletion of your account will result in the irreversible deletion of all User data associated with the account. You must transfer any Local Currency, Collectibles and Cryptocurrencies stored on your Wallets to an external wallet or bank account before any deletion request. Any Local Currency, Collectibles and/or Cryptocurrency stored on your Wallets that have not been transferred out of your account at the date of deletion will be permanently irretrievable. Sorare will not be responsible for the permanent loss of any Local Currency, Collectibles and/or Cryptocurrency that was not saved prior to a deletion request.

**20.1.3**. Sorare may at any time, for any reason and in its sole and absolute discretion, terminate these Terms. In case of termination of these Terms, Sorare will provide you with the opportunity to withdraw your Local Currency, Collectibles and Cryptocurrencies. Please note that in certain cases of breaches of these Terms and/or Applicable Laws, Sorare and/or the independent Payment Services Providers involved may decline, freeze, or hold this transfer for various reasons, including suspected fraud, AML compliance, adherence to economic or trade sanctions. To the fullest extent permitted by applicable law and notwithstanding anything to the contrary, the termination and/or suspension of a User account will be without prejudice to any other legal action that Sorare may take against the User.

## 21. Amendment

**21.1**. We reserve the right to modify or amend these Terms, and/or any Additional Terms at any time. If we make material changes to any of the foregoing terms, we will notify you about these amendments in advance, via a dedicated communication method (such as an email sent to the email address associated with your User account) and allow for a reasonable period for you to review the updated terms. If you do not want to agree to any updated terms, you may terminate the present Terms by requesting deletion of your Using account and automatically discontinuing your use of the Services. If you do not terminate the Terms before the expiry of the period of time specified following the update notification, your continued use of the Services shall constitute acceptance of the revised terms.

**21.2**. The applicable version of the Terms is the latest version published on the date of use of the Services.

## 22.Miscellaneous

**22.1**. Failure by Sorare at any time to exercise its rights under these Terms shall not constitute the waiver of such right.

**22.2**. If any term, provision, or section of these Terms is held invalid or unenforceable, then that term, provision or section will be deemed removed from these Terms and will not affect the validity or enforceability of any remaining part of such provision, section or any other term, provision or section of these Terms, provided that the essential provisions of these Terms remain in effect.

**22.3**. All obligations which by their nature should or are intended to survive the termination or expiration of these Terms shall continue in full force and effect, regardless of the termination of these Terms by you or Sorare.

**22.4**. The Website may periodically feature sweepstakes, contests, raffles, or similar promotions (“**Promotions**”) via the Website. Such Promotions may be governed by rules separate from these Terms. In the event Promotion rules conflict with these Terms, the Promotion rules shall control, but solely in connection with the Promotion.

## 23. Dispute Resolution by Binding Arbitration

**PLEASE READ THIS SECTION CAREFULLY AS IT AFFECTS YOUR RIGHTS.**

**23.1. Dispute Resolution**.

**23.1.1**. If you are having an issue or would like to raise a concern, you should contact us at [legal@sorare.com](mailto:legal@sorare.com). Please include your contact details and provide any additional information and relevant context.

**23.1.2**. If you are a French resident, pursuant to the French Consumer Code we offer access to such alternative dispute resolution program where you have a complaint that we cannot resolve. Before filing a claim with the alternative dispute resolution entity, you must first send us the claim. We shall then attempt to resolve the complaint internally. If we cannot resolve the complaint or if you are unhappy with the solution provided, and if your claim is less than one year old, you can address your claim to CNP Mediation byost at the following address: 

**CNPMEDIATION CONSOMMATION**   
27, avenue de la Libération   
42400 Saint-Chamond, France   

Electronically directly at [https://www.cnpm-mediation-consommation.eu/](https://www.cnpm-mediation-consommation.eu/).

**23.1.3**. If you are a California resident, in accordance with California Civil Code Section 1789.3, you may report complaints to the Complaint Assistance Unit of the Division of Consumer Affairs of the California Department of Consumer Affairs by contacting them in writing at 400 R Street, Sacramento, California 95814, or by telephone at (800) 952-5210.
Additionally, users may contact an independent Ombudsman for free by sending a request to this effect, by email, to the address [legal@sorare.com](mailto:legal@sorare.com) or by post to the Sorare’ address. Upon receipt, Sorare undertakes to work towards a timely resolution.

**23.1.4**. Users in the European Union may also contact the online dispute resolution service of the European Commission at [ec.europa.eu/consumers/odr](ec.europa.eu/consumers/odr). 

**23.2. Applicable law**. 

**23.2.1**. These T&Cs, the Terms of Use and any applicable Additional Terms are governed by French law. However, if you are a consumer and resident of any European Union country you will benefit from mandatory provisions of, and legal rights available to you under, the laws of that country. Nothing in these Terms affects your rights as a consumer to rely on these local law mandatory provisions and legal rights.

**23.2.2**. Notwithstanding the foregoing, if you are based in the United States, by using the Services you therefore agree that these Terms and any counterparts, amendments, or revisions thereto shall be governed and construed in accordance with the New York State law, in the instance of any dispute of any sort arising between you and Sorare, regardless of the conflict of law provisions.  

**23.3. Arbitration Agreement and Class Action Waiver**
  
**23.3.1. Arbitration Agreement**. If you live in the United States, you and Sorare agree to resolve any claims or disputes involving Sorare, its parent, subsidiaries, affiliates, and licensors relating to or arising out of these Terms through a final, mandatory and binding individual arbitration (“**Arbitration Agreement**”). You agree that, by entering into these Terms, you and Sorare are each waiving the right to a trial by jury or to participate in a class action. Your rights will be determined by a neutral arbitrator, not a judge or jury. The Federal Arbitration Act governs the interpretation and enforcement of this Arbitration Agreement.

**23.3.2. Exceptions**. The only claims and disputes involving directly or indirectly Sorare, its parent, subsidiaries, affiliates, and licensors which are not covered by the Arbitration Agreement are (i) small claims court, (ii) Intellectual Property rights or (iii) any claims for injunctive relief. Further, this Arbitration Agreement does not preclude you from bringing issues to the attention of federal, state, or local agencies, and such agencies can, if the law allows, seek relief against us on your behalf. 

**23.3.3. Arbitration procedure**. Most of your concerns can be resolved quickly and satisfactorily by entering in contact with the Help Center available at [https://help.sorare.com/](https://help.sorare.com/). If Sorare cannot resolve your concern, you and Sorare agree to be bound by the procedure set forth by the Arbitration Agreement to resolve any and all claims or disputes arising between Sorare, its parent, subsidiaries, affiliates, and licensors. If the claim or dispute has not been settled amicably within thirty (30) days from when the resolution process was instituted through the Help Center, any party may elect to commence arbitration according to this Arbitration Agreement. Any claim or dispute arising out of or in connection with the Terms, shall be referred to and finally resolved by binding arbitration under the American Arbitration Association’s (“**AAA**”) rules and procedures, including the AAA’s Consumer Arbitration Rules (collectively, the “**AAA Rules**”), as modified by this Arbitration Agreement, which Rules are deemed to be incorporated by reference into this clause or accessible by this link. For information on the AAA, please visit its website, [http://www.adr.org](http://www.adr.org). Information about the AAA Rules and fees for consumer disputes can be found at the AAA’s consumer arbitration page, http://www.adr.org/consumer_arbitration. The number of arbitrators shall be one. The seat, or legal place, of arbitration, shall be in New-York City, United States. The language to be used in the arbitral proceedings shall be English. The arbitrator must also follow the provisions of these Terms as a court would. All issues are for the arbitrator to decide, including, but not limited to, issues relating to the scope, enforceability, and arbitrability of this Arbitration Agreement. Although arbitration proceedings are usually simpler and more streamlined than trials and other judicial proceedings, the arbitrator can award the same damages and relief on an individual basis that a court can award to an individual under the Terms and applicable law. Decisions by the arbitrator are enforceable in court and may be overturned by a court only for very limited reasons.
 
If your claim is for $10,000 or less, Sorare agrees that you may choose whether the arbitration will be conducted solely on the basis of documents submitted to the arbitrator, through a telephonic hearing, or by an in-person hearing as established by the AAA Rules. If your claim exceeds $10,000, the right to a hearing will be determined by the AAA Rules. Regardless of the manner in which the arbitration is conducted, the arbitrator shall issue a reasoned written decision sufficient to explain the essential findings and conclusions on which the award is based.
 
**23.3.4. Costs of Arbitration**. Payment of all filing, administration, and arbitrator fees (collectively, the “**Arbitration Fees**”) will be governed by the AAA Rules, unless otherwise provided in this Arbitration Agreement. If the value of the relief sought is $75,000 or less, at your request, Sorare will pay all Arbitration Fees. If the value of relief sought is more than $75,000 and you are able to demonstrate to the arbitrator that you are economically unable to pay your portion of the Arbitration Fees or if the arbitrator otherwise determines for any reason that you should not be required to pay your portion of the Arbitration Fees, Sorare will pay your portion of such fees. In addition, if you demonstrate to the arbitrator that the costs of arbitration will be prohibitive as compared to the costs of litigation, Sorare will pay as much of the Arbitration Fees as the arbitrator deems necessary to prevent the arbitration from being cost-prohibitive. Finally, if the value of the relief sought is $75,000 or less, Sorare will pay reasonable attorneys’ fees should you prevail. Sorare will not seek attorneys’ fees from you. But, if you initiate an arbitration in which you seek more than $75,000 in relief, the payment of attorneys’ fees will be governed by the AAA Rules.
 
**23.3.5. Confidentiality**. All aspects of the arbitration proceeding, and any ruling, decision, or award by the arbitrator, will be strictly confidential for the benefit of all parties.

**23.3.6. Opt-out**. **YOU MAY WITHDRAW FROM THIS ARBITRATION AGREEMENT**. To withdraw, you must notify Sorare in writing no later than thirty (30) days after becoming subject to these Terms. Your withdrawal request must include your name and address, your Sorare username and the e-mail address you used to create your Sorare account, and an unequivocal statement that you wish to withdraw from this Arbitration Agreement to arbitrate. You must send your withdrawal request by post to this address: Sorare, 5 avenue du Général de Gaulle, 94160, Saint-Mandé, France or by email to the address: [legal@sorare.com](mailto:legal@sorare.com).
Notwithstanding any provision in this Terms of Service to the contrary, Sorare agrees that if it makes any future change to this Arbitration Agreement (other than a change to the Notice Address) while you are a user of the Services, you may reject any such change by sending Sorare written notice within thirty (30) calendar days of the change to the Notice Address provided above. By rejecting any future change, you are agreeing that you will arbitrate any dispute between us in accordance with the language of this Arbitration Agreement.

**23.3.7. Class action waiver**. **TO THE EXTENT PERMITTED BY APPLICABLE LAW, YOU AND SORARE AGREE THAT EACH OF YOU MAY BRING ANY CLAIMS OR DISPUTES AGAINST THE OTHER ONLY IN YOUR INDIVIDUAL CAPACITIES AND NOT AS A PLAINTIFF OR CLASS MEMBER IN ANY ALLEGED REPRESENTATIVE, WIDECLASS ARBITRATION OR CLASS ACTION. UNLESS BOTH YOU AND SORARE AGREE, NO ARBITRATOR OR JUDGE MAY CONSOLIDATE THE CLAIMS OR DISPUTES OF MORE THAN ONE PERSON OR PRESIDE OVER ANY FORM OF REPRESENTATIVE OR CLASS ACTION. THE ARBITRATOR MAY AWARD RELIEF (INCLUDING MONETARY, INJUNCTIVE, AND DECLARATORY RELIEF) ONLY IN FAVOR OF THE INDIVIDUAL PARTY SEEKING RELIEF AND ONLY TO THE EXTENT NECESSARY TO PROVIDE RELIEF NECESSITATED BY THAT PARTY’S INDIVIDUAL CLAIM(S), EXCEPT THAT YOU MAY PURSUE A CLAIM FOR AND THE ARBITRATOR MAY AWARD PUBLIC INJUNCTIVE RELIEF UNDER CALIFORNIA LAW, INCLUDING UNDER THE CALIFORNIA CONSUMER LEGAL REMEDIES ACT, THE UNFAIR COMPETITION LAW, AND THE FALSE ADVERTISING LAW**. 

**23.3.8. Severability**. Without limiting the severability provision of Section 23.2 of the T&Cs , if a court or an arbitrator decides that any term or provision of this Arbitration Agreement other than the class action waiver above is invalid or unenforceable, the parties agree to replace such term or provision with a term or provision that is valid and enforceable and that comes closest to expressing the intention of the invalid or unenforceable term or provision, and this Arbitration Agreement shall be enforceable as so modified. If the class action waiver is found to be unenforceable under the mandatory laws of your country of residence or if any part of this Arbitration Agreement is found to be unenforceable, then the entirety of this Arbitration Agreement shall be null and void, unless the class action waiver is deemed to be invalid or unenforceable solely with respect to claims for public injunctive relief under California law. In such event, the parties agree to take legal action in a court of law and agree that all claims or disputes (whether contractual or otherwise) arising out of or in connection with the Terms or the use of the Services shall be settled exclusively by the New York State courts, unless otherwise permitted by the law of the state in which you reside. The remainder of the Terms will continue to apply.

# Affiliate Program Terms

## 1. Scope

**1.1.** You may apply to and participate in the affiliate program (the “**Affiliate Program**") pursuant to the Terms and Conditions, these Affiliate Program Terms (“**Affiliate Terms**”) and the Master Campaign Agreement. The Affiliate Program offers you the opportunity to earn a commission on revenue generated by other Users you have introduced to Sorare, for the eligible sport(s) listed on the affiliate program page.

## 2. Application

**2.1.** 
In order to participate in the Affiliate Program, you must send an application here. In order to be eligible to apply, you must:

- have a validated account on the Website;
- accept and comply with the Terms and Conditions and these Affiliate Terms;
- apply for participation in the Affiliate Program through the Website [https://sorare.com/affiliate_program](https://sorare.com/affiliate_program). Your application must be accurate and complete to the best of your knowledge.

## 3. Acceptance and Participation

**3.1.** **Acceptance.** Sorare will notify you via email if your application has been accepted or declined. Acceptance depends on factors such as but not limited to the number of current Affiliate Program participants, community reach, relevance, etc. Sorare reserves the right to accept or decline an application in its sole discretion, without any reason or prior notice.

## 4. Participation

**4.1** During the application process, you must enroll with our Affiliate Program Partner, Impact (“**Affiliate Program Partner**”). Please note that our Affiliate Program partner is a Third-Party Service. In order to participate in the Affiliate Program, you must additionally accept their [Master Campaign Agreement](https://app.impact.com/content/MCA.ihtml).

**4.2.** After you have successfully enrolled (“Affiliate” or “Affiliates”),  you may generate an affiliate link under the conditions indicated on the Affiliate Program Partner website (the "**Affiliate Link**").

## 5. Qualified Leads

**5.1.** To be considered as an Affiliate-referred User (a “Lead”), the following conditions must be met:

(i) The Lead must be a natural person, at least eighteen (18) years of age and separate from the Affiliate, who has never created a Sorare User account and was not in the process of creating a User account.

(ii) The Lead must:

- create an account by accessing the Website through the Affiliate’s Affiliate Link;
- verify the User account;
- accept the T&Cs.

(iii) If the Affiliate Link used by the Lead has been promoted, directly or indirectly, through or with the help of paid advertising, any referrals created via that promoted link will not be eligible for rewards under this Section.

## 6. Affiliate Fees

**6.1.** In consideration of the qualified Leads made, the Affiliate shall receive a commission of 10% (the “Affiliate Fees”), of the revenue generated by the Leads for a one-year period from the date of creation of the account by the Lead. Whether or not taxes are included will be updated automatically depending on the requirements of your region.

**6.2.** The Affiliate understands and expressly agrees that Sorare will only pay Affiliate Fees due for Leads referred through the Affiliate Link automatically tracked and reported by the systems of Sorare or its Affiliate Program Partner. The Affiliate understands and acknowledges that, unless proven otherwise, the tracking data of the qualified Leads kept in the information system of Sorare, Affiliate Program Partner or other Third-Party Service will constitute the final and valid record of the Affiliate Fees due. Sorare will not be liable for any Affiliate Fees not recorded.

**6.3.** Affiliate Fees are paid the month following the month in which the revenue was generated by the Lead.

## 7. Violations

**7.1.** In case of fraud or attempted or suspected fraud or violation of the Terms and Conditions or these Affiliate Terms, Sorare reserves the right to limit the relevant User from accessing the Services and/or participating in the Affiliate program, and to undertake other action in connection with the relevant User Account(s) pursuant to the Terms and Conditions, without prejudice to any legal action that may be taken by Sorare against the relevant User.

**7.2.** You acknowledge and agree that where you are reasonably found to have submitted fraudulent Leads for which you have been paid Affiliate Fee(s) for, and/or if the your participation in the Affiliate Program has been accepted by Sorare on the basis of false information, you will be liable to refund Sorare the full amount of the Affiliate Fees paid to you.

## 8. Miscellaneous

**8.1** The conditions and benefits set out in these Affiliate Terms may, at any time and without prior notice, be unilaterally updated. Continued participation in the Affiliate Program following an update shall constitute acceptance of the updated term(s).

