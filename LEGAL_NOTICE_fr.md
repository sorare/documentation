# Mentions légales

## Information SORARE SAS

SORARE SAS est une société par actions simplifiée de droit français au capital de 2 405 149,31 €, immatriculée à Créteil, France, sous le numéro RCS 844 355 727, dont le siège social est situé au 5, avenue du Général de Gaulle, 94160 Saint-Mandé, France.

Numéro d’identification TVA de SORARE SAS : FR75844355727

Pour toute demande d’assistance : support@sorare.zendesk.com

Représentant légal et Directeur de la publication : Nicolas Julia

## Hébergement web

Amazon Web Services EMEA SARL

Située au 38 avenue John F. Kennedy, L-1855 99137 Luxembourg

Contact : https://aws.amazon.com/fr/contact-us/

Téléphone : +352 26 73 30 00

## Propriété intellectuelle

Tous les éléments présents sur le site, y compris les marques, photographies, textes, commentaires, illustrations, images animées ou non, séquences vidéo, sons, ainsi que les applications informatiques utilisées pour son fonctionnement, sont soumis à la protection accordée par les lois sur la propriété intellectuelle en vigueur.

Ces éléments sont la propriété exclusive de Sorare SAS ou de ses partenaires. Toute reproduction, représentation, utilisation ou adaptation, sous quelque forme que ce soit, de la totalité ou d'une partie de ces éléments, y compris les applications informatiques, sans l'accord préalable et écrit de l'éditeur, est strictement interdite. Le fait que l'éditeur ne déclenche pas immédiatement des procédures judiciaires dès qu'il a connaissance de telles utilisations non autorisées ne constitue en aucun cas une acceptation de ces utilisations ni une renonciation à l'intention de poursuivre en justice.
