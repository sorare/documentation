# Loaned Cards Terms and Conditions

These Loaned Cards Terms and Conditions (the “Loan Terms”) govern your eligibility to the Sorare.com’s Loan Program and your use of Loaned Cards (as both defined below). To participate in the Loan Program, you must accept the Sorare Terms and Conditions and these Loan Terms. Capitalized terms not defined here will have the meaning given to them in the Terms and Conditions, Game Rules, and/or additional Applicable Terms.

## 1. The Loan Program   

1.1. This program (the “Loan Program”) offers some Users the opportunity to experience Sorare Pro using Collectibles that are made temporarily available to them to play in the Game (the “Loaned Cards”). 

1.2. The eligibility criteria for the Loan Program, including but not limited to user segments, geographic restrictions, and any other applicable conditions, are determined at Sorare’s sole discretion and may be updated from time to time. The specific eligibility requirements applicable at any given time will be detailed on the Website or App where the Loan Program is presented. 

## 2. Authorized Use of Loaned Cards

2.1. Users eligible to the Loan Program are granted a limited and revocable license to use and display on the Website or App specific Collectibles to compete in Sorare Pro Competitions. That license may be revoked at any time by Sorare without prior notice to the holding User. 

2.2. Unless otherwise communicated on the Website or App, holders of Loaned Cards can participate in the same Sorare Pro Competitions as owners of Collectibles with similar characteristics in terms of season, scarcity and other in-game eligibility criteria. Subject to compliance with these Loan Terms and Sorare’s Terms and Conditions, the rewards that Users may win using Loaned Cards, including Collectibles, Essence and ETH, are theirs to keep. 

2.3. The scarcity of Loaned Cards that are made available to Users, as well as the player represented on Loaned Cards, are at Sorare’s sole discretion. Additionally, any bonuses or in-game benefits applicable to Loaned Cards, including but not limited to XP, in-season bonuses, or other bonuses, are determined at Sorare’s sole discretion and may be modified or revoked at any time without prior notice.

## 3. Usage Restrictions 

3.1. Users have no claim, title, or ownership interest in Loaned Cards, and Sorare retains all rights to modify, reclaim, or revoke Loaned Cards at any time. The holder of a Loaned Card is granted a license to use that Collectible in connection with the Game for a limited period of time as specified on the Website or App, but is in no event granted any other rights in connection with such Collectible. Notably, holding a Loaned Card does not grant a User the right to freely dispose of their Loaned Card through sales, loans, donations or transfers, including in the Marketplace or through a Third-Party Service or marketplace.

3.2. In order to maintain a fair and equitable gameplay for all Users, Sorare reserves the right to apply any additional limitation to the use of Loaned Cards at any time and without notice.

## 4. Third-Party Rights Restrictions

4.1. No license to Third-Party Rights is granted to a User in connection with their Loaned Cards.

4.2. Users may not, under any circumstances and without Sorare’s prior written consent, attempt to carry out or carry out any of the following with Loaned Cards, whether or not held by them: (i) use the Loaned Cards and any of the Third-Party Card Elements for commercial purposes, advertising or promotion of a third party product or service; (ii) market merchandise, physical or digital, that represents the Loaned Card; (iii) alter the image associated with the Loaned Card, and/or alter, edit or modify the Third-Party Card Elements in any other way; (iv) attempt to claim any additional intellectual property rights relating to the Loaned Card or the Third-Party Card Elements; (v) violate any applicable Third-Party Right; and/or (vii) use the Third-Party Card Elements and/or the Loaned Card in connection with images, videos, or other forms of media that depict hatred, intolerance, violence, cruelty, or anything else that could reasonably be found to constitute hate speech, defamation or otherwise infringe upon the rights of others, including the image right(s) of the featured player.

## 5. Miscellaneous

5.1. In case of actual, suspected and/or attempted fraud and/or abuse, violation of these Loan Terms and/or the Terms and Conditions, Sorare reserves the right to limit the relevant User from accessing the Services and/or participating in the Loan Program and/or receiving Rewards from their use of Loaned Cards, and to undertake other action in connection with the relevant User account(s) pursuant to the Terms and Conditions, without prejudice to any legal action that may be taken by Sorare against the relevant User.

5.2. The conditions and benefits set out in these Loan Terms may, at any time and without prior notice, be unilaterally updated. As these Loan Terms govern a specific promotional offer and not the general terms and conditions of the Sorare platform, any such updates shall not be considered a material modification of Sorare’s overall Terms and Conditions. Continued participation in the Loan Program following an update shall constitute acceptance of the updated term(s).

5.3. Sorare reserves the right, in its sole discretion, to modify, limit, or discontinue the Loan Program at any time, each without penalty or further obligation to you.