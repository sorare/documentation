# Sorare Policies Repository

Welcome to the Sorare Policies Repository. This is where we maintain our latest Terms & Conditions, Privacy Policy, and Cookie Policy.

## Contents

1. [Introduction](#introduction)
2. [Terms & Conditions](#terms-conditions)
3. [Privacy Policy](#privacy-policy)
4. [Cookie Policy](#cookie-policy)
5. [Contributing](#contributing)

## Introduction

Sorare is a global fantasy sport online game where managers can trade and play with officially licensed digital cards. As users interact with our platform, we strive to maintain transparency and fairness in our operations. This repository hosts the most updated versions of our Terms & Conditions, Privacy Policy, and Cookie Policy to ensure that our users are aware of the rules, their rights, and our responsibilities.

## Terms & Conditions

The Terms & Conditions outline the rules for using the Sorare platform. They include important information such as user obligations, restrictions on use, intellectual property rights, and the resolution of disputes.

[Link to the Terms & Conditions](./TERMS.md)

## Privacy Policy

Our Privacy Policy provides information about how we collect, use, and protect your personal data. It covers details on the type of information we collect, how we use this information, and how we safeguard your personal data.

[Link to the Privacy Policy](./PRIVACY_POLICY.md)

## Cookie Policy

The Cookie Policy describes how we use cookies and similar technologies on our platform to enhance user experience.

[Link to the Cookie Policy](./COOKIE_POLICY.md)

## Contributing

While this repository is primarily for informational purposes, we appreciate feedback and suggestions. Feel free to create issues and submit pull requests.
