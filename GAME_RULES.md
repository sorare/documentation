# Table of Contents

- [Sorare Football - Game Rules](#so5-game-rules)
- [Sorare: MLB - Game Rules](#mlb-game-rules)
- [Sorare NBA - Game Rules](#nba-game-rules)
- [Alternative Entry Method Rules](#no-card-entry-rules)

<a id="so5-game-rules"></a>

# Sorare Football Game Rules

_Last update: February 14, 2025_

Sorare Football is organized by Sorare SAS. Participation in Sorare Football (“Game”) and in any game mode (including, without limitation, “Sorare Pro”, “Sorare Rivals” and the “Collection Game”) (“Game Mode(s)”) is governed by the Terms and Conditions, these Sorare Football Game Rules (“Game Rules” or “Rules”), the Privacy Policy and any applicable Additional Terms. Capitalized terms not defined here will have the meaning given to them in the Terms and Conditions.

## 1. The Game

### 1.1 Gameplay basics

Sorare Football consists of different competitions that track the real performance of football players on the field (“Competition(s)”). The Competitions are accessible from a dedicated area ("Lobby") on the Website and App. The Game and all Competitions are free-to-play and no entry fee is required to enter a Team (as defined below). Your objective in the Game is to use your football knowledge and general manager strategies to create the best lineup(s) of five (5) Collectibles and/or Common Cards (“Cards”) representing professional football players, enter the lineup(s) into Competitions to compete with other Users, and progress through the different levels of the Game Mode(s)in which you are competing. Each lineup you enter must include five (5) different players’ Cards with at least one (1) goalkeeper, one (1) defender, one (1) midfielder, one (1) forward and one (1) extra (midfielder, forward or defender) (collectively a “Team”). Sorare reserves the right, in its sole discretion, to determine the number and frequency of Competitions. Each Team is awarded points based on the performance of each of the players featured in the relevant Cards. Each player's points are awarded based on that player’s actual performance in real games during the Competition. The winner of a Competition is the User who finishes the Competition period with the most points. The winning User and other participants may win a reward, as specified in the Lobby and outlined below.

### 1.2 Eligibility

You need to have a valid Sorare account to participate in the Game. You are prohibited from participating in the Game where doing so would be in violation of the Terms and Conditions and/or an agreement you have entered into with a third party.

### 1.3 Scoring and Winning

**Scoring system.** Points are awarded for each player’s Card on the Team, on the basis of data provided by a Third-Party Service regarding the featured player’s real life performance in a game during the Competition period. More information about the point systems are available on the [Help Center](https://help.sorare.com/hc/en-us/articles/4402897588241).

The winner of a Competition is the User who composed the Team that was awarded the most points at the end of the Competition period. In some Competitions, rewards are also granted to Teams finishing in other positions. Unless otherwise stipulated in the Game Rules, in the event two Users' scores are tied, the User who submitted their line up first will be considered to have the higher score.
Winners are notified via their User account. Participants’ usernames will be displayed on the Website for the duration of the Game.

**Delayed scoring.** Due to circumstances that are outside of Sorare’s control, Sorare may not receive from the data partner a Player’s statistics in connection with a specific game in real-time. If Sorare does not receive a Player’s full statistics before the scoring for a Competition is closed (as indicated on the Website or App), you acknowledge and agree that only a restricted number of statistics will be taken into account to determine that Player’s definitive scoring for that Competition. Please refer to our Help Center [article](https://help.sorare.com/hc/en-us/articles/4588357481885) for more information.

**Scoring bonuses.** Points awarded to each Player’s Card on the Team may be augmented by an additional bonus in Competitions where applicable. More information about the bonus systems are available on the [Help Center](https://help.sorare.com/hc/en-us/articles/4402897813777-How-do-Sorare-Football-Card-Levels-XP-and-Bonuses-work-).

### 1.4. Rewards

**Available rewards.** Available rewards for winners are communicated during the Entry Period of the Competition on the Website or App. The rewards(s) available will depend on the Competition and may be based on either (i) your Team's final place on the Competition leaderboard, and/or (ii) the number of points your Team scored at the end of the Competition (and as always specified on the Website or App).

The reward(s) granted for a given Competition may be **Wallet Rewards (Collectibles, ETH or User’s Preferred Currency), Credits, in-game items (notably XP, Essence, Reputation Points or Arena Tickets), merchandise, real-life experiences**. The scarcity of the Collectible awarded to the winner(s) depends on the Competition and their place in the ranking and/or Team's score, as applicable. The player represented by Collectible is at Sorare’s discretion. Any above-mentioned reward may, at Sorare’s sole discretion, either be awarded (i) as an individual reward, or (ii) as part of a Reward Box (as defined below).

Following verification of each winner’s eligibility and compliance with these Rules, the Terms and Conditions and any other applicable Additional Terms, the prize(s) will be transmitted pursuant to these Game Rules. By participating in the Game, you give your express permission to be contacted by Sorare by telephone and/or email to administer rewards.

**Reward Boxes.** A Reward Box is a one-time use in-game item that offers Users the opportunity to randomly win a reward from a selection of prizes. The prizes available in a specific Reward Box, as well as the probability for each prize to be won by a User, will be disclosed in advance by Sorare on the Website or App. You acknowledge and agree that an unclaimed and/or unopened Reward Box can be definitely removed from your inventory at all times, subject to a reasonable prior notice, and that you would definitely lose the option to redeem any prize that may have been associated with that Reward Box.

**Thresholds and Streaks**. In addition to Competition rewards, you may be eligible for an extra reward, as indicated on the Website or App, if you (i) compete with a full Team of In-Season Collectibles (subject to other eligibility requirements) and (ii) your Team reaches a designated point threshold (“Threshold”). You may need to reach Thresholds multiple times consecutively (“Streak(s)”) to unlock rewards, as specified on the Website or App. The required points and Collectibles (including, without limitation, their scarcity) to win a Threshold or Streak reward are determined solely by Sorare. Additional conditions for Threshold and Streak rewards may be communicated by Sorare on the Website or App from time to time.

**Automatic claim**. If you have not claimed your rewards within 14 calendar days of their distribution, or such other period as Sorare may, in its sole discretion, determine and notify you of on the Website or the App, we reserve the right to automatically claim your rewards, which will be added directly to your Club, Wallet or inventory, as applicable.

## 1.5. Declining a Reward

In the event you do not want a merchandise or real life experience reward you have validly won, you may decline your reward by responding to the reward notification email. If you decline a reward, you will under no circumstances be entitled to an alternative reward or compensation. By declining a reward you renounce any claim to the reward.

## 1.6. Exclusion and Fraud

Sorare reserves the right to exclude any Users who do not comply or are reasonably suspected of not complying with these Rules, the Terms and Conditions, and any applicable Additional Terms. Any violation of these Rules, the Terms and Conditions, and/or any applicable Additional Terms will result in the User’s immediate disqualification from the Competition and other Service limits, as provided for in the Terms and Conditions.

## 2. Game Modes

### 2.1. Sorare Pro

Entering a Team in a Sorare Pro Competition is restricted to Users who are eighteen (18) years of age or older, except in connection with some Onboarding Competitions (as defined below) when applicable,  except in connection with some Onboarding Competitions (as defined below) when applicable.

Sorare Pro Competitions use a bi-weekly schedule, with each Competition spanning over multiple weekdays and real-life football games (“Game Week” or “GW”). Competitions available in Sorare Pro are of two types: In-Season Competitions and Classic Competitions (as detailed below, collectively referred to as “Sorare Pro Competitions”). Where specified, you may also have the opportunity to participate in cumulative Sorare Pro Competitions (“Leaderboard Competition(s)”), where your Teams’ scores are tracked over numerous Game Weeks.

#### 2.1.1. Choose Your Competition

Sorare Pro offers a number of different Competitions, the accessibility of which to Users depends on the Collectibles they own, with a possibility to win higher scarcity and In-Season Collectibles (as defined below) throughout the Game. All Sorare Pro Competitions are free to play, whether you win or lose, the Collectibles you enter in a Team are yours to keep. No entry fee is required to enter a Team in a Sorare Pro Competition. 

**Available Competitions**. In Sorare Pro Competitions, you and other Users will build Teams of players’ Cards from real world leagues and compete against each other to see who has created the best Team(s) to progress through the Game. Sorare may, at its sole discretion and following its own discretionary criteria, regroup real world leagues to create tier-based Competitions (e.g Contenders, Challengers, Champions). Sorare reserves the right to change, at any time and in its sole discretion, the tiering of a league that is available to play in Sorare Pro. A full list of available Sorare Pro Competitions is located here. Sorare Pro Competitions are announced on the Website or App at least three (3) days before they are scheduled to begin. 

**Onboarding**. Users that have not yet participated in any Sorare Pro Competition may have access to Competitions that are specifically designed to introduce them to the gameplay mechanics of Sorare Pro (“Onboarding Competition(s)”). Onboarding Competitions offer new Users the opportunity to win their first Collectible(s) using Common Cards to progress through the Game. 

**Classic and In-Season**. Sorare Pro Competitions are of two types: Classic and In-Season. In-Season Collectibles are Collectibles issued in connection with the ongoing football season of the club’s league featured on the Collectible. In-Season Competitions primarily require the use of In-Season Collectibles as detailed on the Website or App. Classic Competitions do not require the use of In-Season Collectibles.

**Special Competitions (or “Special Weeklies”)**. Sorare may from time-to-time, and at its sole discretion, organize limited-in-time Competitions with specific eligibility criteria. In such a case, all relevant eligibility criteria will be communicated to you in advance on the Website or App.

#### 2.1.2. Enter Your Team(s)

**Entry Period**. The timeframe to enter a Team into a Sorare Pro Competition, Special Competition and/or Leaderboard Competition will always be communicated to you on the Website or App (the “Entry Period”) and materialized by a countdown. During the Entry Period, you can access the Lobby to compose your Team and enter that Team into the relevant Sorare Pro Competition, Special Competition and/or Leaderboard Competition subject to eligibility requirements as indicated on the Website or App. In addition, some Competitions may require that you enter your Team(s) in a specific Division (as defined below). When the Entry Period ends, the composition of the Team(s) can no longer be modified for the duration of the Sorare Pro Competition.

**Select Your Team(s)**. Unless otherwise specified on the Website or App, you can enter a maximum of three (3) Teams per Sorare Pro Competition.  For each Team you can enter, you must choose five (5) players’ Cards pursuant to the indicated criteria, season and scarcity requirements. The combined total of the players points for each of your Teams must be below the Points Cap specified, if applicable. Higher scarcity Collectibles may be eligible to participate in lower scarcity Sorare Pro Competitions, subject to meeting the other eligibility criteria for the relevant Competition as specified on the Website or App (e.g. Rare Collectibles may participate in a Limited Sorare Pro Competition and Unique Collectibles may participate in any Sorare Pro Competition, subject to meeting other eligibility criteria). Notwithstanding the foregoing, the scarcity bonus associated with a higher scarcity Collectible may not be applicable to a lower scarcity Sorare Pro Competition, as specified in the relevant Sorare Pro Competition description on the Website or App.
A particular player’s Card can only be entered once in a Team (even if you have more than one of that player’s Cards). A Card can only be used in one Team at a time. 

**Flexible Deadline**. In addition, and as applicable, you may be able to modify the composition of your Team until the start of each corresponding player's real game, hereafter referred to as the “Flexible Deadline”, as indicated on the Website or App. For example, if Lamine Yamal’s game starts at 8 PM, you may have until just before to lock his Card into your Team, but if Hachraf Hakimi’s at 10 PM you may have until then to swap his Card’s out..

**List-or-Play**. A particular player’s Collectible cannot simultaneously be entered in a Team and offered for sale or trade on the Marketplace. If you have already listed a particular player’s Collectible on the Marketplace, that same Collectible cannot be entered in a Team without first removing your offer from the Marketplace. If you have already entered a player’s Collectible in a Team and decide to list that Collectible on the Marketplace, the related Collectible will immediately be invalidated from your Team and the full Team will not be eligible for scoring points for the relevant Pro Competition. If you receive a direct offer for a particular player’s Collectible you have entered in a Team, that Team is only invalidated if/when You accept the offer.

#### 2.1.3. Progress Through Division(s)

**Select Your Division(s)**. Sorare Pro Competition may be divided into divisions (the “Divisions”) to help Users progress through the Game and compete against other Users of their level. Unless otherwise specified on the Website or App, all Teams entered in a Sorare Pro Competition start in the Competition’s lowest Division. Higher Divisions unlock as your Team(s) get promoted at the end of a Game Week as explained below. The number of Divisions available per Sorare Pro Competition is at Sorare’s sole discretion.

**Promotion and Relegation**. At the end of each Game Week, each and all Teams entered in Sorare Pro Competitions can either be promoted to a higher Division, maintained in their current Division, or relegated to a lower Division, based on their position in the relevant Sorare Pro Competition’s leaderboard. Relegation and/or promotion criteria for each Division will be communicated to you on the Website or App and are subject to change at any time at the sole discretion of Sorare. The Teams that have been relegated to a lower Division cannot reaccess higher Divisions until they are promoted back. The breakdown of promoted and relegated Teams per Division will be disclosed before each relevant Sorare Pro Competition on the Website or App. Not submitting a Team in a Division during a Game Week may result in the relegation of your Team in the lower Division. Notwithstanding the foregoing, Sorare reserves the right, at its sole discretion, to automatically enter a Team into a Division before a Sorare Pro Competition starts. 

### 2.2. Sorare Rivals

Sorare Rivals offers Users the possibility to individually challenge other Users (“Rival(s)”) in head-to-head Competitions (“Rivals Competition(s)”) based on real-life football games (“Match(es)”). Matches that are eligible to Rivals Competitions are announced on the Website or App at least twenty-four (24) hours before they begin. All Matches are available to all Users and can be played with Common Cards and/or Collectibles. Rivals Competitions are of two types: Arena Games and Friendly Challenges (as detailed below).

#### 2.2.1. Enter a Rivals Competition

**Entry Period.** The entry period to enter a Team into a Rivals Competition runs from the moment the corresponding Match is announced on the Website or App and until the Match starts. During the entry period, you can compose your Team, select your Tactic (as defined below), and challenge other Users in Friendly Challenges or Arena Games (as both defined below). Users cannot enter more than one (1) Team per Rivals Competition. Sorare Rivals Competitions are free-to-play and no entry fee is required to enter a Team

**Points Cap.** Rivals Competitions have a specified points cap, which is the maximum number of total player points the Cards composing a Team can have (“Points Cap”) for the relevant Rivals Competition. Please note that Points Caps may vary depending on the Rivals Competitions you are entering. To select your Team, choose five (5) players’ Cards available for the relevant Rivals Competition, pursuant to the indicated criteria. The combined total of the players points for your Team must be below the Points Cap specified. A particular player’s Card can only be entered once in a Team. Remember that win or lose, the Collectibles you enter in a Team, if any, are yours to keep. You do not forfeit your Collectibles when you enter them into a Rivals Competition.

**Tactic(s).** Your Team may be awarded additional points for specific actions accomplished by the real-life players represented on the Cards entered in your Team during the applicable Match. The strategy (or “Tactic”) that you chose (e.g. “Tiki Taka”, “Joga Bonito”, etc.) may have an impact on your score by granting you additional points based on specific actions performed by the players featured in your Team (e.g number of accurate passes, number of won dribbles, etc.).

**Bonuses.** Points awarded to each player’s Card in your Team may be augmented by an additional bonus in Rivals Competitions, where applicable. Please check our [Help Center](https://sorare.com/en-gb/help/a/15981170919197/how-do-sorare-rivals-card-bonuses-work) for more information on bonuses in Rivals Competitions.

**Substitutes.** After the Match has started, you may be allowed to substitute one (1) player’s Card from your Team against a Card representing another real-life player which has not yet entered the field during the applicable Match. 

#### 2.2.2. Find Your Rivals

**Friendly Challenges (or "Friendlies").** Friendly Challenges are Rivals Competitions where you can compete against as many other Users as you want. You cannot win rewards nor Reputation Points (as defined below) with Friendly Challenges. The Team of your Rival(s) in Friendly Challenges will only be revealed to you when the Match starts.

**Arena Tickets.** Arena tickets are one-time use in-game items that are necessary to access Arena Games, with each Arena Game requiring the use of one (1) Arena Ticket. Users may win Arena Tickets on the Platform notably through Reward Boxes and Missions, if applicable. In addition, once per day, every User can compete in one (1) or more Arena Game(s) of their choice without the need to use an Arena Ticket, as indicated on the Website or App. 

**Arena Games.** Arena Games are Rivals Competitions where you compete against another User to win Reputation Points, improve your Rank and climb through your Division's leaderboard for a chance to win Rewards as indicated on the Website or App. In Arena Games, you do not choose your Rival but are automatically matched against a Rival of your Division (as defined below). In addition, Users that are based in France or the United-Kingdom and who did not enter any Collectible in their Arena Game lineup will only be matched against Rivals who did not enter any Collectible for that same Arena Game either.

#### 2.2.3. Climb Through Divisions and Win Rewards

**Rivals Divisions.** When entering an Arena Game, all Users are divided into ten (10) different groups (or “Division(s)”) that reflect their level in the Game, with an opportunity to progress through Divisions every week (spanning from Tuesday to Tuesday, herein referred to as a “Rivals Season”). Unless otherwise specified on the Website or App, all Users start in the lowest Division.

**Rivals Leaderboards, Reputation Points and Rank.**  In addition, Users that have entered an Arena Game automatically enter a leaderboard of twenty Users within their own Division for the duration of the Rivals Season (“Rivals leaderboard(s)”). A complete Rivals leaderboard is constituted of twenty (20) Users. To reach the top of your Rivals leaderboard, you must win Arena Games to accumulate a maximum of points (or “Reputation Point(s)”) and increase your position (or “Rank”) within the Rivals leaderboard. Losing an Arena Game will result in the loss of Reputation Points which may impact your Rank, as indicated on the Website or App.

**Promotion and Relegation.** In order to be promoted to a higher Division, you must reach the higher ranks of your Rivals leaderboard, as indicated on the Website or App. If you are not promoted, you may either remain in the same Division or be relegated to a lower Division for the next Rivals Season, depending on your Rank in the Rivals leaderboard, as indicated on the Website or App. If you do not enter any Arena Game during a Rivals Season, you will be automatically relegated to the lower Division for the next Rivals Season. If two or more Users have an equal number of Reputation Points at the end of a Rivals Season, the ranking will be adjusted based on the score differences observed in their Arena Games during the said Rivals Season.

**Win Streaks, Missions and Rewards.** Regardless of whether or not your Team includes any Collectibles, you may win rewards in Arena Games if (i) you manage to consecutively win a certain number of Arena Games against other Users within a certain period of time (“Win Streak(s)”), or (ii) reach the top positions of your Rivals leaderboard (as indicated on the Website or App). The amount of consecutive Arena Games required to complete a Win Streak, as well as the timeframe required to achieve that goal, will be communicated to you on the Website or App. You hereby acknowledge and agree that Sorare may unilaterally modify the number of consecutive wins and/or timeframe required to unlock a reward at any time. In addition, you may win additional Reputation Points and/or unlock a reward (as indicated on the Website or App) by completing certain missions in connection with Arena Games (“Missions”). The content of the Missions, their associated reward(s) and the timeframe for their completion will be communicated to you on the Website or the App and is at the sole discretion of Sorare.

**Tied Scores.** In the event two Users' scores are identical at the end of an Arena Game, the Competition ends in a tie and both Rivals lose their Win Streak to win a reward. 

**Incomplete Rivals Leaderboards**. Users who have joined a Rivals Leaderboard that is incomplete (i.e. with less than twenty (20) Users) will not be eligible to either (i) win a Reward, (ii) be promoted to a higher Division, or (iii) be relegated to a lower Division.

#### 2.2.4. Join a Squad and Win Rewards!

**Rivals Squads.** Users can team up to form a group of Users (“Squad(s)” or “Rivals Squad(s)”) where they can join forces to compete in leaderboards against other Squads for a chance to win rewards. A Squad can have between 2 and 10 members. You can only be part of one Squad at a time. While you can join a Squad at any time, if you already earned Reputation Points during the current Rivals Season, you will only be able to join a new Squad at the beginning of the next Season. Additionally, only Users that have reached Division 9 or above can create a Squad.

**Squad Divisions, Leaderboards and Rank.** Squads are divided into ten (10) different groups that reflect their level in the Game (“Squad Division(s)”). New Squads start from the lower Squad Division with an opportunity to progress through Squad Divisions at the end of every Rivals Season. Every Rivals Season, Squads enter a new leaderboard alongside other Squads from the same Squad Division (“Squads Leaderboard(s)”). A complete Squads Leaderboard is constituted of five (5) Squads. For their Squad to reach the top of the Squads Leaderboard, Squad members must accumulate a maximum of Reputation Points to increase their Squad Rank within the Squads Leaderboard. Only the top 5 members of a Squad sum up their individual Reputation Points to determine their Squad’s Rank within the Squads Leaderboard.

**Squad Promotion and Relegation.** In order to be promoted to a higher Squad Division, your Squad must reach the top position(s) of its Squads Leaderboard, as indicated on the Website or App. If your Squad is not promoted, it may either remain in the same Squads Division or be relegated to a lower Squads Division for the next Rivals Season, depending on your Squad’s Rank in the Squads Leaderboard, as indicated on the Website or App. If no member of your Squad enters any Arena Game during a Rivals Season, your Squad may be automatically relegated to the lower Squad Division for the next Rivals Season.

**Squad Quests and Rewards.** All members of a Squad that has been promoted are eligible to win rewards, as indicated on the Website or App. In addition, the top 5 members of a promoted Squad at the end of a Rivals Season are eligible to win individual rewards, as indicated on the Website or App. 

**Tied Scores.** If two or more Squads have accumulated an equal number of Reputation Points at the end of a Rivals Season, their ranking within the Squads Leaderboard will be adjusted based on the cumulated score differences observed in their member’s Arena Games during the said Rivals Season.

### 2.3. The Collection Game

The Collection Game offers Users the possibility to build collections of Collectibles in albums (“Collection Album(s)”) for Users to win a scoring bonus within the Game (“Collection Bonus”), subject to the conditions set forth below.

#### 2.3.1. Collection Requirements

Where indicated on the Website, a licensed club will have one (1) Collection Album per season and scarcity. For a Collectible to contribute to a Collection Album, its corresponding club must have a Collection Album available in the Game for that season. Only one (1) Collectible per individual player is valid per Collection Album (collectively, “Collection Requirements”). Additional details, criteria and other specificities of the Collection Requirements may vary at the discretion of Sorare and will be communicated in advance by Sorare. Sorare reserves the right , at its sole discretion, to determine whether a Collectible has been validly included in a Collection Album, pursuant to the applicable Collection Requirements. Common Cards are not eligible for the Collection Game.

#### 2.3.2. Scoring and Bonus

**Player Card Score.** Each Collectible validly included in a Collection Album will have a score attributed to it based on the scoring matrix available on the [Website](https://help.sorare.com/hc/en-us/articles/10903545889693-What-are-the-rules-of-the-Collection-game-on-Sorare-Football) (“Player Card Score”).

**Collection Album Score.** In addition, each Collection Album will have a total collection album score attributed to it, based on the Player Card Score attributed to each Collectible in the corresponding Collection Album (“Collection Album Score”).If you have two or more (2) Collectibles of the same player that would fulfill the same Collection Requirements, the relevant Collectible of that player with the highest Player Card Score will count toward your Collection Album. If a player represented in a Collectible in your Collection Album moves clubs, that player’s Collectible will continue to contribute to the Collection Album Score of the club and season that are represented on the relevant Collectible.

**Collection Bonus.** Collectibles that have contributed to your Collection Album Score may receive a Collection Bonus in Competitions where XP is scored, as detailed on the [Website](https://help.sorare.com/hc/en-us/articles/10903545889693-What-are-the-rules-of-the-Collection-game-on-Sorare-Football).

#### 2.3.3. List-or-Play

A particular Collectible cannot simultaneously contribute to a Collection Album Score and be offered for sale or trade on the Marketplace. If you have already listed a particular Collectible on the Marketplace, that same Collectible cannot contribute to a Collection Album Score without first removing your offer from the Marketplace. If a particular Collectible already contributes to a Collection Album Score and you decide to list that Collectible on the Marketplace, that same Collectible will immediately lose its Player Card Score thus impacting the corresponding Collection Album Score and Collection Bonus (if any) for all Collectibles in the related Collection Album. If you receive a direct offer for a particular Collectible which belongs to a Collection Album, the associated Player Card Score is only invalidated if/when you accept the offer.

## 3. Rewards

### 3.1. Wallet Rewards

Some rewards will require Users to activate all features of their Cash Wallet (also referred to as "Default Wallet") and/or activate a Blockchain Wallet, as detailed below. Users with both a Cash Wallet and a Blockchain Wallet will be able to configure their preferences to receive rewards in their Preferred Currency or in ETH in their Wallets setting of their Accounts.

#### 3.1.1. Cash Wallet

Rewards won by Users in their Preferred Currency are transferred to the winner’s Default Wallet, subject to full completion by winning Users of account verification requirements set forth on the Website and through Sorare’s partner Mangopay. The exact amount of Preferred Currency sent to the winner’s Cash Wallet will reflect the amount displayed in USD on the Website as converted using the exchange rate displayed on https://www.cryptocompare.com at the close of business day of the relevant Game Week or Competition Leaderboard period.

#### 3.1.2. Blockchain Wallet

Blockchain rewards (Collectibles or ETH, where applicable) are transferred to the winner’s Blockchain Wallet. For ETH rewards, the ETH available in the relevant Competition, Special Competition and/or Leaderboard Competition will be shown in your local currency in the Lobby. The exact amount of ETH sent to the winner's Blockchain Wallet will reflect the amount shown in the local currency and the exchange rate between the local currency and ETH at the close of business day of the relevant Game Week or Competition Leaderboard Period, as displayed on https://www.cryptocompare.com.

Blockchain reward’s value may fluctuate, and Sorare is not responsible for any value fluctuation of blockchain rewards after they are delivered to the winner(s). Blockchain rewards may under no circumstances be delivered to a different address.

### 3.2. In-game Items

In-game Items won by Users are traditional digital game items that remain the entire property of Sorare at all times. Users are only granted a limited and revocable license to personally use in-game items in connection with the Game. Sorare retains all rights to the in-game items and reserves all rights to change or suspend temporarily or indefinitely the possibility for a User to access in-game items. Sorare does not provide any warranties or representation in relation with these in-game items notably the possibility to exchange them for anything else of value.

### 3.3. Merchandise

You will provide the relevant requested information (including mailing address, sizing information as applicable and other information as reasonably requested) to Sorare in order to receive any merchandise rewards. Failure to provide such information within the timeframe specified by Sorare will result in your automatic forfeiture of the reward. You are responsible for any customs, duties, taxes or other fee(s) applicable to the merchandise. To the extent permitted by law, you agree to release, discharge and hold harmless, and waive any and all claims against Sorare in the event a merchandise reward is delayed or lost while in transit to you. Sorare will not replace or resend merchandise delayed or lost in transit. Sorare is not a seller or retailer. For any merchandise reward where a specific team or a player is indicated (including, without limitation, signed merchandise), Sorare reserves the right to replace the stated merchandise with other equivalent merchandise in its sole discretion.

### 3.4. Real Life Experiences

Any real life experience invitations or game/event/exhibition tickets awarded as a reward must be claimed within the time period specified by Sorare, if applicable. Except where clearly specified, **TRAVEL AND LODGING NOT INCLUDED**.

Where specified, you may be able to request your geographic preference for the invitation or tickets being awarded as rewards. All game/event/exhibition tickets and invitations are subject to availability.

Tickets or invitations may not be exchanged, resold, offered for resale, or used for any commercial or promotional purpose whatsoever. Any such resale or commercial or promotional use may result in disqualification and reward forfeiture, and may invalidate the tickets or invitations. Not redeemable for cash or credit. Rewards are nominative and personal, they are not transferable and may not be auctioned, traded, copied, transferred, modified or sold. Use of any game/event/exhibition ticket or invitation is subject to the standard terms, conditions, and health and safety policies applicable to the ticket or invitation. Tickets and seat locations at the relevant game are subject to availability. Guest(s) must be of legal age of majority in their relevant jurisdiction(s) of residence unless accompanied by a parent or legal guardian. Game dates and times may be subject to change. Failure to claim the invitation or ticket(s) awarded in the time period specified by Sorare will result in the automatic forfeiture of the relevant invitation or ticket(s), which Sorare may award to an alternative Competition participant, in its sole discretion.

The terms and conditions of the tickets awarded as part of any Competition will govern in the event a game/event/exhibition is not played or held due to weather conditions, an act of God, an act of terrorism, civil disturbance, or any other reason. Each recipient and, where applicable, his/her guest(s) agree to comply with all applicable venue regulations in connection with the tickets. Sorare and its partners reserve the right to remove or to deny entry to any recipient and/or his/her guest(s) who engage in an unsportsmanlike or disruptive manner or with intent to annoy, abuse, threaten, or harass any other person at the game/event/exhibition. Released Parties (as defined below) will not be responsible for weather conditions; acts of God; acts of terrorism; civil disturbances; local, state, or federal regulation, order, or policy; work stoppage; epidemic, pandemic, or any other issue concerning public health or safety; or any other event outside of their control that may cause the cancellation or postponement of any game/event/exhibition.

You additionally agree, to the extent permitted by applicable law, to release, discharge and hold harmless, and waive any and all claims against Sorare, all Partner Entities, and each of their respective parents, affiliated companies, subsidiaries, officers, directors, employees, general and limited partners, shareholders, members (including, all players (“Players”)), agents, licensees, distributors, dealers, retailers, printers, representatives, advertising and promotion agencies, and any other company associated with the reward, and all of their respective officers, directors, employees, agents, and representatives (collectively, “Released Parties”) for any injury, damage, liability or loss of any kind that may occur, directly or indirectly, in whole or in part, from participation in the Game and/or a Competition, possession, receipt or use of the rewards (or any portion thereof), or any travel or activity related thereto. “Partner Entities” means Sorare’s licensing partners.

The Game is in no way sponsored by any of the Partner Entities. Officially Licensed Product. All rights reserved.

### 3.4. Football Primary Market Credits ("Credit(s)")

**Primary Market.** Credits can only be used to purchase eligible Collectibles on the Sorare.com Football primary market (i.e., Auctions and Instant Buy only). Credits may not be available for all scarcities and/or players’ Collectibles offered on Sorare.com; availability is subject to Sorare’s sole discretion. The currency applicable to Credits will depend on the User's Preferred Currency setting.

**Applicable Credits.** Credits are a fifty percent (50%) discount on Sorare Football primary market Collectibles, up to a certain currency amount, as indicated. For instance, one 50% Credit up to $50, would permit a User to purchase up to $100 in Collectibles with a full, 50% discount of $50 off, resulting in a final price of $50. Relatedly, a User may decide to use less than the full amount of the Credit for the purchase of a Collectible. For instance, if a User purchases a Collectible costing $8 and uses a 50% Credit up to $50, it will result in a final sale price of $4 for that Collectible. 

**Minimum Spend.** Sorare, in its sole discretion, may designate a minimum purchase amount for which a Credit may be applied per User account and/or per transaction. In the event that there is a minimum purchase amount applicable, it will be indicated to you. 

**Maximum Use.** Sorare, in its sole discretion, may designate a maximum limit on the amount a Credit may be applied per User account and/or per transaction. Unless otherwise indicated, Credits may be used for multiple purchases before the Expiration Date up to the certain currency amount in Credits (e.g $50). As an example, if a User purchases a Collectible costing $10 with a 50% Credit up to $50, the Credit only applies to $5 and the User can reuse the 50% Credit for future purchases up to $45 before the Expiration Date (as defined below).

**Expiration.** Credits will expire thirty (30) days after they have been made available to a User (the “Expiration Date”). Notwithstanding the foregoing, each time additional Credits are made available to a User, the Expiration Date timer will be reset so that the User will have thirty (30) days from the most recent date on which Credit is won to use their accumulated amount of Credits. For example, if a User wins a 50% Credit up to $50 which expires in five days, and subsequently wins a 50% Credit up to $5 before the Expiration Date, this User will now have a 50% Credit up to $55, which expires in thirty (30) days). By failing to use your Credit(s) before the Expiration Date, you understand and agree that you have waived the right to use such Credit(s) and are not entitled to, nor will you receive, additional or alternative Credit(s), nor any other compensation or remuneration. 

**Restrictions.** Credits have no cash value, they are not redeemable for cash, ETH, or other items on Sorare.com, including any Club Shop items or other available prizes. Credit(s) are personal, non refundable and non-transferable and may only be redeemed as outlined in this section, and cannot be redeemed on the Sorare.com Marketplace. Except where specified, Credit cannot be combined with any other offers or discounts that may be available on or offered by Sorare.com. Credits cannot be purchased for cash, and cannot be sold. Collectibles purchased with the redemption of a Credit may not be sold or traded on the Sorare.com Marketplace for a certain period of time (e.g 14 days), as indicated on the Website or App (“Ownership Change Delay”). Sorare reserves the right to modify the Ownership Change Delay at any time, and for any reason, in its sole discretion.

## 4. Your Content

By entering this Game, you grant Sorare and Sorare’s affiliates, representatives, licensees, partners, successors, and assigns the transferable, sub-licensable, irrevocable, free of charge, global right to use, copy, distribute, adapt, creative derivative works, reproduce, distribute, modify, translate, publish, broadcast, distribute, and otherwise exploit, including to in any media or support (including but not limited to digital formats, social media, media, television, streaming platforms, Sorare newsletters, e-banners or other promotional materials, etc.) a Team you have composed and information relating to your Team, including, but not limited to, your username, any statements you have made about the Game, and biographical information, for advertising, marketing, public relations and promotional purposes without any further compensation to you.

## 5. Amendment

We reserve the right to modify, at any time, all or part of the Rules. By participating in the Game, you accept the then-current version of the Rules. The applicable version of the Rules is the latest version published on the Website.

---

<a id="mlb-game-rules"></a>

# Sorare: MLB - Game Rules

_Last update: June 11, 2024_

Sorare: MLB is organized by Sorare SAS, a French company registered in the Créteil Trade and Companies Register under the number 844 355 727, with a registered address of at 5 avenue du Général de Gaulle, 94160, Saint-Mandé, France. Participation in Sorare: MLB (“Game”) and in the Collection Game (“Collection Game”)is governed by the Terms and Conditions and these Sorare: MLB Game Rules (“Game Rules” or “Rules”). Capitalized terms not defined here will have the meaning given to them in the Terms and Conditions.

## 1. The Game

Sorare: MLB is based on virtual tournaments that track the real performance of baseball players on the field (“Competition(s)”). The Competitions are accessible from a dedicated area ("Lobby") on the Website. The Game and all Competitions are free to enter.

Competitions are announced in the Lobby at least one (1) week before it is scheduled to begin. The duration of each Competition may vary depending on the time of year and the sporting events planned. Please note that Sorare reserves the right, in its sole discretion to determine the number and frequency of Competitions.

Your objective in the Game is to use your baseball skills and knowledge to create the best team of seven (7) Collectibles and/or Common Cards (“Cards”) representing professional baseball players. Each team must include at least one (1) starting pitcher, one (1) relief pitcher, one (1) corner infielder, one (1) middle infielder and one (1) outfielder (“Team”).

Each Team is awarded points based on the performance of each of the players featured in the relevant Cards. Each player's points are awarded based on that player’s actual performance in real games during the Competition or Leaderboard Competition (as defined below) and according to the scale specified below. Where specified, you may also have the opportunity to participate in cumulative Competitions (“Leaderboard Competition(s)”), where your Teams’ scores are tracked over numerous game weeks. The winner of a Competition or Leaderboard Competition is the User who finishes the Game Week or Leaderboard Competition period with the most points. The winning User and other participants may win a reward, as specified in the Lobby and outlined below.

## 2. Participation in the Game

### 2.1. Entering a Team

**Entry Period**. The entry period to enter a Team into a Competition or Leaderboard Competition varies depending on the duration of the Competition or Leaderboard Competition. The relevant entry period in a given Competition will always be specified in advance on the Website and materialized by a countdown (the “Entry Period”).

During the Entry Period, you can access the Lobby to compose your Team and enter that Team into the relevant Competition or Leaderboard Competition. When the Entry Period ends, the composition of the Team can no longer be modified.

In all cases, Users may enter the Competition or Leaderboard Competition of their choice by submitting a Team of seven (7) Cards representing seven (7) different players and containing at least one (1) starting pitcher, one (1) relief pitcher, one (1) corner infielder, one (1) middle infielder and one (1) outfielder.

**Selecting Your Team**. There are different Competitions or Leaderboard Competitions depending on the scarcity of the Cards required to compose a Team and the rewards available.

You can only enter a maximum of six (6) of players from the same real life team in a Team, and a particular player can only be entered once in that Team (even if you have more than one of that player’s Cards).

A Card can only be used in one (1) Team at a time. You can enter one Team per Competition or Leaderboard Competition.

Remember that win or lose, the Cards you enter in a Team are always yours to keep! You do not forfeit your Cards when you enter them into a Competition or Leaderboard Competition.

### 2.2. Eligibility

**Game Eligibility.** The Game is open to all natural persons who have a valid Sorare account, and who are 18 years of age or older except in connection with some specific Competitions where applicable. You are prohibited from participating in the Game where doing so would be in violation of an agreement you have entered into with a third party. Employees, officers, and directors, and their immediate family members (spouse, parent, child, sibling, and their respective spouses, regardless of where they reside) and members of the same household (whether or not related), of the MLB Entities are not eligible to participate or win a reward.

**Competitions Eligibility.** There are different Competitions or Leaderboard Competitions available to you subject to your compliance  with the relevant eligibility criteria set forth in these Game Rules. To participate, you must have **at least seven (7) total Cards (consisting of Collectibles or Common Cards, as required by the Relevant Competition), including a starting pitcher, a relief pitcher, a corner infielder, a middle infielder and an outfielder.**.

**Champion Competitions.** Champions Competitions require the use of In-Season Collectibles, as detailed in each Champion Competition description. In-Season (or “New Season”) Collectibles are Collectibles issued in connection with the ongoing MLB season (i.e the year indicated on the Collectible matches the current MLB season’s year). 

**Special Competition (or “Special Weeklies”).** Sorare may from time-to-time, and at its sole discretion, organize limited-in-time Competitions with specific eligibility criteria. In such a case, all relevant eligibility criteria will be communicated to you in advance on the Website or App. 

### 2.3. Scoring and Winning

Points are awarded for each player in the Team on the basis of data provided by a Third-Party Service and according to the scale specified below.

More information about Competitions and point systems are available on the [Service FAQs](https://help.sorare.com/hc/en-us/categories/5794681100701-Sorare-MLB).

The winner(s) of a Competition are the user(s) one who composed the Team(s) that were awarded the most points. In some Competitions, rewards are also awarded to Teams finishing in other positions (for example, second or third place).

Winners are notified via their User account. Winners’ usernames will be displayed on the Website for the duration of the Game.

### 2.4. Rewards

**Available rewards.** Available rewards for winners are communicated during the Entry Period of the Competition in the Lobby. 

The reward(s) available depend on the Competition and the final place of the Teams. The reward(s) awarded for a given Competition are **Common Cards, Wallet Rewards (Collectibles, ETH or User’s Preferred Currency), merchandise, Credits, or real life experiences**. The scarcity of the Collectible awarded to the winners depends on the Competition or Leaderboard Competition and their place in the ranking. The player represented by the Common Card or Collectible is at Sorare's discretion. Any above-mentioned reward may, at Sorare’s sole discretion, either be awarded (i) as an individual prize, or (ii) as part of a Reward Box (as defined below).

Following verification of each winner’s eligibility and compliance with these Rules, the Terms and Conditions and any other applicable Additional Terms, the reward(s) will be transmitted pursuant to these Game Rules. By participating in the Game, you give your express permission to be contacted by Sorare by telephone and/or email to administer rewards.

**Reward Boxes.** A Reward Box is a one-time use in-game item that offers Users the opportunity to randomly win a reward from a selection of prizes. The prizes available in a specific Reward Box, as well as the probability for each prize to be won by a User, will be disclosed in advance by Sorare on the Website or App. You acknowledge and agree that an unclaimed and/or unopened Reward Box can be definitely removed from your inventory at all times, subject to a reasonable prior notice, and that you would definitely lose the option to redeem any prize that may have been associated with that Reward Box.

### 2.5. Exclusion and Fraud

Sorare reserves the right to exclude any Users who do not comply or are reasonably suspected of not complying with these Rules, the Terms and Conditions, and any applicable Additional Terms. Any violation of these Rules, the Terms and Conditions, and/or any applicable Additional Terms will result in the User’s immediate disqualification from the Competition or Leaderboard Competition and other Service limits, as provided for in the Terms and Conditions.

### 2.6. Sponsored Competitions

**Competition Partner.** The Game may be sponsored by a third-party partner (“Competition Partner”), where indicated on the Website and/or in other communication. Links or reference to a Competition Partner are not an endorsement of Sorare of such Competition Partner.**

## 3. The Collection Game

The Collection Game offers Users the possibility to build collections of Collectibles in albums (“Collection Album(s)”) for Users to win a scoring bonus within the Game (“Collection Bonus”), subject to the conditions set forth below.

### 3.1. Collection Requirements

Where indicated on the Website, a licensed club will have one (1) Collection Album per season and scarcity. For a Collectible to contribute to a Collection Album, its corresponding club must have a Collection Album available in the Game for that season. Only one (1) Collectible per individual player is valid per Collection Album (collectively, “Collection Requirements”). Additional details, criteria and other specificities of the Collection Requirements may vary at the discretion of Sorare and will be communicated in advance by Sorare. Sorare reserves the right, at its sole discretion, to determine whether a Collectible has been validly included in a Collection Album, pursuant to the applicable Collection Requirements. Common Cards are not eligible for the Collection Game.

### 3.2. Scoring and Bonus

**Player Card Score.** Each Collectible validly included in a Collection Album will have a score attributed to it based on the scoring matrix available on the Website (“Player Card Score”).

**Collection Album Score.** In addition, each Collection Album will have a total collection album score attributed to it, based on the Player Card Score attributed to each Collectible in the corresponding Collection Album (“Collection Album Score”). If you have two or more (2) Collectibles of the same player that would fulfill the same Collection Requirements, the relevant Collectible of that player with the highest Player Card Score will count toward your Collection Album. If a player represented in a Collectible in your Collection Album moves clubs, that player’s Collectible will continue to contribute to the Collection Album Score of the club and season that are represented on the relevant Collectible.

**Collection Bonus.** Collectibles that have contributed to your Collection Album Score may receive a Collection Bonus in Competitions where XP is scored, as detailed on the Website.

### 3.3 List-or-Play

A particular Collectible cannot simultaneously contribute to a Collection Album Score and be offered for sale or trade on the Marketplace. If you have already listed a particular Collectible on the Marketplace, that same Collectible cannot contribute to a Collection Album Score without first removing your offer from the Marketplace. If a particular Collectible already contributes to a Collection Album Score and you decide to list that Collectible on the Marketplace, that same Collectible will immediately lose its Player Card Score thus impacting the corresponding Collection Album Score and Collection Bonus (if any) for all Collectibles in the related Collection Album. If you receive a direct offer for a particular Collectible which belongs to a Collection Album, the associated Player Card Score is only invalidated if/when you accept the offer.

## 4. Rewards

### 4.1 Wallet Rewards

Some rewards will require Users to activate all features of their Default Wallet and/or activate a Blockchain Wallet, as detailed below. Users with both a Default Wallet and a Blockchain Wallet will be able to configure their preferences to receive rewards in their Preferred Currency or in ETH in their Wallets setting of their Accounts. The exact amount of Preferred Currency sent to the winner’s Default Wallet will reflect the amount displayed in USD on the Website as converted using the exchange rate displayed on https://www.cryptocompare.com at the close of business day of the relevant Game Week or Competition Leaderboard Period.

**4.1.1 Default Wallet**

Rewards won by Users in their Preferred Currency are transferred to the winner’s Default Wallet, subject to full completion by winning Users of account verification requirements set forth on the Website and/or through Sorare’s partner Mangopay.

**4.1.2 Blockchain Wallet**

Subject to activation by the winning User of the Blockchain Wallet, Blockchain rewards (Collectibles or ETH, where applicable) are transferred to the winner’s Blockchain Wallet. For ETH rewards, the ETH available in the relevant Competition and/or Leaderboard Competition will be shown in United States dollars in the Lobby. The exact amount of ETH sent to the winner will reflect the amount shown in United States dollars and the exchange rate between the United States dollar and ETH when the reward is transferred to the winner’s Blockchain Wallet, as displayed on https://www.cryptocompare.com. Blockchain rewards’ value may fluctuate, and Sorare is not responsible for any value fluctuation of blockchain rewards after they are delivered to the winner(s). Blockchain rewards may under no circumstances be delivered to a different address.

### 4.2 Merchandise

You will provide the relevant requested information (including mailing address, sizing information as applicable and other information as reasonably requested) to Sorare in order to receive any merchandise rewards. Failure to provide such information within the timeframe specified by Sorare will result in your automatic forfeiture of the reward. You are responsible for any customs, duties, taxes or other fee(s) applicable to the merchandise. To the extent permitted by law, you agree to release, discharge and hold harmless, and waive any and all claims against Sorare in the event a merchandise reward is delayed or lost while in transit to you. Sorare will not replace or resend merchandise delayed or lost in transit. Sorare is not a seller or retailer. For any merchandise reward where a specific team or a player is indicated (including, without limitation, signed merchandise), Sorare reserves the right to replace the stated merchandise with other equivalent merchandise in the event the original reward offered is not available. Some merchandise rewards may be subject to additional terms and conditions, including MLB eGift Cards and MLB.TV subscription codes, as detailed below:

- MLB eGift cards: MLB eGift Cards are issued by Fanatics Retail Group Fulfillment, LLC and only valid on the official MLB Store. Except where required by law, MLB eGift Cards cannot be redeemed for cash or cash equivalent, returned, reproduced, modified, sold, traded, refunded, or replaced if lost or stolen. MLB eGift Cards are not valid on previous purchases or returns, and no cash back is allowed unless required by law. Any unused amount will be applied when you re-enter your MLB eGift Card during your next purchase. All purchases are subject to the official MLB Store Terms of Use.
- MLB.TV subscription codes: blackout and other restrictions may apply, check MLB.TV for more information and access to the current listing of events and regular season games that will be nationally blacked out internationally and in the United States. Your subscription to MLB.TV may automatically renew, please check MLB.TV for more information on cancellation options.

### 4.3 Real Life Experiences

Any game/event/exhibition tickets awarded as a reward must be claimed within the time period specified by Sorare, if applicable. **TRAVEL AND LODGING NOT INCLUDED.**

Tickets may not be exchanged, resold, offered for resale, or used for any commercial purpose whatsoever. Any such resale or commercial use may result in disqualification and tickets forfeiture, and may invalidate the tickets. Not redeemable for cash or credit. Tickets are nominative and personal. Tickets are not transferable and may not be auctioned, traded, copied, transferred, modified or sold. Use of any game/event/exhibition ticket is subject to the standard terms, conditions, and health and safety policies applicable to the ticket. Tickets and seat locations at the game are subject to availability. Guest(s) must be of legal age of majority in their relevant jurisdiction(s) of residence unless accompanied by a parent or legal guardian. Use of any Major League Baseball game/event/exhibition ticket is subject to the standard terms, conditions, and health and safety policies applicable to the ticket; see mlb.com/ticketback for details. Tickets and seat locations at the game are subject to availability. Guest(s) must be of legal age of majority in their relevant jurisdiction(s) of residence unless accompanied by a parent or legal guardian. Major League Baseball game dates and times are determined in the sole discretion of the Commissioner of Baseball, and/or the applicable club and may be subject to change.

The terms and conditions of the tickets awarded as part of any Competition will govern in the event a legal game/event/exhibition, as defined by Major League Baseball, is not played or held due to weather conditions, an act of God, an act of terrorism, civil disturbance, or any other reason. Each recipient and his/her guest(s) agree to comply with all applicable ballpark and venue regulations in connection with the tickets. Sorare and the MLB Entities reserve the right to remove or to deny entry to any recipient and/or his/her guest(s) who engage in a non-sportsmanlike or disruptive manner or with intent to annoy, abuse, threaten, or harass any other person at the game/event/exhibition. Released Parties (as defined below) will not be responsible for weather conditions; acts of God; acts of terrorism; civil disturbances; local, state, or federal regulation, order, or policy; work stoppage; epidemic, pandemic, or any other issue concerning public health or safety; or any other event outside of their control that may cause the cancellation or postponement of any Major League Baseball game/event/exhibition. Major League Baseball game/event/exhibition tickets awarded as rewards may not be resold, offered for resale, or used for any commercial or promotional purpose whatsoever. Any such resale or commercial or promotional use may result in disqualification and reward forfeiture, and may invalidate the license granted by the game/event/exhibition ticket.

You additionally agree, to the extent permitted by applicable law, to release, discharge and hold harmless, and waive any and all claims against Sorare, Competition Partner(s) where applicable, the MLB Entities (as defined below),the Major League Baseball Players Association (“MLBPA”), MLB Players, Inc. (“MLBPI”), OneTeam Partners, LLC (“OTP”), and each of their respective parents, affiliated companies, subsidiaries, officers, directors, employees, general and limited partners, shareholders, members (including, with respect to MLBPA, all Major League baseball players (“Players”)), agents, licensees, distributors, dealers, retailers, printers, representatives, advertising and promotion agencies, and any other company associated with the reward, and all of their respective officers, directors, employees, agents, and representatives (collectively, “Released Parties”) for any injury, damage, liability or loss of any kind that may occur, directly or indirectly, in whole or in part, from participation in the Game and/or a Competition, possession, receipt or use of the rewards, (or any portion thereof), or any travel or activity related thereto. “MLB Entities” means the Office of the Commissioner of Baseball (“BOC”), its Bureaus, Committees, Subcommittees and Councils, MLB Advanced Media, L.P. (“MLB”), Major League Baseball Properties, Inc., The MLB Network, LLC, the Major League Baseball Clubs (“Clubs”), each of their parent, subsidiary, affiliated, and related entities, any entity which, now or in the future, controls, is controlled by, or is under common control with the Clubs or the BOC, and the owners, general and limited partners, shareholders, directors, officers, employees, and agents of the foregoing entities. By participating in the Game, you give your express permission to be contacted by Sorare by telephone and/or email to administer rewards. Sorare is not a seller or retailer.

The Game is in no way sponsored by any of the MLB Entities. Major League Baseball trademarks and copyrights are used with permission of Major League Baseball. Visit MLB.com. The Game is in no way sponsored, administered, executed or produced by MLBPA, MLBPI, OTP or any Players. Officially Licensed Product of MLB Players, Inc. MLBPA and MLBPI trademarks, copyrighted works and other intellectual property rights are owned and/or held by MLBPA or MLBPI and may not be used without the written consent of MLBPA or MLBPI. All rights reserved.

### 4.4 Sorare MLB Primary Market Credits (“Credit(s)”)

**Primary Market.** Credits can only be used to purchase eligible Collectibles on the Sorare.com MLB primary market (i.e., Auctions and Instant Buy only). Credits may not be available for all scarcities and/or players’ Collectibles offered on Sorare.com; availability is subject to Sorare’s sole discretion. The currency applicable to Credits will depend on the User's Preferred Currency setting.

**Applicable Credits.** Credits are a twenty-five percent (25%) discount on Sorare MLB primary market Collectibles, up to a certain currency amount, as indicated. For instance, one 25% Credit up to $50, would permit a User to purchase up to $200 in Collectibles with a full, 25% discount of $50 off, resulting in a final price of $150. Relatedly, a User may decide to use less than the full amount of the Credit for the purchase of a Collectible. For instance, if a User purchases a Collectible priced at $20 and uses a 25% Credit up to $50, it will result in a final sale price of $15 for that Collectible. 

**Minimum Spend.** Sorare, in its sole discretion, may designate a minimum purchase amount for which a Credit may be applied per User account and/or per transaction. In the event that there is a minimum purchase amount applicable, it will be indicated to you. 

**Maximum Use.** Sorare, in its sole discretion, may designate a maximum limit on the amount a Credit may be applied per User account and/or per transaction. Unless otherwise indicated, Credits may be used for multiple purchases before the Expiration Date up to the certain currency amount in Credits (e.g $50). As an example, if a User purchases a Collectible priced $20.00 with a 25% Credit up to $50, the Credit only applies to $5 and the User can reuse the 25% Credit for future purchases up to $45 before the Expiration Date (as defined below).

**Expiration.** Credits will expire thirty (30) days after they have been made available to a User (the “Expiration Date”). Notwithstanding the foregoing, each time additional Credits are made available to a User, the Expiration Date timer will be reset so that the User will have thirty (30) days from the most recent date on which Credit is won to use their accumulated amount of Credits. For example, if a User wins a 25% Credit up to $50 which expires in five days, and subsequently wins a 25% Credit up to $5 before the Expiration Date, this User will now have a 25% Credit up to $55, which expires in thirty (30) days. By failing to use your Credit(s) before the Expiration Date, you understand and agree that you have waived the right to use such Credit(s) and are not entitled to, nor will you receive, additional or alternative Credit(s), nor any other compensation or remuneration. 

**Restrictions.** Credits have no cash value, they are not redeemable for cash, ETH, or other items on Sorare.com, including any Club Shop items or other available prizes. Credit(s) are personal, non refundable and non-transferable and may only be redeemed as outlined in this section, and cannot be redeemed on the Sorare.com Marketplace. Except where specified, Credit cannot be combined with any other offers or discounts that may be available on or offered by Sorare.com. Credits cannot be purchased for cash, and cannot be sold. Collectibles purchased with the redemption of a Credit may not be sold or traded on the Sorare.com Marketplace for a certain period of time (e.g 14 days), as indicated on the Website or App (“Ownership Change Delay”). Sorare reserves the right to modify the Ownership Change Delay at any time, and for any reason, in its sole discretion.

## 5. Your Content

By entering this Game, you grant Sorare and Sorare’s affiliates, representatives, licensees, partners, successors, and assigns the transferable, sub-licensable, irrevocable, free of charge, global right to use, copy, distribute, adapt, creative derivative works, reproduce, distribute, modify, translate, publish, broadcast, distribute, and otherwise exploit, including to in any media or support (including but not limited to digital formats, social media, media, television, streaming platforms, Sorare newsletters, e-banners or other promotional materials, etc.) a Team you have composed and information relating to your Team, including, but not limited to, your username, any statements you have made about the Game, and biographical information, for advertising, marketing, public relations and promotional purposes without any further compensation to you.

## 6. Amendment

We reserve the right to modify, at any time, all or part of the Rules. By participating in the Game, you accept the then-current version of the Rules. The applicable version of the Rules is the latest version published on the Website.

---

<a id="nba-game-rules"></a>

# Sorare NBA - Game Rules

_Last update: October 15, 2024_

The Sorare NBA game is organized by Sorare SAS, a French company registered in the Créteil Trade and Companies Register under the number 844 355 727, with a registered address of at 5 avenue du Général de Gaulle, 94160, Saint-Mandé, France. Participation in the Sorare NBA game (“Game”) and in any game mode (including, without limitation, “Sorare Pro NBA”, “Daily Hoops" and the “Collection Game”) (“Game Mode(s)”) is governed by the Terms and Conditions and these Sorare NBA Game Rules (“Game Rules” or “Rules”). Capitalized terms not defined here will have the meaning given to them in the Terms and Conditions.

## 1. The Game

### 1.1 Gameplay basics

The Sorare NBA game is based on virtual Competitions that track the real performance of basketball players on the court (“Competition(s)”). The Competitions are accessible from a dedicated area ("Lobby") on the Website. The Game and all Competitions are free to enter.

Competitions are announced in the Lobby before they are scheduled to begin. The duration of each Competition may vary depending on the time of year and the sporting events planned. Please note that Sorare reserves the right, in its sole discretion to determine the number and frequency of Competitions.

Your objective in the Game is to use your basketball skills and knowledge to create the best team of five (5) Collectibles and/or Common Cards (“Cards”) representing professional basketball players (a “Team”). A Team’s cards must come in under the Points Cap (defined below).

Each Team is awarded points based on the performance of each of the players featured in the relevant Cards. Each player's points are awarded based on that player’s actual performance in real games during the Competition or Leaderboard Competition (as defined below) and according to the scale specified below. Where specified, you may also have the opportunity to participate in cumulative Competitions (“Leaderboard Competition(s)” or “Chase(s)”), where your Teams’ scores are tracked over numerous game weeks. The winner of a Competition or Leaderboard Competition is the User who finishes the Game Week or Leaderboard Competition period with the most points. The winning User and other participants may win a reward, as specified in the Lobby and outlined below.

### 1.2 Eligibility 

The Game is open to all natural persons who have a valid Sorare account, and who are 18 years of age or older. To participate, you must have at **least five total cards (consisting of Collectibles and/or Common Cards, as required for each Competition)**.

You are prohibited from participating in the Game where doing so would be in violation of an agreement you have entered into with a third party.

Some Competitions may require the use of In-Season Collectibles, as detailed in each Competition description. In-Season (also known as “Current Season” or “New Season”) Collectibles are Collectibles issued in connection with the ongoing NBA season (i.e the year indicated on the Collectible matches the current NBA season’s year). 

### 1.3 Scoring and Winning 

Scoring system. Points are awarded for each player in the Team on the basis of data provided by a Third-Party Service and according to the scale specified below. More information about Competitions and point systems are available on the [Service FAQs](https://sorare.com/help/a/6658037522205/What%20is%20Sorare%20NBA). Note that some Competitions (including Special Weeklies and Daily Hoops, as defined below) may have altered scoring formats that will be communicated in advance on the Website or App.

The winner(s) of a Competition or Leaderboard Competition are the user(s) one who composed the Team(s) that were awarded the most points. In some Competitions or Leaderboard Competitions, rewards are also awarded to Teams finishing in other positions (for example, second or third place). Winners are notified via their User account. Winners’ usernames will be displayed on the Website for the duration of the Game.

**Scoring bonuses**. Points awarded to each Player’s Card in the Team may be augmented by an additional bonus in Competitions where applicable. More information about the bonus systems are available on the Help Center.

### 1.4 Rewards 

Available rewards. Available rewards for winners are communicated during the Entry Period of the Competition or Leaderboard Competition in the Lobby. The reward(s) available depend on the Competition or Leaderboard Competition and the final place of the Teams.

The reward(s) awarded for a given Competition or Leaderboard Competition are Wallet Rewards (Collectibles, ETH or User’s Preferred Currency), Credits, in-game items (Common Cards, Essence or Reward Boxes), merchandise and real-life experiences. The scarcity of the Collectible awarded to the winners depends on the Competition and their place in the ranking. The player represented by the Common Card or Collectible is at Sorare's discretion.

Following verification of each winner’s eligibility and compliance with these Rules, the Terms and Conditions and any other applicable Additional Terms, the reward(s) will be transmitted pursuant to these Game Rules. By participating in the Game, you give your express permission to be contacted by Sorare by telephone and/or email to administer rewards.

**Reward Boxes**. A Reward Box is a one-time use in-game item that offers Users the opportunity to randomly win a reward from a selection of prizes. The prizes available in a specific Reward Box, as well as the probability for each prize to be won by a User, will be disclosed in advance by Sorare on the Website or App. You acknowledge and agree that an unclaimed and/or unopened Reward Box can be definitely removed from your inventory at all times, subject to a reasonable prior notice, and that you would definitely lose the option to redeem any prize that may have been associated with that Reward Box.

### 1.5 Declining a Reward

In the event you do not want a merchandise or real world experience reward you have validly won, you may decline your reward by responding to the reward reward email. If you decline a reward, you will under no circumstances be entitled to an alternative reward or compensation. By declining a reward you renounce any claim to the reward.

### 1.6 Exclusion and Fraud 

Sorare reserves the right to exclude any Users who do not comply or are reasonably suspected of not complying with these Rules, the Terms and Conditions, and any applicable Additional Terms. Any violation of these Rules, the Terms and Conditions, and/or any applicable Additional Terms will result in the User’s immediate disqualification from the Competition or Leaderboard Competition and other Service limits, as provided for in the Terms and Conditions.

### 1.7 Sponsored Competitions 

The Game may be sponsored by a third-party partner (“Competition Partner”), where indicated on the Website and/or in other communication. Links or reference to a Competition Partner are not an endorsement of Sorare of such Competition Partner.

## 2. Game Modes 

### 2.1 Sorare Pro NBA

Sorare Pro NBA Competitions use a bi-weekly schedule, with each Competition spanning over multiple weekdays and real-life basketball games (“Game Week” or “GW”). Competitions available in Sorare Pro are of two types: In-Season Competitions and Classic Competitions (as detailed below, collectively referred to as “Sorare Pro NBA Competitions”). Where specified, you may also have the opportunity to participate in cumulative Sorare Pro NBA Competitions (“Leaderboard Competition(s)” or “Chases”), where your Teams’ scores are tracked over numerous Game Weeks. 

#### 2.1.1. Choose Your Competition

Sorare Pro NBA offers a number of different Competitions, the accessibility of which to Users depends on the Collectibles they own, with a possibility to win higher scarcity and In-Season Collectibles (as defined below) throughout the Game. All Sorare Pro NBA Competitions are free to play, whether you win or lose, the Collectibles you enter in a Team are yours to keep. No entry fee is required to enter a Team in a Sorare Pro NBA Competition. 

**Available Competitions**. In Sorare Pro NBA Competitions, you and other Users will build Teams of players’ Cards from real world NBA teams and compete against each other to see who has created the best Team(s) to progress through the Game. Sorare may, at its sole discretion and following its own discretionary criteria, regroup real world NBA teams to create tier-based Competitions (e.g Underdog, Challengers, Champions). Sorare reserves the right to change, at any time and in its sole discretion, the tiering of a team that is available to play in Sorare Pro. Sorare Pro NBA Competitions are announced on the Website or App at least three (3) days before they are scheduled to begin. 

**Classic and In-Season**. Sorare Pro NBA Competitions are of two types: Classic and In-Season. In-Season Collectibles are Collectibles issued in connection with the ongoing NBA season. In-Season Competitions primarily require the use of In-Season Collectibles as detailed on the Website or App. Classic Competitions do not require the use of In-Season Collectibles.

**Special Competitions (or “Special Weeklies”)**. Sorare may from time-to-time, and at its sole discretion, organize limited-in-time Competitions with specific eligibility criteria. In such a case, all relevant eligibility criteria will be communicated to you in advance on the Website or App.

#### 2.1.2 Enter Your Team(s)

**Entry Period**. The entry period to enter a Team into a Competition or Leaderboard Competition varies depending on the duration of the Competition or Leaderboard Competition. The relevant entry period in a given Competition or Leaderboard Competition will always be specified in advance on the Website and materialized by a countdown. During the entry period, you can access the Lobby to compose your Team and enter that Team into the relevant Competition or Leaderboard Competition. When the Entry period ends, the composition of the Team can no longer be modified. In all cases, Users may enter the Competition or Leaderboard Competition of their choice by submitting a Team of five (5) Cards representing five (5) different players.

**Points Cap**. Each Competition will have a specified Points Cap, which is the maximum number of total player points the Cards composing a Team can have. Each player will have a 10 game average score, this score is the average number of points a player has scored in the Game following their last 10 real-life games played.

**Selecting Your Team**. There are different Leagues and Challenges depending on the scarcity of the Cards required to compose a Team and the rewards available. You can only enter a maximum of four (4) of players from the same real life team in a Team, and a particular player can only be entered once in that Team (even if you have more than one of that player’s cards). At least two (2) NBA teams must be represented in your Team. A Card can only be used in one (1) Team at a time. You can enter once per League or Challenge. Remember that win or lose, the Collectibles you enter in a Team are always yours to keep. You do not forfeit your Collectibles when you enter them into a Competition or Leaderboard Competition.

**List-or-Play**. A particular player’s Collectible cannot simultaneously be entered in a Team and offered for sale or trade on the Marketplace. If you have already listed a particular player’s Collectible on the Marketplace, that same Collectible cannot be entered in a Team without first removing your offer from the Marketplace. If you have already entered a player’s Collectible in a Team and decide to list that Collectible on the Marketplace, the related Collectible will immediately be invalidated from your Team and the full Team will not be eligible for scoring points for the relevant Pro Competition. If you receive a direct offer for a particular player’s Collectible you have entered in a Team, that Team is only invalidated if/when You accept the offer.

### 2.2. Daily Hoops 

Daily Hoops competitions use a daily schedule, with each Competition spanning over a single day with at least one real-life basketball game (“Game Day(s)”).

#### 2.2.1. Choose Your Daily Hoops Competition

Daily Hoops offers a number of different daily Competitions, the accessibility of which to Users depends on the Collectibles they own, with a possibility to win higher scarcity Cards’ Essence and other rewards to progress throughout the Game. All Daily Hoops Competitions are free to play, whether you win or lose, the Collectibles you enter in a Team are yours to keep. No entry fee is required to enter a Team in a Daily Hoops Competition. 

**Available Daily Hoops Competitions**. In Daily Hoops Competitions, you and other Users can build Teams of players’ Cards from real world basketball teams and compete everyday against each other to progress through the Game. The same Daily Hoops Competitions are played across all scarcities (from Common to Unique), with specific eligibility requirements for each Daily Hoops Competition being detailed on the Website or App. 

**Pickup Competition**. Before they can compete in Daily Hoops Competitions, Users may be directed to a Common-only daily Competition called “Pickup”. Pickup offers new Users the opportunity to win their first Collectible(s) using Common Cards to progress through the Game. Users that already participated in a Sorare Pro NBA Competition or collected their first Collectible may not be eligible to Pickupp anymore.

#### 2.2.2 Lock Your Player Cards

**Player Cards Lock**.  Each Card entered in your Team has its own individual Entry Period (hereafter referred to as the “Lock Time”) - corresponding to the start of each player's real-life game time (e.g if Donovan Mitchell’s game tips off at 8 PM, you’ll have until just before to lock his Card into your Team, but if Jokic plays at 10 PM you’ll have until then to swap his Card’s out). The relevant Lock Time for each Card may be specified in advance on the Website or App and materialized by a countdown. In all cases, Users may enter the Daily Hoops Competition of their choice by submitting a Team of five (5) Cards representing five (5) different players. A minimum of one real-life basketball game needs to be scheduled for there to be a live Game Day. If there is no basketball game scheduled on a given day, the Game Day for the next Daily Hoops Competitions is open but doesn’t lock on until the first game of that Game Day. 

**Points Cap**. Each Daily Hoops Competition will have a specified Points Cap, which is the maximum number of total player points the Cards composing a Team can have. Each player will have a 10 game average score (the “L10 score”), this score is the average number of points a player has scored in the Game following their last 10 real-life games played. Unless otherwise specified on the Website or App,  L10 scores refresh after each Sorera Pro NBA Game Week

**Selecting Your Player Cards**. A particular player Card can only be used once in your Team (even if you have more than one of that player’s Card). Additionally, a Collectible cannot be used in two Teams at the same time (i.e if you want to enter a player’s Collectible in a Daily Hoop Competition but you have already entered that player’s Collectible in your ongoing Sorare Pro NBA Competition’s Team, you must cancel the latter first). Remember that win or lose, the Collectibles you enter in a Team are always yours to keep. You do not forfeit your Collectibles when you enter them into a Daily Hoops Competition.

**List-or-Play**. A particular player’s Collectible cannot simultaneously be entered in a Team and offered for sale or trade on the Marketplace. If you have already listed a particular player’s Collectible on the Marketplace, that same Collectible cannot be entered in a Team without first removing your offer from the Marketplace. If you have already entered a player’s Collectible in a Team and decide to list that Collectible on the Marketplace, the related Collectible will immediately be invalidated from your Team and the full Team will not be eligible for scoring points for the relevant Pro Competition. If you receive a direct offer for a particular player’s Collectible you have entered in a Team, that Team is only invalidated if/when You accept the offer.

## 3. The Collection Game

The Collection Game offers Users the possibility to build collections of Collectibles in albums (“Collection Album(s)”) for Users to win a scoring bonus within the Game (“Collection Bonus”), subject to the conditions set forth below.

### 3.1. Collection Requirements

Where indicated on the Website, a licensed club will have one (1) Collection Album per season and scarcity. For a Collectible to contribute to a Collection Album, its corresponding club must have a Collection Album available in the Game for that season. Only one (1) Collectible per individual player is valid per Collection Album (collectively, “Collection Requirements”). Additional details, criteria and other specificities of the Collection Requirements may vary at the discretion of Sorare and will be communicated in advance by Sorare. Sorare reserves the right, at its sole discretion, to determine whether a Collectible has been validly included in a Collection Album, pursuant to the applicable Collection Requirements. Common Cards are not eligible for the Collection Game.

### 3.2. Scoring and Bonus

**3.2.1. Player Card Score.** Each Collectible validly included in a Collection Album will have a score attributed to it based on the scoring matrix available on the Website (“Player Card Score”).

**3.2.2. Collection Album Score.** In addition, each Collection Album will have a total collection album score attributed to it, based on the Player Card Score attributed to each Collectible in the corresponding Collection Album (“Collection Album Score”). If you have two or more (2) Collectibles of the same player that would fulfill the same Collection Requirements, the relevant Collectible of that player with the highest Player Card Score will count toward your Collection Album. If a player represented in a Collectible in your Collection Album moves clubs, that player’s Collectible will continue to contribute to the Collection Album Score of the club and season that are represented on the relevant Collectible.

**3.3.3. Collection Bonus.** Collectibles that have contributed to your Collection Album Score may receive a Collection Bonus in Competitions where XP is scored, as detailed on the Website.

### 3.3 List-or-Play

A particular Collectible cannot simultaneously contribute to a Collection Album Score and be offered for sale or trade on the Marketplace. If you have already listed a particular Collectible on the Marketplace, that same Collectible cannot contribute to a Collection Album Score without first removing your offer from the Marketplace. If a particular Collectible already contributes to a Collection Album Score and you decide to list that Collectible on the Marketplace, that same Collectible will immediately lose its Player Card Score thus impacting the corresponding Collection Album Score and Collection Bonus (if any) for all Collectibles in the related Collection Album. If you receive a direct offer for a particular Collectible which belongs to a Collection Album, the associated Player Card Score is only invalidated if/when you accept the offer.

## 4. Prizes

### 4.1 Wallet Rewards

Some Prizes will require Users to activate all features of their Default Wallet and/or activate a Blockchain Wallet, as detailed below. Users with both a Default Wallet and a Blockchain Wallet will be able to configure their preferences to receive rewards in their Preferred Currency or in ETH in their Wallets setting of their Accounts.

#### 4.1.1 Default Wallet

Prizes won by Users in their Preferred Currency are transferred to the winner’s Default Wallet, subject to full completion by winning Users of account verification requirements set forth on the Website and/or through Sorare’s partner Mangopay. The exact amount of Preferred Currency sent to the winner’s Default Wallet will reflect the amount displayed in USD on the Website as converted using the exchange rate displayed on https://www.cryptocompare.com at the close of business day of the relevant Game Week or Competition Leaderboard Period.

#### 4.1.2 Blockchain Wallet

Subject to activation by the winning User of the Blockchain Wallet, Blockchain prizes (Collectibles or ETH, where applicable) are transferred to the winner’s Blockchain Wallet. For ETH rewards, the ETH available in the relevant Tournament and/or Leaderboard Tournament will be shown in United States dollars in the Lobby. The exact amount of ETH sent to the winner will reflect the amount shown in United States dollars and the exchange rate between the United States dollar and ETH when the reward is transferred to the winner’s Blockchain Wallet, as displayed on https://www.cryptocompare.com. Blockchain prizes’ value may fluctuate, and Sorare is not responsible for any value fluctuation of blockchain prizes after they are delivered to the winner(s). Blockchain prizes may under no circumstances be delivered to a different address.

### 4.2 Merchandise

You will provide the relevant requested information (including mailing address, sizing information as applicable and other information as reasonably requested) to Sorare in order to receive any merchandise rewards. Failure to provide such information within the timeframe specified by Sorare will result in your automatic forfeiture of the prize. You are responsible for any customs, duties, taxes or other fee(s) applicable to the merchandise. To the extent permitted by law, you agree to release, discharge and hold harmless, and waive any and all claims against Sorare in the event a merchandise prize is delayed or lost while in transit to you. Sorare will not replace or resend merchandise delayed or lost in transit. Sorare is not a seller or retailer. For any merchandise reward where a specific team or a player is indicated (including, without limitation, signed merchandise), Sorare reserves the right to replace the stated merchandise with other equivalent merchandise in the event the original prize offered is not available. Some merchandise rewards may be subject to additional terms and conditions, as detailed below:

- NBA eGift cards: NBA eGift Cards are issued by Fanatics Retail Group Fulfillment, LLC and only valid on the official NBA Store. Except where required by law, NBA eGift Cards cannot be redeemed for cash or cash equivalent, returned, reproduced, modified, sold, traded, refunded, or replaced if lost or stolen. NBA eGift Cards are not valid on previous purchases or returns, and no cash back is allowed unless required by law. Any unused amount will be applied when you re-enter your NBA eGift Card during your next purchase. All purchases are subject to the official NBA Store Terms of Use.


### 4.3 Real Life Experiences

Any game/event/exhibition tickets awarded as a reward must be claimed within the time period specified by Sorare, if applicable. **TRAVEL NOT INCLUDED**.

Tickets may not be exchanged, resold, offered for resale, or used for any commercial purpose whatsoever. Any such resale or commercial use may result in disqualification and tickets forfeiture, and may invalidate the tickets. Not redeemable for cash or credit. Tickets are nominative and personal. Tickets are not transferable and may not be auctioned, traded, copied, transferred, modified or sold. Use of any game/event/exhibition ticket is subject to the standard terms, conditions, and health and safety policies applicable to the ticket. Tickets and seat locations at the game are subject to availability. Guest(s) must be of legal age of majority in their relevant jurisdiction(s) of residence unless accompanied by a parent or legal guardian. Use of any basketball game/event/exhibition ticket is subject to the standard terms, conditions, and health and safety policies applicable to the ticket. Guest(s) must be of legal age of majority in their relevant jurisdiction(s) of residence unless accompanied by a parent or legal guardian. Game dates and times may be subject to change.

The terms and conditions of the tickets awarded as part of any Tournament or Leaderboard Tournamen will govern in the event a legal game/event/exhibition is not played or held due to weather conditions, an act of God, an act of terrorism, civil disturbance, or any other reason. Each recipient and his/her guest(s) agree to comply with all applicable venue regulations in connection with the tickets. Sorare and its partners reserve the right to remove or to deny entry to any recipient and/or his/her guest(s) who engage in an unsportsmanlike or disruptive manner or with intent to annoy, abuse, threaten, or harass any other person at the game/event/exhibition. Released Parties (as defined below) will not be responsible for weather conditions; acts of God; acts of terrorism; civil disturbances; local, state, or federal regulation, order, or policy; work stoppage; epidemic, pandemic, or any other issue concerning public health or safety; or any other event outside of their control that may cause the cancellation or postponement of any basketball game/event/exhibition. Game/event/exhibition tickets awarded as prizes may not be resold, offered for resale, or used for any commercial or promotional purpose whatsoever. Any such resale or commercial or promotional use may result in disqualification and reward forfeiture, and may invalidate the license granted by the game/event/exhibition ticket.

You additionally agree, to the extent permitted by applicable law, to release, discharge and hold harmless, and waive any and all claims against Sorare, Tournament Partner(s) where applicable, the NBA Entities (as defined below),the NBPA Entities, and each of their respective parents, affiliated companies, subsidiaries, officers, directors, employees, general and limited partners, shareholders, members (including, with respect to NBPA, all NBA players (“Players”)), agents, licensees, distributors, dealers, retailers, printers, representatives, advertising and promotion agencies, and any other company associated with the reward, and all of their respective officers, directors, employees, agents, and representatives (collectively, “Released Parties”) for any injury, damage, liability or loss of any kind that may occur, directly or indirectly, in whole or in part, from participation in the Game and/or a tournament, possession, receipt or use of the rewards, (or any portion thereof), or any travel or activity related thereto. NBA Entities” means NBA Properties, Inc., the National Basketball Association (the “NBA”), and the NBA member teams. “NBPA Entities” means National Basketball Players Association, its affiliates and the individual members of the NBPA, and each of their parent, subsidiary, affiliated, and related entities, any entity which, now or in the future, controls, is controlled by, or is under common control with the NBA member teams, and the owners, general and limited partners, shareholders, directors, officers, employees, and agents of the foregoing entities. By participating in the Game, you give your express permission to be contacted by Sorare by telephone and/or email to administer rewards. Sorare is not a seller or retailer.

The Game is in no way sponsored by any of the NBA Entities. The Game is in no way sponsored, administered, executed or produced by NBA Entities or NBPA Entities. Officially Licensed Product. All rights reserved.

### 4.4 Sorare NBA Primary Market Credits (“Credit(s)”)

**Primary Market.** Credits can only be used to purchase eligible Collectibles on the Sorare.com NBA primary market (i.e., Auctions and Instant Buy only). Credits may not be available for all scarcities and/or players’ Collectibles offered on Sorare.com; availability is subject to Sorare’s sole discretion. The currency applicable to Credits will depend on the User's Preferred Currency setting.

**Applicable Credits.** Credits are a twenty-five percent (25%) discount on Sorare NBA primary market Collectibles, up to a certain currency amount, as indicated. For instance, one 25% Credit up to $50, would permit a User to purchase up to $200 in Collectibles with a full, 25% discount of $50 off, resulting in a final price of $150. Relatedly, a User may decide to use less than the full amount of the Credit for the purchase of a Collectible. For instance, if a User purchases a Collectible priced at $20 and uses a 25% Credit up to $50, it will result in a final sale price of $15 for that Collectible. 

**Minimum Spend.** Sorare, in its sole discretion, may designate a minimum purchase amount for which a Credit may be applied per User account and/or per transaction. In the event that there is a minimum purchase amount applicable, it will be indicated to you. 

**Maximum Use.** Sorare, in its sole discretion, may designate a maximum limit on the amount a Credit may be applied per User account and/or per transaction. Unless otherwise indicated, Credits may be used for multiple purchases before the Expiration Date up to the certain currency amount in Credits (e.g $50). As an example, if a User purchases a Collectible priced $20.00 with a 25% Credit up to $50, the Credit only applies to $5 and the User can reuse the 25% Credit for future purchases up to $45 before the Expiration Date (as defined below).

**Expiration.** Credits will expire thirty (30) days after they have been made available to a User (the “Expiration Date”). Notwithstanding the foregoing, each time additional Credits are made available to a User, the Expiration Date timer will be reset so that the User will have thirty (30) days from the most recent date on which Credit is won to use their accumulated amount of Credits. For example, if a User wins a 25% Credit up to $50 which expires in five days, and subsequently wins a 25% Credit up to $5 before the Expiration Date, this User will now have a 25% Credit up to $55, which expires in thirty (30) days. By failing to use your Credit(s) before the Expiration Date, you understand and agree that you have waived the right to use such Credit(s) and are not entitled to, nor will you receive, additional or alternative Credit(s), nor any other compensation or remuneration. 

**Restrictions.** Credits have no cash value, they are not redeemable for cash, ETH, or other items on Sorare.com, including any Club Shop items or other available prizes. Credit(s) are personal, non refundable and non-transferable and may only be redeemed as outlined in this section, and cannot be redeemed on the Sorare.com Marketplace. Except where specified, Credit cannot be combined with any other offers or discounts that may be available on or offered by Sorare.com. Credits cannot be purchased for cash, and cannot be sold. Collectibles purchased with the redemption of a Credit may not be sold or traded on the Sorare.com Marketplace for a certain period of time (e.g 14 days), as indicated on the Website or App (“Ownership Change Delay”). Sorare reserves the right to modify the Ownership Change Delay at any time, and for any reason, in its sole discretion.

## 5. Your Content

By entering this Game, you grant Sorare and Sorare’s affiliates, representatives, licensees, partners, successors, and assigns the transferable, sub-licensable, irrevocable, free of charge, global right to use, copy, distribute, adapt, creative derivative works, reproduce, distribute, modify, translate, publish, broadcast, distribute, and otherwise exploit, including to in any media or support (including but not limited to digital formats, social media, media, television, streaming platforms, Sorare newsletters, e-banners or other promotional materials, etc.) a Team you have composed and information relating to your Team, including, but not limited to, your username, any statements you have made about the Game, and biographical information, for advertising, marketing, public relations and promotional purposes without any further compensation to you.

## 6. Amendment

We reserve the right to modify, at any time, all or part of the Rules. By participating in the Game, you accept the then-current version of the Rules. The applicable version of the Rules is the latest version published on the Website.

<a id="no-card-entry-rules"></a>

# ALTERNATIVE ENTRY METHOD RULES
_Last Update: February 7, 2025_ 

These Alternative Entry Method Rules (“**Alternative Entry Rules**”) govern your submission of a Team in the Game via the alternative method of entry (“**Alternative Route**”), as set forth below. Where indicated, you may participate in a Competition or a Special Weekly pursuant to these Alternative Entry Rules, the T&Cs, the Game Rules applicable to the relevant sport, and any other applicable Additional Terms. Capitalized terms not defined here will have the meaning given to them in the Terms and Conditions and/or Game Rules applicable to the relevant sport.
## 1. Participation 
Where and when available, the Alternative Route will be indicated as an option in the Lobby. In order to participate using the Alternative Route, you must meet the eligibility criteria below and follow the submission instructions communicated to the email address associated with your User account. For the avoidance of doubt, participation in the Game via the Alternative Route does not grant you a license or access to Cards.
## 2. Eligibility 
**2.1**. The Alternative Route is open to all natural persons, at least 18 years old (or older where required by your jurisdiction) based in France or in the United-Kingdom, who have a valid Sorare account and have verified the phone number associated with the User account. You may not use the Alternative Route if you have already entered a Team with Collectibles during the relevant entry period. 
**2.2**. Additional eligibility criteria may be communicated to you via email. Sorare reserves the right to verify your eligibility to participate via the Alternative Route (including by requesting supporting documentation and/or additional information from you to ensure that your participation meets the eligibility criteria set forth in this section) and to disqualify the selected team from the Alternative Route if necessary.
## 3. Teams Submissions
**3.1**. Unless otherwise specified on the Website or App, eligible Users have 45 minutes before the relevant Game Week starts to submit their Team using the Alternative Route. The timeline for submitting a Team using the Alternative route may be adjusted from time to time by Sorare. 
**3.2**. Submissions via the Alternative Route will be limited per Sorare account, per sport and per Competition’s scarcity as indicated on the Website for the relevant Game Week. 
## 4. Applicable Rules
**4.1**. Users must select a Team of real-life athletes from a pool that is randomly proposed to the User and which is based on the entire pool of corresponding players’ Collectibles entered in the Competitions for that Game Week. The players proposed by Sorare may have bonuses. If bonuses apply, they will be indicated and will be identical to those affiliated with Collectibles.   
**4.2**. Once Users have validated their Team, they must select a Competition where their Team is eligible for in that Game Week subject to meeting the eligibility criteria for the relevant Competition as specified on the Website.    
**4.3**. The relevant sport’s Game Rules will continue to apply to Users’ participation in any Competition or Tournament, as applicable, including but not limited to the Division progression or the maximum number of Teams allowed per Competition and per Special Weekly. In the event these Alternative Entry Rules conflict with the relevant sport’s Game Rules, these Alternative Entry Rules will apply. 
## 5. Leaderboards and Rewards
If the Team submitted by a User via this Alternative Route scores enough points to win a reward in that Competition, the User will win the reward corresponding to their ranking in that Competition. Users will be notified via email, their usernames will not be displayed on the Website and rewards will be transferred to following any necessary verification steps, which may take a reasonable time to complete.
## 6. Amendment
We reserve the right to modify, at any time, all or part of these Alternative Entry Rules. By participating using the Alternative Route, you accept the then-current version of the Alternative Entry Rules. The applicable version of the Alternative Entry Rules is the latest version published on the Website.

