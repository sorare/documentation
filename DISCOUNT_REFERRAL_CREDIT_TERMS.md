# Referral Program Terms
_Updated: February 12, 2025_ 

These Referral Program Terms and Conditions (the “**Referral Terms**”) govern your eligibility to the Sorare.com’s referral program and your use of Referral Credits (as defined below) on Sorare.com’s Primary Market (i.e., Auctions and Instant Buy only) for the purchase of Collectibles. To participate in the Referral Program, you must accept the Sorare Terms and Conditions and these Referral Terms. Capitalized terms not defined here will have the meaning given to them in the Terms and Conditions and/or additional Applicable Terms.

## 1. Referral program

**1.1**. You may refer other users to sign-up for a User account on the Sorare website by participating in the referral program governed by these Referral Program terms (the “**Referral Program**"). The Referral Program offers Users (the “**Referrer**”) the opportunity to win rewards by inviting others (the “**Referee**”) to use the Services.

## 2. Referrer Eligibility

**2.1**. To be eligible to participate in the Referral Program, you must:
- have a validated account on the Website that complies with the Terms and Conditions;
- comply with these Referral Terms ;
- generate a referral link from the Website https://sorare.com/invite (the "**Referral Link**").

**2.2**. For the avoidance of doubt, the sending, sharing and/or posting of your Referrer's Referral Link in a public place, or the mass advertising and/or distribution of the latter via social media posts, emails or a messaging applications (including with the help of paid advertising), in order to profit from the Referral Program may constitute a breach of these Referral Terms. Please see section 4 "Violations of the Referral Terms" for further information.

## 3. Qualified Referrals

**3.1**. To be considered valid (the "**Qualified Referral**"), the referral must meet the following conditions:
1. the Referee must be a natural person, at least eighteen (18) years of age and separate from the Referrer, who has never created a Sorare User account and was not in the process of creating a User account.
2. The Referee must:     
(i) create an account by accessing the Website through the Referrer's Referral Link;          
(ii) verify the account;           
(iii) accept the Terms and Conditions;           
(iv) purchase five (5) Collectibles of the relevant sport via Auctions or Instant Buy (or seven (7) for Sorare: MLB), within 30 days of creating a User account. For the avoidance of doubt, purchases on the Secondary Market by Referee (i.e through Managers sales) will not count towards the conditions required for a Qualified Referral.
3. Both the Referrer and the Referee must validate their phone number in their User account settings. 

**3.2. Referral Rewards**.

**3.2.1**. In case of a Qualified Referral, both the Referrer and the Referee receive a Referral Credit (“**Credit(s)**”). Except for delays due to technical reasons, Credits won as a reward are added to the Referee’s and Referrer’s User accounts (and are accessible through the “My Sorare Wallet” section on the Website or App) within fourteen (14****) days of the date of the Qualified Referral (the "**Claiming Delay**").  

**3.2.2**. The Claiming Delay may be reduced if the Referee participates in two (2) distinct Game Weeks using Teams that each include at least (1) Collectible that they purchased for their Qualified Referral. In such an event, and except for delays due to technical reasons, any Credits won as rewards shall be added to the Referee’s and Referrer’s User accounts upon the closing of the second Game Week played.

**3.3. Use of Credits**.

**Applicable Credits**. Credits are a fifty percent (50%) discount on Sorare.com’s primary market Collectibles (auctions or Instant Buy), up to a certain currency amount, as indicated. For instance, a $50 Credit would permit a User to purchase up to $100 in Collectibles with a full, 50% discount of $50 off, resulting in a final price of $50. The currency applicable to Credits will depend on the User's Preferred Currency setting.

**Minimum Spend**. Sorare, in its sole discretion, may designate a minimum purchase amount for which a Credit may be applied per User account and/or per transaction. In the event that there is a minimum purchase amount applicable, it will be indicated to you.

**Maximum Use**. Sorare, in its sole discretion, may designate a maximum limit on the amount a Credit may be applied per User account and/or per transaction. Unless otherwise indicated, Credits may be used for multiple purchases before the Expiration Date (as defined below) up to the certain currency amount in Credits (e.g $50). As an example, if a User purchases a Collectible costing $10 with a $50 Credit, the Credit only applies to $5 and the User can reuse the Credit for future purchases up to $45 before the Expiration Date.

**Expiration**. Credits will expire thirty (30) days after they have been made available to a User (the “**Expiration Date**”). Notwithstanding the foregoing, each time additional Credits are made available to a User, the Expiration Date timer will be reset so that the User will have thirty (30) days from the most recent date on which Credit is won to use their accumulated amount of Credits. For example, if a User wins a $50 Credit which expires in five days, and subsequently wins a $5 Credit before the Expiration Date, this User will now have a $55 Credit, which expires in thirty (30) days. By failing to use your Credit(s) before the Expiration Date, you understand and agree that you have waived the right to use such Credit(s) and are not entitled to, nor will you receive, additional or alternative Credit(s), nor any other compensation or remuneration.

**Restrictions**. Credits have no cash value, they are not redeemable for cash, ETH, or other items on Sorare.com, including any Club Shop items or other available prizes. Credit(s) are personal, non refundable and non-transferable and may only be redeemed as outlined in these Terms, and cannot be redeemed on the Sorare.com Marketplace. Except where specified, Credit cannot be combined with any other offers or discounts that may be available on or offered by Sorare.com. Credits cannot be purchased for cash, and cannot be sold. Collectibles purchased with the use of a Credit may not be sold or traded on the Sorare.com Marketplace for a certain period of time (e.g 14 days), as indicated on the Website or App (“**Ownership Change Delay**”). Sorare reserves the right to modify the Ownership Change Delay at any time, and for any reason, in its sole discretion.

## 4. Violations of the Referral Terms

**4.1**. In case of actual, suspected and/or attempted fraud and/or abuse, violation of these Referral Terms and/or the Terms and Conditions, Sorare reserves the right to limit the relevant User from accessing the Services and/or participating in the Referral Program and/or receiving the Referral Rewards, and to undertake other action in connection with the relevant User account(s) pursuant to the Terms and Conditions, without prejudice to any legal action that may be taken by Sorare against the relevant User.

**4.2**. Credit(s) won in violation of these Referral Terms and/or the Terms and Conditions will be invalidated and considered void, regardless of the remaining time available before the Expiration Date, and the involved Referrer will be considered ineligible for any future Referral Rewards.

## 5. Miscellaneous

**5.1**. Sorare reserves the right to ask users of the Referral Program to switch to the Affiliate Program if their performance exceeds the scope and purpose of the Referral Program.

**5.2**. The conditions and benefits set out in these Referral Terms may, at any time and without prior notice, be unilaterally updated. Continued participation in the Referral Program following an update shall constitute acceptance of the updated term(s).

**5.3**. Sorare reserves the right, in its sole discretion, to modify, limit, or discontinue the Referral Program at any time, each without penalty or further obligation to you.