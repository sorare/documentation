# Legal Notice

## SORARE SAS information

SORARE SAS is a French company (société par actions simplifiée) with a share capital of € 2 405 149,31 €, registered in Créteil, France, under number 844 355 727, whose registered office is located at 5, avenue du Général de Gaulle, 94160 Saint-Mandé, France.

SORARE SAS VAT identification number: FR75844355727

⁠Support contact: [support@sorare.zendesk.com](mailto:support@sorare.zendesk.com)

Legal representative and Publishing Director: Nicolas Julia

## ⁠⁠Web hosting

Amazon Web Services EMEA SARL

Located at 38 avenue John F. Kennedy, L-1855 99137 Luxembourg

Contact: https://aws.amazon.com/fr/contact-us/

Phone number: +352 26 73 30 00

## Intellectual property

All content on the site, including trademarks, photographs, texts, comments, illustrations, images (animated or not), video sequences and sounds, as well as the computer applications used for its operation, are protected by current intellectual property laws.

These elements are the exclusive property of Sorare SAS or its partners. Any reproduction, representation, use or adaptation, in any form whatsoever, of all or part of these elements, including computer applications, without the prior written consent of the publisher, is strictly prohibited. The fact that the publisher does not immediately initiate legal proceedings upon becoming aware of any such unauthorized use does not in any way constitute acceptance of such use or a waiver of the intention to take legal action.
