# Credit and Voucher Terms and Conditions

These Credit and Voucher Terms and Conditions (“Credit and Voucher Terms”) govern your eligibility for and use of discount credits (“Credits”) and Collectible vouchers (“Vouchers”) on Sorare.com’s Football Primary Market for the purchase of NFT-backed cards. Your use of the Sorare.com platform is governed by the Terms and Conditions, as well as any additional Applicable Terms. Capitalized terms not defined here will have the meaning given to them in the Terms and Conditions and/or additional Applicable Terms. 

## Eligibility for Credits and Vouchers  

### Credit Eligibility Criteria. 

To be eligible for Credits, you must (i) have a valid Sorare User account, (ii) participate in a program, including for instance certain sponsored private leagues (the “Program(s)”) while the offer is valid (the “Validity Period”) as indicated in the relevant Program details and (iii) meet the other criteria specified by Sorare, as applicable. The relevant Validity Period for the Programs will be communicated to you. Additional terms may apply to the Programs; where applicable, such additional terms will be communicated to you.   
### Voucher Eligibility Criteria. 

To be eligible for a Voucher, you must (i) have a valid Sorare User account, (ii) participate in one of the Programs during the Validity Period as indicated in the relevant Program details, (iii) own a maximum of one (1) Limited Card for any club or player, and (iv) meet the other criteria specified by Sorare, as applicable. The relevant Validity Period for the Programs will be communicated to you. Additional terms may apply to the Programs; where applicable, such additional terms will be communicated to you.

## Use of Credits  

### Primary Marketplace

Credits can only be used to purchase eligible Collectibles on the Sorare.com Football Primary Market (i.e., Auctions and Instant Buy only). Credits may not be available for all scarcities and/or players’ Collectibles offered on Sorare.com; availability is subject to Sorare’s sole discretion. 

### Applicable Credits

The relevant, applicable Credits for the Programs in which you are participating will be awarded as indicated in the relevant Program details. The currency applicable to Credits will depend on the User's Preferred Currency setting. Credits are a certain percentage of discount on Primary Market Collectibles, up to a certain currency amount (e.g., one (1) fifty percent (50%) Credit up to fifteen U.S. Dollars ($15.00), would permit a User to purchase up to $30.00 in Collectibles with a full, fifty percent (50%) discount of $15.00 off, resulting in a final sale price of fifteen U.S. Dollars ($15.00)). Relatedly, less than the full amount of the fifty percent (50%) Credit may be redeemed for the purchase of a Collectible (e.g., if a User purchases a Collectible worth five U.S. Dollars ($5.00), the fifty percent (50%) Credit up to fifteen U.S. Dollars ($15.00) will only apply two U.S. Dollars and fifty cents ($2.50) in credit, resulting in a final sale price of two U.S. Dollars and fifty cents ($2.50) for that Collectible.). Notwithstanding the foregoing, no Credit will be applied in an an amount more than the awarded Credit amount (e.g., if a User attempts to purchase a one hundred U.S. Dollar ($100.00) Collectible, a fifty percent (50%) Credit up to fifteen U.S. Dollars ($15.00) will result in a final sale price of eighty-five U.S. Dollars ($85.00).). 

### Minimum Spend

Sorare, in its sole discretion, may designate a minimum purchase amount for which a Credit may be applied per User account and/or per transaction. In the event that there is a minimum purchase amount applicable, it will be indicated to you 

### Maximum Use

Sorare, in its sole discretion, may designate a maximum limit on the amount a Credit may be applied per User account and/or per transaction. Unless otherwise indicated, Credits may be used for multiple purchases within the Validity Period up to the certain currency amount in Credits awarded to you in the relevant Program. 

### Expiration
Sorare may, at its sole discretion, specify a limited period of time during which Credits may be used before they expire. In the event that there is an expiration date applicable, it will be indicated to you. By failing to use your Credit(s) before the relevant expiration date, you understand and agree you have waived the right to redeem such Credit(s) and are not entitled to, nor will you receive, additional or alternative Credit(s), nor any other compensation or remuneration. 

### Restrictions 

Credits are not redeemable for cash, ETH, or other items on Sorare.com, including any Club Shop items or other available prizes. Credit(s) are personal and non-transferable and may only be redeemed as outlined in Section 2.1, and cannot be redeemed on the Sorare.com Secondary Market. Except where specified, Credit cannot be combined with any other offers or discounts that may be available on or offered by Sorare.com. Collectibles purchased with the redemption of a Credit may not be sold or traded on the Sorare.com Secondary Market for a minimum period of fourteen (14) days (“Ownership Change Delay”). Sorare reserves the right to extend this Ownership Change Delay at any time, and for any reason, in its sole discretion. 

## Use of Vouchers  
### Instant Buy
Vouchers can only be used to purchase eligible Collectibles on the Sorare Website or App using the Instant Buy feature. Vouchers may not be available for all scarcities and/or players’ Collectibles offered on the Sorare Website or App; availability is subject to and in Sorare’s sole discretion. 
### Applicable Vouchers
The relevant, applicable Vouchers for the Programs in which you are participating will be provided to those that meet the Voucher Eligibility Criteria provided for in Section 1.2 or as otherwise indicated in the relevant Program details. Vouchers are a one hundred percent (100%) discount on Primary Market Collectibles, up to a certain currency amount, as indicated in the relevant Program details. Sorare makes no representations or warranties that such Voucher will purchase a Collectible outright. For instance, a ten US Dollars ($10,00) Voucher may be used either as a one hundred percent (100%) discount on Collectible sales of ten US Dollars ($10,00) or less, or as a ten US Dollars ($10,00) discount against the total sale price of the Collectible(s) (e.g., one (1) ten US Dollars ($10,00) Voucher would permit a User to purchase up to $10,00 in Collectibles with a full, one hundred percent (100%) discount of $10,00 off, resulting in a final sale price of zero US Dollars ($0,00).). Relatedly, less than the full amount of the ten US Dollars ($10,00) Voucher may be redeemed for the purchase of a Collectible (e.g., if a User purchases a Collectible worth five US Dollars ($5,00), the ten US Dollars ($10,00) Voucher will only apply five US Dollars ($5,00) in credit, resulting in a final sale price of zero US Dollars ($0,00) for that Collectible. For the avoidance of doubt, in the instance where a Collectible’s sale price is more than the Voucher’s amount, the Voucher will be applied to the final sale price, resulting in discount off the final sale price corresponding to the Voucher’s currency amount (e.g., if a User attempts to purchase a one hundred US Dollars ($100,00) Collectible using a ten US Dollars ($10,00) Voucher, the Voucher will apply a ten US Dollars ($10,00) discount, resulting in a final sale price of ninety US Dollars ($90,00)). 
### Minimum Spend  

Sorare, in its sole discretion, may designate a minimum purchase amount for which a Voucher may be applied per User account and/or per transaction. In the event that there is a minimum purchase amount applicable, it will be indicated to you.

### Maximum Use

Sorare, in its sole discretion, may designate a maximum limit on the amount a Voucher may be applied per User account and/or per transaction. 

### Expiration
Sorare may, at its sole discretion, specify a limited period of time during which Vouchers may be used before they expire. In the event that there is an expiration date applicable, it will be indicated to you. By failing to use your Voucher before the relevant expiration date, you understand and agree you have waived the right to redeem such Voucher and are not entitled to, nor will you receive, additional or alternative discounts, nor any other compensation or remuneration. 
### Restrictions
Vouchers are not redeemable for cash, ETH, or other items on Sorare.com, including any Club Shop items or other available prizes. Vouchers are personal and non-transferable and may only be redeemed as outlined in Section 3.1, and cannot be redeemed on the Sorare.com Secondary Market. Except where specified, Vouchers cannot be combined with any other offers or discounts that may be available on or offered by Sorare.com. Collectibles purchased with the redemption of a Voucher may not be sold or traded during the Ownership Change Delay. Sorare reserves the right to extend this Ownership Change Delay at any time, and for any reason, in its sole discretion. Sorare reserves the right to take back from you a Voucher kept and/or used outside the context of its applicable Program and void any transaction made by you using a Voucher in this context. In such circumstances, any amount paid by you for the purchase of the Collectible on top of the expired Voucher will be refunded to you.
### Wallet Activation and KYC Requirements
Sorare, at its sole discretion, may require you to successfully activate the Cash Wallet powered by Mangopay (or Default Wallet as defined in Sorare Terms and Conditions) and complete the KYC verification required by Mangopay within a specific timeframe indicated in the relevant Program details (e.g., fourteen (14) days) as a condition to your acquisition of a Collectible using a Voucher. In the event that there is a KYC verification required, it will be indicated to you along with the corresponding timeframe in which to complete such KYC verification. If you do not successfully complete the KYC verification within the specified timeframe, the transaction will be voided and any amount paid by you for the purchase of the Collectible on top of the Voucher will be refunded to you.

## Miscellaneous

### Violations of these Credit and Voucher Terms
In case of actual, suspected and/or attempted violation of these Credit and Voucher Terms and/or the Terms and Conditions, including but not limited to where a User is suspected of or is operating multiple accounts or is suspected of or has carried out otherwise fraudulent activity in connection with Credit(s) or Voucher(s), the relevant User and any involved User accounts will be considered ineligible for any future Credit(s) or Voucher(s), and the User’s current Credit(s) or Voucher(s) will be invalidated and considered void, regardless of the remaining time available in the Validity Period before expiration. Further, for particularly egregious conduct, Sorare reserves the right to rescind any and all Collectibles obtained via the redemption of a Credit or Voucher. Additionally, Sorare reserves the right to limit the relevant User from accessing the Services, and to undertake other action in connection with the relevant User account(s) pursuant to the Terms and Conditions, without prejudice to any legal action that may be taken by Sorare against the relevant User(s).
### Amendment
We reserve the right to modify, at any time, all or part of these Credit and Voucher Terms. By accepting and using a Credit or Voucher, you accept the then-current version of these Credit and Voucher Terms. The applicable version of the Credit and Voucher Terms is the latest version made available to you.