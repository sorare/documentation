# Discount Credit Terms

These Discount Credit Terms and Conditions (“Discount Credit Terms”) govern your eligibility for and use of discount credits for use on Sorare.com’s Primary Market for the purchase of NFT-backed cards (“Discount Credits”). Your use of the Sorare.com platform is governed by the Terms and Conditions, as well as any additional Applicable Terms. Capitalized terms not defined here will have the meaning given to them in the Terms and Conditions and/or additional Applicable Terms.

## Eligibility for Discount Credits

### Eligibility Criteria.

To be eligible for Discount Credits, you must (i) have a valid Sorare User account, (ii) participate in a discount program (a “Program”) while the offer is valid (the “Validity Period”) and (iii) meet the other criteria specified by Sorare, if any. The relevant Validity Period for a Program will be communicated to you. Additional terms may apply to a Program; where applicable such additional terms will be communicated to you.

## Use of Discount Credits

### Primary Marketplace.

Discount Credits can only be used to purchase eligible Collectibles on the Sorare.com Primary Market. Discount Credits may not be available for all sports, scarcities and/or players’ Collectibles offered on Sorare.com, in Sorare’s sole discretion.

### Applicable Discount.

The relevant, applicable discount for the Discount Credits Program in which you are participating will be indicated to you.

### Minimum Spend.

Sorare, in its sole discretion, may designate a minimum purchase amount that a Discount Credit may be applied per User account and/or per transaction. In the event that there is a minimum purchase amount applicable, it will be indicated to you.

### Maximum Use.

Sorare, in its sole discretion, may designate a maximum limit on the amount a Discount Credit may be applied per User account and/or per transaction. In the event that there is a maximum Discount Credit applicable, it will be indicated to you.

### Expiration.

Discount Credits must be used before the expiration date communicated to you. By failing to use your Discount Credits before the relevant expiration date, you understand and agree you waive such Discount Credits and will not be entitled to alternative Discount Credits or other compensation.

### Restrictions.
Not Redeemable for cash, ETH, or other items on Sorare.com. Discount Credits cannot be used on the Secondary Market. Except where specified, Discount Credits cannot be combined with other offers, discounts, etc. that may be available on Sorare.com. Collectibles purchased with the use of a Credit may not be sold or traded on the Sorare.com Marketplace for a certain period of time (e.g 14 days), as indicated on the Website or App (“Ownership Change Delay”). Sorare reserves the right to modify the Ownership Change Delay at any time, and for any reason, in its sole discretion.

## Miscellaneous

### Violations of these Discount Credit Terms.

In case of actual, suspected and/or attempted violation of these Discount Credit Terms and/or the Terms and Conditions, including but not limited to where a User is suspected of or is operating multiple accounts or is suspected of or has carried out otherwise fraudulent activity in connection with Discount Credits, the relevant User and any involved User accounts will be considered ineligible for any Discount Credits. Additionally, Sorare reserves the right to limit the relevant User from accessing the Services, and to undertake other action in connection with the relevant User account(s) pursuant to the Terms and Conditions, without prejudice to any legal action that may be taken by Sorare against the relevant User(s).

### Amendment.

We reserve the right to modify, at any time, all or part of these Discount Credit Terms. By accepting and using Discount Credits, you accept the then-current version of these Discount Credit Terms. The applicable version of the Discount Credit Terms is the latest version made available to you.
