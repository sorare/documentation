# Conditions du Marché

## Dernière mise à jour : 29 mars 2024

## 1. Périmètre

Le Marché se présente comme un tableau électronique d'affichage où vous, non-professionenel, pouvez publier une annonce pour vendre une Carte à Collectionner ou acheter des Cartes à Collectionner qui y auraient été mises en vente par d'autres Utilisateurs non-professionnels. Si vous publiez une annonce pour vendre une Carte à Collectionner, vous êtes un « Vendeur » et si vous achetez une Carte à Collectionner, vous êtes un « Acheteur ». Les présentes Conditions du Marché, ainsi que les Conditions générales, régissent vos Offres de vente ou d'achat de Cartes à Collectionner sur Sorare.

## 2. Vente et échange

**2.1** **Achat, vente et échange.** Vous pouvez mettre vos Cartes à Collectionner en vente sur le Marché tel que cela est expliqué dans le [Help Center](https://help.sorare.com/hc/fr/sections/10968029982621-March%C3%A9-secondaire). Lorsque vous mettez en vente une Carte à Collectionner sur le Marché, vous en fixez le prix et publiez une annonce pour la vente de ladite Carte à Collectionner sur le Marché (« **Mise en Vente** »). La Mise en Vente sera ouverte sur le Marché pour la période que vous déterminerez et pourra faire l'objet de contre-offres de la part d'autres Utilisateurs (telles que définies ci-dessous). Vous pouvez accepter des Devises, une Carte à Collectionner ou une combinaison des deux comme contrepartie de la Mise en Vente en question ou d’une Contre-Offre; cet accord implique exclusivement le Vendeur et l'Acheteur. Si vous ne vendez pas votre Carte à Collectionner au cours de la Période de Vente que vous avez déterminée, la Mise en Vente concernée expirera, mais vous pourrez mettre en vente à nouveau la Carte à Collectionner dès que vous le souhaiterez. Veuillez noter qu’en raison de contraintes techniques, il n’est aujourd’hui pas techniquement possible de mélanger les Cryptomonnaies et les Devises Locales, ni les Cartes à Collectionner de différents sports disponibles dans les Services (Football, MLB et NBA) dans une Mise en Vente.

**2.1.1.** **Contre-Offre**. En tant qu'Utilisateur intéressé par la Mise en Vente d'un autre Utilisateur, vous pouvez faire une contre-offre pour acquérir une Carte à Collectionner, en utilisant des Devises, des Cartes à Collectionner, ou une combinaison des deux (« **Contre-Offre(s)** »). Si le montant (en Cryptomonnaies ou en Devises Locales) stipulé dans votre Contre-Offre dépasse le solde actuel de votre Portefeuille, vous serez invité à effectuer un dépôt de Devises dans votre Portefeuille avant d'envoyer votre Contre-Offre. Veuillez noter qu’en raison de contraintes techniques, il n’est aujourd’hui pas techniquement possible de mélanger des Cryptomonnaies et des Devises Locales, ni des Cartes à Collectionner de différents sports disponibles dans les Services (Football, MLB et NBA) dans une Contre-Offre.

**2.1.2** **Frais de service.** Lorsque vous achetez des Cartes à Collectionner sur le Marché, Sorare et/ou les Prestataires de Paiement auxquels vous auriez recours pourraient être amenés à vous facturer des frais de services (« **Frais de Services** »). Les Frais de Services appliqués par Sorare correspondent à la seule mise à disposition de l’infrastructure technique du Marché. L’ensemble des Frais de Services applicables et leurs montants exacts, vous seront clairement indiqués avant la validation de toute transaction. Sorare se réserve le droit de changer le montant de ces Frais de Service à tout moment.

**2.2** **Transactions entre Acheteurs et Vendeurs.** Sorare n'intervient pas, n'arrange pas et plus généralement ne participe pas à la conclusion de la vente entre Acheteurs et Vendeurs sur le Marché de quelque manière que ce soit, ni n'intervient de quelque manière que ce soit dans le choix et/ou le prix des Cartes à Collectionner faisant l'objet de Mises en Vente sur le Marché. Sorare n'est ni partie, ni agent à la vente ou à l'achat d'une Carte à Collectionner sur le Marché, et Sorare n'agit pas comme intermédiaire pour la mise en relation bilatérale ou multilatérale des intérêts des Acheteurs et des Vendeurs. Cela signifie que Sorare n'organise pas et/ou ne fait pas correspondre les intérêts d'achat d'Acheteurs avec des Cartes à Collectionner mises en vente par les Acheteurs, ni ne dirige d'une quelconque manière les Mises en Vente vers les Acheteurs potentiels. Vous êtes seul responsable de vos Offres de vente ou d'achat de Cartes à Collectionner ; nous ne faisons aucune déclaration concernant une Mise en Vente, un Vendeur, un prix, un Acheteur et/ou la valeur d'une Carte à Collectionner. 

**2.3** **Prix et Mise en Vente sur le Marché.** Sorare ne contrôle pas la valeur, la pertinence et/ou l'équité des prix proposés par un Vendeur. Le Vendeur est seul responsable de la détermination du prix de vente de la Carte à Collectionner concernée et l'Acheteur est seul responsable de l'évaluation du prix de toute Carte à Collectionner affichée sur le Marché. Tout Utilisateur disposant d'un compte valide peut publier une Mise en Vente sur le Marché, à la seule discrétion de cet Utilisateur.

**2.4. Classement d'une Mise en Vente**

**2.4.1.** Les Cartes à Collectionner mises en vente sont affichées par défaut selon le critère “Joueur Populaire”, qui est obtenu en fonction du nombre récent d’Utilisateurs abonnés sur Sorare à l’athlète représenté sur la Carte. 

**2.4.2.** Les Utilisateurs peuvent néanmoins choisir un autre critère qui déterminera l’affichage des Mises en Vente de Cartes. Ils ont le choix entre les critères suivants :

- “**Ajoutées récemment**”: les Cartes à Collectionner sont affichées en fonction de leur date de Mise en Vente sur le Marché
- “**Pertinence**”: les Cartes à Collectionner sont affichées en prenant en compte cumulativement les éléments suivants: 
	- La performance des athlètes représentés sur la Carte (score moyen des 15 derniers matchs
	- Le prix proposé par le Vendeur 
	- Les éventuels bonus (d’expérience, de collection, de saison) attachés aux Cartes 

- “**Prix croissant**”: les Cartes à Collectionner sont affichées en fonction du prix proposé par le Vendeur (dans un ordre croissant) 
- “**Plus haut score moyen**”: les Cartes à Collectionner sont affichées en fonction du score moyen de l’athlète représenté sur les 15 derniers matchs (dans un ordre décroissant) 

**2.4.3.** Les Utilisateurs peuvent ensuite filtrer les Cartes à Collectionner listées sur le Marché en fonction des critères objectifs qu’ils souhaitent. Il est ainsi possible aux Utilisateurs d’afficher les Cartes mises en vente sur le Marché en fonction de leur prix, leur rareté, la ligue ou le championnat dans lequel elles évoluent. .

**2.4.4.** Une Carte à Collectionner représentant le même athlète dans une même rareté peut être proposée par plusieurs Vendeurs. Dans ce cas, l’affichage par défaut des Mises en Ventes se fait selon un critère de “Pertinence” qui prend en compte la performance de l’athlète, le prix proposé et les bonus attachés à la Carte. Les Utilisateurs peuvent néanmoins choisir un autre critère: “Ajoutées récemment”, “Prix croissants” ou “Prix décroissants”. Les Utilisateurs peuvent ensuite filtrer les Cartes listées en fonction des critères objectifs de leur choix incluant le numéro de série, les options de paiement ou la saison. 
Les Cartes à Collectionner sont ensuite classées dans les résultats de la recherche en fonction des critères sélectionnés.

**2.5. Les litiges sur le Marché.**

**2.5.1.** **Responsabilité des litiges entre les Acheteurs et les Vendeurs.** Lorsque vous achetez ou vendez des Cartes à Collectionner sur le Marché, vous reconnaissez et acceptez être seul responsable de la transaction en question. Dans toute la mesure permise par la loi applicable, Sorare ne peut être tenue responsable des dommages causés par la vente, l'achat, le prix ou tout autre acte effectué par les Acheteurs ou les Vendeurs sur ou en relation avec le Marché.

**2.5.2.** **Résolution des litiges entre les Acheteurs et les Vendeurs.** Toute réclamation potentielle doit être résolue entre l'Acheteur ou le Vendeur concerné. Le Vendeur est seul responsable de la résolution de toute réclamation faite à son encontre. Les droits des consommateurs découlant du droit de l’Union européenne ou de tout autre droit applicable en matière de protection des consommateurs, incluant le droit de rétractation, ne s’appliquent pas lorsque vous achetez des Cartes sur le Marché. De même, les garanties légales de conformité des contenus numériques existant en droit français ou dans tout autre droit applicable ne s’appliquent pas lorsque vous achetez ou vendez des Cartes à Collectionner sur le Marché (à l'exception de la garantie des vices cachés le cas échéant). 

**2.5.3. Immutabilité des transactions sur la blockchain.** Les transactions effectuées sur le Marché sont enregistrées de manière immuable sur la blockchain. Une fois enregistrées, ces transactions ne peuvent pas être modifiées, annulées ou supprimées par Sorare ou toutes autres personnes extérieures à cette transaction. Vous êtes donc invité à exercer la plus grande diligence avant de procéder à toutes transactions, celles-ci étant par nature définitives et irréversibles pour Sorare.

**2.6. Obligations fiscales et de sécurité sociale.**

**2.6.1.** **Obligations générales.** Nous attirons votre attention sur le fait que pour la plupart des législations fiscales, la vente d'une Carte à Collectionner est susceptible de constituer un revenu imposable, notamment lorsque le prix de vente de la Carte à Collectionner est supérieur à son prix d'achat.

(A) Vous êtes seul responsable de la déclaration de tout revenu provenant d'une vente de votre Carte à Collectionner à l'administration fiscale et/ou sociale compétente.

(B) Rien dans cette section ne doit être interprété comme un conseil financier, fiscal ou juridique. Nous vous encourageons à contacter les autorités compétentes en matière de fiscalité et sociale afin de vous assurer que vous respectez toutes les lois et réglementations qui vous sont applicables.

(C) Vous ne pouvez pas utiliser les Services et/ou le Marché dans le but de contourner et/ou d'éviter toute obligation fiscale, financière ou autre qui vous est applicable, y compris, mais sans s'y limiter, les obligations en matière de revenus ou de sécurité sociale, les lois et/ou réglementations contre le blanchiment d'argent, les obligations de paiement imposées par un tribunal et/ou pour financer ou soutenir toute activité illicite ou liée au terrorisme.

**2.6.2.** **Résidents de France.** Les Utilisateurs résidant fiscalement en France peuvent s'acquitter de leurs obligations fiscales via le site Internet de l'administration fiscale française, [www.impots.gouv.fr](www.impots.gouv.fr), et obtenir des informations sur les régimes fiscaux éventuellement applicables et les modalités de déclaration des impôts sur la page accessible via le lien suivant : [https://www.impots.gouv.fr/portail/node/10841](https://www.impots.gouv.fr/portail/node/10841).

Les Utilisateurs résidant fiscalement en France peuvent, le cas échéant, remplir leurs obligations en matière de sécurité sociale via le Site web de la sécurité sociale, [www.urssaf.fr](www.urssaf.fr), et obtenir des informations sur les régimes de sécurité sociale éventuellement applicables et les procédures de déclaration : [https://www.urssaf.fr/portail/home/espaces-dedies/activites-relevant-de-leconomie.html](https://www.urssaf.fr/portail/home/espaces-dedies/activites-relevant-de-leconomie.html).

**2.6.3.** **Rapport de transaction.** Les Cartes à Collectionner mises en vente sur le Marché et qui ont fait l'objet de transactions seront soumises à un rapport de transaction. Le rapport de transaction porte sur le nombre de transactions et le montant effectué au cours de l'année précédente et sera mis à votre disposition au début de l'année.

