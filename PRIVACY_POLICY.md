# PRIVACY POLICY

September 2022

Version 1.0

## 1. Foreword

Sorare is a sports entertainment platform where sports fans (“you,” or “User”) can engage with their favorite athletes and teams and build connections with the global sports community.

We respect your privacy and are committed to providing a secure environment to our Users. This privacy policy (“**Policy**”) is intended to provide you with important information about how and why your data may be collected and processed by Sorare (“**we**,” “**us**” or “**Sorare**”). All Personal Data (defined below) will be processed pursuant to applicable law, including the European Union’s General Data Protection Regulation, federal and State law in the United States governing personal data protection, and other applicable data protection laws. This Policy is part of the [Terms and Conditions](https://sorare.com/terms_and_conditions), as it is set forth directly therein. Capitalized terms not directly defined here shall have the meaning given to them in the Terms and Conditions.

## 2. Data Collected and Purposes

We collect and process a limited amount of Personal Data for specific purposes, such as allowing you to access the Services, making the Services fun and easy to use, improving your experience and preventing fraud. Whenever we collect your Personal Data, and for whatever purposes, we are committed to always collecting the minimum amount of Data that is necessary for the relevant purpose.

This section provides more information regarding what Personal Data we collect, what we process that Personal Data for and the legal basis for each specific processing purpose.

- **Access to and Use of the Services.** Sorare collects and processes Personal Data in order to allow you to access and use the different parts of the Services, including to facilitate the purchase of a Collectible, play the Game, or participate in our Affiliate or Referral Programs.

  - **To purchase a Collectible** on the Primary Market and/or to purchase, transfer, withdrawal and/or exchange a Collectible via the Marketplace, Sorare collects your username, phone number, wallet public key or payment identifier and has access to part of your credit card number (i.e., the last four digits of the credit card that we have access to via payment services providers but do not directly collect or store).
  - **To facilitate participation in a Tournament**, send prizes to the winners, publish leaderboards, and more, Sorare processes your username, phone number, wallet public key and the prize(s) you have won (where applicable). Sorare may publish your username and the prize(s) you have won on its Website, as part of the leaderboard presenting the results of the tournaments.
  - **To allow participation in our Affiliate or Referral program**, Sorare collects and processes your email address, phone number, username and wallet public key.
  - The legal basis of this processing is contractual. We require this data in order to execute the Terms and Conditions, and any other applicable Additional Terms, that you agreed to when signing up for your User account.
  - **To play directly on the Application with your Game User friends**, Sorare will be able to access your phone contact book with your consent. Sorare may also share your user name with the Game Users who have your contact information in their phone contacts. This possibility can be changed at any time in your user settings.
  - The legal basis for this processing is your consent.

- **Terms Compliance**; Website and Account Security. Sorare collects certain device and browser information (user agent), browsing data, including IP address (from which information on your country/state of location can be inferred), account information (including your email address and phone number) and transaction details, to ensure the security of the Services and your User account, and to detect, prevent, investigate, and/or trace any attempt to violate our Terms and Conditions or any other Applicable Terms, such as by malicious hacking. Your phone number may be used by Sorare for SMS account authentication by our partners. Your phone number will not be shared or sold to any third parties for marketing purposes. In the course of an investigation, Sorare may take additional security measures, including but not limited to requesting additional supporting documents and/or information, or suspending or canceling the relevant transaction in the event the transaction cannot be verified. Where payment fraud has been investigated and confirmed, the relevant Data and other related information will be stored in Sorare’s monitoring system and used for future verifications.

  - The legal basis of this processing is Sorare’s legitimate interest. Sorare has a legitimate interest in detecting, preventing, and investigating fraud to ensure a fair game for all users and to protect its Services and Users against potential credit card frauds.

- **User Requests and Communication**. Sorare collects and processes Personal Data, such as your username, contact details, and the content of your message (to the extent such content is considered Personal Data) when managing and following up on any questions and/or requests you may submit via our Help Center or other method.

  - The legal basis of this processing is contractual. Support is integral to the Services we provide, so we require this data in order to execute the Terms and Conditions, and any other applicable Additional Terms, that you agreed to when signing up for your User account.

## 3. Blockchain Technology and Your Data

- **NFT.** Blockchain is at the core of the Services that Sorare provides. This technology in the form of non-fungible-tokens (NFT) creates the verifiable authenticity, scarcity, and interoperability of your Collectibles.

- **Layer 2 and ZKP.** To provide you with the best experience, Sorare has partnered with Starkware to further build on the Ethereum public blockchain with Zero Knowledge Proof (“ZKP”) technology.
  This infrastructure allows Sorare to benefit from a Layer 2on top of the Layer 1 Ethereum public blockchain. This Layer 2 employs a ZKP data rollup which processes transaction data, updates the Layer 2 state and then offloads the compressed Layer 2 state to the Layer 1 Ethereum blockchain for reference.
  From a data protection perspective, the rollup generates cryptographic receipts that confirm the authenticity of transaction data without publishing or storing Personal Data about the transaction participants on the Layer 1 Ethereum public blockchain.

- **Your data.** Your wallet public key and the information related to your purchases, transfers, withdrawals, and exchanges of Collectibles are stored on our AWS servers, by Starkware who provides the Layer 2 described above and our data availability committee whose members ensure the back-up of this information and are committed to strict confidentiality obligations. No Personal Data of our Users is stored by Sorare on the public Ethereum blockchain. However, please note that if you withdraw your funds or a Collectible from your Wallet (for instance to transfer them to another service or to an external wallet), such transfer will involve the storage of the information related to this transfer on a public blockchain. All transfers to external wallets are subject to the relevant Third-Party Service’s privacy policy and/or terms of service, please make sure to review the privacy policies and practices of the third parties involved before you proceed with such transfer. As indicated in our terms and conditions Sorare will not be held liable for any third-party content or service and disclaims all responsibility for any and all services provided to a User by a third-party service provider.
  - The legal basis of this processing is contractual. The blockchain is at the core of our services. This is what allows Sorare to offer verifiable authenticity, scarcity, and interoperability of your Collectibles.

## 4. Data Retention

- **Account Information**. Sorare will keep your Personal Data until the closure of your account. However, where required or allowed by applicable law, if Sorare needs to retain certain Personal Data beyond the date of closure of your account, then the maximum applicable retention period will apply, pursuant to the applicable statutory limitation periods. This period will not exceed the period for which the Company must retain the Data.

- **Website Use**. Personal Data processed by or on behalf of Sorare for the purposes of analytics or other study or analysis will not be retained by Sorare after such studies have been completed and the relevant reports, outputs and/or insights have been produced.

- **Access to the Services**. Personal Data processed for the use of the Services will only be kept for fifteen (15) months from the date of the relevant activity.

- **Blockchain Technology**. Personal Data processed and logged on a blockchain in connection with your transactions on Sorare will be stored in an immutable way. Please see “Blockchain technology” section above to better understand what categories of information are logged on the public Ethereum blockchain and what measures have been implemented by Sorare to protect your Personal Data.

- **Terms Compliance**. Website and Account Security. Personal Data processed for fraud prevention will be kept for three (3) years after the relevant data has been included on an alert list. Personal Data processed for security matters will be kept for five (5) years from the last account login or suspicious activity.

- **User Requests and Communication**. For management of your questions and/or requests, Sorare will retain your Personal Data for a maximum of three (3) years after the last time you reached out to us.

Notwithstanding the above, Sorare may store some Personal Data for a longer duration pursuant to its legitimate interests and applicable legal obligations.
When the relevant retention period expires, Sorare will delete your Personal Data or apply adapted measures to the relevant data to ensure that you can no longer be identified.
If Sorare concludes that it is not necessary to keep your Personal Data in its active database, it will archive the relevant Data. For all active and archived Data, Sorare will ensure that access to the relevant Data is restricted to a limited number of persons with a genuine need to access it.

## 5. Processors and Data Transfer

Sorare may use Third-Party Service providers and other third parties (“Processors”) to facilitate, maintain, improve, analyze, and secure the Website, App, and Services. Processors may have access to Personal Data for the sole and exclusive purpose of carrying out the processing activity assigned to the relevant Processor by Sorare. Sorare ensures that the service providers have sufficient guarantees for the performance of the processing activity and comply with the applicable laws and regulations.

Personal Data may be processed outside the European Economic Area (“EEA”). When Data is transferred outside of the EEA, Sorare will take all necessary precautions, and alternatively or cumulatively: (i) ensure that the European Commission has taken an adequacy decision regarding the country of destination, (ii) that model contractual clauses adopted by the European Commission or the supervisory authority have been signed with the recipient, (iii) that the recipient adhered to an approved code of conduct or certification mechanism, etc.

## 6. Data Sharing

The Company shares your Personal Data with Third-Party Service providers and suppliers which assist the Company for fulfilling the purposes specified in this Policy.

**Sorare’s API**

At Sorare, we are committed to providing an open platform for our community of Users and developers to build upon new services, games, or applications. To this end, we provide access to an Application Programming Interface (API) allowing authenticated and authorized users to access more conveniently to some public Users’ information published on Sorare’s Website:

- Basic User information, including their username, avatar, and wallet address,
- User’s cards, achievements, and favorites,
- Users’ auctions, offers, purchases, sales, and notifications.

Please note that the following information is never shared through our API:

- Email address,
- Future lineups and rewards,
- Claiming rewards.

**Third-Party Services**

- Some content or features of the Website and/or App are provided by Third-Party Service providers. In order to facilitate your access to such content and/or features, Sorare may share some of your Personal Data with Third-Party Services and suppliers. The Third-Party Service may ask you for additional Data. Whether or not integrated into the Website and/or App directly, these Third-Party Services collect and process any Data they receive pursuant to their relevant privacy policy, in that provider’s sole responsibility.
- Please note that the contractual arrangements between Sorare and our Third-Party Service Providers (such as payments services providers or marketing partners) specify that both Sorare and the Third-Party Service Provider will act as an “independent controller” as defined by the GDPR (or “Business” as defined by the CCPA) and must comply with all applicable data protection laws and regulations. Please consult the relevant privacy policy of the relevant Third-Party Service for more information.

**Partner Sports Leagues, Teams, and Clubs**

- Our partner sports leagues, teams and clubs (“**Partner(s)**”) make Sorare great. Sorare may share your email address and username with these trusted Partners for the relevant Partner’s own direct marketing and promotional purposes. During User account registration, you can choose if you consent to us sharing such Data with our Partners or not. This is totally up to you - If you do not consent, we will not share the Data.
- If you do consent, please note that the contractual arrangements between Sorare and its Partners specify that both Sorare and each Partner will act as an “independent controller” as defined by the GDPR (or “Business” as defined by CCPA) and must comply with all applicable data protection laws and regulations. Please consult the relevant privacy policy of the relevant Partner for more information.

**Judicial and Governmental Authorities**

- Where required by law or valid court order and to the extent legally permitted, Sorare may share your Personal Data with competent courts and/or any other governmental and/or public authorities requesting access to your Personal Data.

We take sharing your Data seriously. Whenever we share your Personal Data with any of the above recipients, we do so on a strictly need-to-know basis and only as necessary for fulfilling identified processing purposes.

## 7. Use by Children

The Website, App, and Services are intended for use by individuals eighteen (18) years old or older. Pursuant to the [Terms and Conditions](https://sorare.com/terms_and_conditions), the Website, App and Services are not available for Users under 18 years old.

## 8. Your Rights

Most applicable data privacy laws (including the European Union’s GDPR, and some American states’ laws, including but not limited to the CCPA) have granted individuals rights in connection with their Data and how that Data is processed. The below section is intended to inform you of your rights in connection with your Personal Data. Please note that all requests pursuant to this section will be processed in accordance with the applicable European laws (notably GDPR) disregarding your location or country of residence but our answers may be adapted to comply with local laws requirements.

You have the right to access your Personal Data, correct it, move it, delete it and a right to restrict how your Data is processed.
You also the right to specify instructions defining the way Personal Data shall be managed after your death.

**Requests**

- To exercise your rights or make an enquiry regarding data privacy at Sorare, you should make a request accompanied by proof of identity by mail addressed to Sorare - 5 avenue du Général de Gaulle 94160 SAINT-MANDÉ FRANCE - or by email at [privacy@sorare.com](mailto:privacy@sorare.com).
- Sorare will attempt to answer your request or enquiry as soon as possible, and at the latest within one (1) month of receipt of the request. For complex requests, we reserve the right to extend this period to three (3) months.
- Individuals located in France also have the option to submit a claim to the Commission Nationale Informatique et Libertés (“CNIL”). Individuals elsewhere in the EU may submit a request to the competent data protection authority in their respective location.

**Deletion of User account**

- If you intend to request the deletion of your Personal Information you can use the method described above or simply request the deletion of your User account via the settings of your User account as described in Sorare’s terms and conditions.
- The deletion of your account will result in the irreversible deletion of the Personal Data associated with the account. To ensure the respects of your rights without altering the history and traceability of each Collectible you owned, the username associated with the ownership of these Collectibles will be pseudonymized and replaced by a random series of alphanumeric characters.
- You must save any Collectibles and Cryptocurrencies stored on the Wallet to an external wallet before any deletion request. Any Collectibles and/or Cryptocurrency stored on your Wallet that have not been transferred out of your account at the date of deletion will be permanently irretrievable. Sorare will not be responsible for the permanent loss of any Collectibles and/or Cryptocurrency that was not saved prior to a deletion request.

Protecting your Personal Data and complying with the applicable data protection legal framework are two of Sorare’s top priorities and we need your help to be successful. By agreeing to this Policy, you agree to keep your Data on Sorare up-to-date, and only provide accurate and current Personal Data. Additionally, if you send us any information that would allow us to identify, directly or indirectly, any other individuals, you represent and warrant that, prior to sharing this information, such other natural persons have been provided with this Policy and, to the extent applicable, have consented to the processing of their Data.

## 9. Cookies

- When you browse our Website, cookies are placed on your terminal, either directly or after having obtained your consent when required by the regulations on cookies.
- Cookies are pieces of information placed on the Internet user's terminal equipment by the server of the website visited. They are used by a website to send information to the Internet user's browser and to allow this browser to send information back to the website of origin (for example, a session identifier or the choice of a language).
- Only the sender of a cookie can read or modify the information contained in it.
- There are different types of cookies:
  - session cookies that disappear as soon as you leave the Website;
  - permanent cookies that remain on your terminal until their lifetime expires or until you delete them using your browser's functions.
- You are informed that, during your visits to the Website, cookies may be installed on your terminal equipment.
- To learn more about the use of cookies on our Website, please see the cookie policy here: [Sorare Cookie Policy](https://sorare.com/cookie_policy).

## 10. Security

Sorare has implemented and maintains measures to protect the confidentiality, security, and integrity of your User account and Personal Data against unauthorized access and disclosure, modification, alteration, damage, accidental loss or accidental or illicit destruction, as well as against any other form of unauthorized processing or disclosure.

We recommend you observe security best practices, including storing your account information securely and not sharing account information. Please note that our backup services will not make a backup of your data to your personal, local storage or drive; we recommend you use a secondary backup source and/or other best practices for the preservation of data. Additionally, we recommend you enable two-factors-authentication, choose a strong password and/or use a password manager for any and all password protected features of your User account and your User account itself.

Sorare recognizes the importance and value of security researchers’ efforts in helping keep our community safe. We encourage responsible disclosure of security vulnerabilities via our bug bounty program (“Bug Bounty Program”) described on [this page](https://hackerone.com/sorare).

## 11. Updates to this Privacy Policy

Sorare reserves the right to change this Policy as necessary, as the Service evolves or as required by applicable laws. Any material Policy changes will be communicated to you.
When using the Services, you are deemed to have accepted the terms of the Policy, as published on our Website.

## 12. Jurisdiction and Applicable Law

The Policy is governed and interpreted according to French and European law. However, if you are a consumer and resident of any European Union country you will benefit from mandatory provisions of, and legal rights available to you under, the laws of that country. Nothing in this Policy affects your rights as a consumer to rely on these local law mandatory provisions and legal rights.

Where allowed or required by applicable law, you agree that the courts of France will have exclusive jurisdiction. However, if you are a consumer and a resident of any other European Union member, you and Sorare may also bring proceedings in that country.


---

<a id="sms-terms-and-conditions"></a>

# SMS Terms and Conditions

December 2023

Version 1.0

These SMS Terms and Conditions ("Terms") govern the use of Short Message Service (SMS) for account verification and fraud detection purposes by Sorare SAS ("Sorare"). By providing your mobile phone number and opting in to receive SMS messages, you agree to these Terms. If you do not agree, please do not use the SMS service.

## 1. Consent to Receive SMS Messages
   By providing your mobile phone number, you consent to receive SMS messages from Sorare for the purpose of account verification and fraud detection. Standard message and data rates may apply.
## 2. Frequency of Messages
   Sorare will send SMS messages only when necessary for account verification or fraud detection purposes. The frequency of messages will be kept to a minimum to respect your privacy.
## 3. Opt-Out
   You may opt-out of receiving SMS messages at any time by replying with "STOP" to the received message. However, opting out is not recommended, as it may prevent Sorare from proceeding with account verification. Please be aware that failure to receive SMS messages for account verification purposes could result in you being unable to retrieve access to your account.
   Upon opting out, you will no longer receive SMS messages, except for those required for legal or transactional purposes.
## 4. Data Privacy
   Sorare values your privacy. Your mobile phone number and any other information collected for SMS purposes will be handled in accordance with our Privacy Policy, available at this address https://sorare.com/privacy-policy.
## 5. Message Content
   SMS messages sent by Sorare may include, but are not limited to, account verification codes, alerts about suspicious account activities, and other messages necessary for account security.
## 6. Message Delivery
   Sorare will make reasonable efforts to ensure the timely delivery of SMS messages. However, we are not responsible for delays or failures in message delivery caused by factors beyond our control.
## 7. Security
   It is your responsibility to keep your mobile phone and account information secure. Sorare is not liable for any unauthorized access or use of your mobile phone or account.
## 8. Independent Partner Responsibility
   SMS messages are sent by independent partners, including but not limited to Twilio and Vonage, on behalf of Sorare. These partners are considered “Data Processor” as defined in the EU General Data Protection Regulation and "Businesses" as defined in the California Consumer Privacy Act (CCPA). Company is not liable for any issues related to the content, delivery, or security of the SMS messages sent by these independent partners.
## 9. Changes to Terms
   Sorare reserves the right to modify or update these Terms at any time. Notice of changes will be provided through the SMS service or on our website. Continued use of the SMS service after the effective date of the changes constitutes acceptance of the modified Terms.
## 10. Contact Information
   For questions or concerns about these SMS Terms and Conditions, please contact us at privacy@sorare.com.